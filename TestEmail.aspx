﻿<%@ Page Language="C#" %>

<script runat="server">
    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        try
        {
            string from = "tien.quach@s60.com.au";
            string to = "tien.quach@s60.com.au";
            int port = 587;
            string smtpServer = "smtp.gmail.com";
            string smtpUser = "ghevanminh@gmail.com";
            string smtpPassword = "pass4now";
            bool enableSSL = true;

            //string from = "tien.quach@s60.com.au";
            //string to = "tien.quach@s60.com.au";
            //int port = 25;
            //string smtpServer = "smtp.abcdef.com"; //103.28.36.174
            //string smtpUser = "noreply@abcdef.com";
            //string smtpPassword = "!@#$%12345";
            //bool enableSSL = false;

            string subject = "this is a test email";
            string body = "this is my test email body";
            
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(from, to);
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            client.Port = 587;
            client.Host = smtpServer;
            client.EnableSsl = enableSSL;
            client.Timeout = 10000;
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(smtpUser, smtpPassword);
            mail.Subject = subject;
            mail.Body = body;
            client.Send(mail);

            lblResult.Text = "Mail Sent";
        }
        catch (Exception ex)
        {
            lblResult.Text = ex.Message + "<br/>" + ex.InnerException.Message;
        }
    }
</script>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Button ID="btnSendEmail" Text="Send Email" OnClick="btnSendEmail_Click" runat="server" />
        <asp:Label ID="lblResult" runat="server"></asp:Label>
    </form>
</body>
</html>

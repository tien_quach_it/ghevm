﻿<%@ WebHandler Language="C#" Class="GetImage" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

public class GetImage : IHttpHandler {

    public int _width;
    public int _height;
    public static string noImageUrl = @"images\no_photo.jpg";
    public string imageURL;

    public bool IsReusable { get { return true; } }

    public void ProcessRequest(HttpContext context)
    {
        Bitmap bitOutput;
        Bitmap bitInput = GetImageBitMap(context);
        bitInput = RotateFlipImage(context, bitInput);

        if (SetHeightWidth(context, bitInput))
        { bitOutput = ResizeImage(bitInput, _width, _height); }
        else { bitOutput = bitInput; }

        // Check Extension
        string fileExtension = GetImageFileExtension(context);
        switch (fileExtension)
        {
            case "gif": 
                context.Response.ContentType = "image/gif";
                bitOutput.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
                break;
            case "png": 
                context.Response.ContentType = "image/png";
                bitOutput.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png);
                break;
            default:  // "jpeg", "jpg": 
                context.Response.ContentType = "image/jpeg";
                bitOutput.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                break;
        }

        context.Response.Flush();
        context.Response.End();
    }

    private string GetImageFileExtension(HttpContext context)
    {
        string imageFile = context.Request.QueryString["image"];
        if (String.IsNullOrEmpty(imageFile))
            return ".jpg";
        string fileExtension = imageFile.Substring(imageFile.LastIndexOf(".") + 1);
        return fileExtension;

    }

    //private static System.Drawing.Imaging.ImageFormat GetFormat(String MimeType)
    //{
    //    switch (MimeType.ToLower())
    //    {
    //        case "image/jpeg":
    //        case "image/jpg": return System.Drawing.Imaging.ImageFormat.Jpeg;
    //        case "image/gif": return System.Drawing.Imaging.ImageFormat.Gif;
    //        case "image/png": return System.Drawing.Imaging.ImageFormat.Png;
    //        default: return System.Drawing.Imaging.ImageFormat.Bmp;
    //    }
    //}

    /// <summary>
    /// Get the image requested via the query string.
    /// </summary>
    ///     /// <returns>Return the requested image or the "no image" default if it does not exist.</returns>
    public Bitmap GetImageBitMap(HttpContext context)
    {

        Bitmap bitOutput = null;
        try
        {
            String imagePath = context.Server.MapPath(ImageHelper.GetProductImageFullPath(context.Request.QueryString["image"]));
            FileStream stream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
            bitOutput = new Bitmap(stream);
            stream.Close();
        }
        catch
        {
        }
        return bitOutput;

    }


    /// <summary>
    /// Set the height and width of the handler class.
    /// </summary>
    /// The context to get the query string parameters, typically current context.    /// The bitmap that determines the     /// <returns>True if image needs to be resized, false if original dimensions can be kept.</returns>
    public bool SetHeightWidth(HttpContext context, Bitmap bitInput)
    {
        double inputRatio = Convert.ToDouble(bitInput.Width) / Convert.ToDouble(bitInput.Height);

        if (!(String.IsNullOrEmpty(context.Request["width"])) && !(String.IsNullOrEmpty(context.Request["height"])))
        {
            _width = Int32.Parse(context.Request["width"]);
            _height = Int32.Parse(context.Request["height"]);
            return true;
        }
        else if (!(String.IsNullOrEmpty(context.Request["width"])))
        {
            _width = Int32.Parse(context.Request["width"]);
            _height = Convert.ToInt32((_width / inputRatio));
            return true;
        }
        else if (!(String.IsNullOrEmpty(context.Request["height"])))
        {
            _height = Int32.Parse(context.Request["height"]);
            _width = Convert.ToInt32((_height * inputRatio));
            return true;
        }
        else
        {
            _height = bitInput.Height;
            _width = bitInput.Width;
            return false;
        }
    }

    /// <summary>
    /// Flip or rotate the bitmap according to the query string parameters.
    /// </summary>
    /// The context of the query string parameters.    /// The bitmap to be flipped or rotated.    /// <returns>The bitmap after it has been flipped or rotated.</returns>
    public Bitmap RotateFlipImage(HttpContext context, Bitmap bitInput)
    {
        Bitmap bitOut = bitInput;

        if (String.IsNullOrEmpty(context.Request["RotateFlip"]))
        {
            return bitInput;
        }
        else if (context.Request["RotateFlip"] == "Rotate180flipnone")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate180FlipNone);
        }
        else if (context.Request["RotateFlip"] == "Rotate180flipx")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate180FlipX);
        }
        else if (context.Request["RotateFlip"] == "Rotate180flipxy")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate180FlipXY);
        }
        else if (context.Request["RotateFlip"] == "Rotate180flipy")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate180FlipY);
        }
        else if (context.Request["RotateFlip"] == "Rotate270flipnone")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate270FlipNone);
        }
        else if (context.Request["RotateFlip"] == "Rotate270flipx")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate270FlipX);
        }
        else if (context.Request["RotateFlip"] == "Rotate270FlipXY")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate270FlipXY);
        }
        else if (context.Request["RotateFlip"] == "Rotate270FlipY")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate270FlipY);
        }
        else if (context.Request["RotateFlip"] == "Rotate90FlipNone")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate90FlipNone);
        }
        else if (context.Request["RotateFlip"] == "Rotate90FlipX")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate90FlipX);
        }
        else if (context.Request["RotateFlip"] == "Rotate90FlipXY")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate90FlipXY);
        }
        else if (context.Request["RotateFlip"] == "Rotate90FlipY")
        {
            bitOut.RotateFlip(RotateFlipType.Rotate90FlipY);
        }
        else if (context.Request["RotateFlip"] == "RotateNoneFlipX")
        {
            bitOut.RotateFlip(RotateFlipType.RotateNoneFlipX);
        }
        else if (context.Request["RotateFlip"] == "RotateNoneFlipXY")
        {
            bitOut.RotateFlip(RotateFlipType.RotateNoneFlipXY);
        }
        else if (context.Request["RotateFlip"] == "RotateNoneFlipY")
        {
            bitOut.RotateFlip(RotateFlipType.RotateNoneFlipY);
        }
        else { return bitInput; }

        return bitOut;
    }


    /// <summary>
    /// Resizes bitmap using high quality algorithms.
    /// </summary>
    /// 
    private Bitmap ResizeImage(Bitmap originalImage, int newWidth, int newHeight)
    {

        Bitmap newImage = new Bitmap(originalImage, newWidth, newHeight);
        Graphics g = Graphics.FromImage(newImage);
        g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        g.DrawImage(originalImage, 0, 0, newImage.Width, newImage.Height);
        originalImage.Dispose();

        return newImage;
    }

}
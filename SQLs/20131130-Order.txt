﻿alter proc [dbo].[SP_OrderWithCount]
	(
		@PageIndex nvarchar(10),
		@PageSize nvarchar(10),
		@OrderBy nvarchar(100)
	)
as
begin
	declare @SQL nvarchar(4000);	
	set @SQL = '
		SELECT *			
		FROM (
			select c.*,
				ROW_NUMBER () OVER ( '
		
		--if CHARINDEX('packagename', lower(@OrderBy)) > 0
		if lower(@OrderBy) = ''
		begin
			set @SQL = @SQL + ' ORDER BY c.ID'
		end 		
		else
		begin
			set @SQL = @SQL + ' ORDER BY c.' + @OrderBy
		end 	
			
		set @SQL = @SQL + '
				) AS RowNumber
			from vw_Order c	
			where c.Deleted = 0 ' + '	
		) AS Results
		WHERE RowNumber BETWEEN ' + @PageIndex + ' * ' + @PageSize + ' + 1 and (' + @PageIndex + ' + 1) * ' + @PageSize

	EXEC sp_executesql @SQL;	

	
	select count(*) from vw_Order 
	where Deleted = 0;
End



alter view vw_OrderDetail
as
	select d.*, p.Picture, p.ProductCode, p.ProductName, IsNull(c.ColorName, '') as ColorName, IsNull(m.MaterialName, '') as MaterialName
	from OrderDetail d
	left outer join Product p on d.ProductID = p.ID
	left outer join ProductColor c on d.ColorID = c.ID
	left outer join Material m on c.MaterialID = m.ID




﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SocialShare.ascx.cs" Inherits="controls_SocialShare" %>
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>

<script type="text/javascript">
    window.fbAsyncInit = function () {
        FB.init({
            appId: '1598654940392207',
            xfbml: true,
            version: 'v2.3'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    window.twttr = (function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function (f) {
            t._e.push(f);
        };

        return t;
    }(document, "script", "twitter-wjs"));
</script>

<style type="text/css">
    .social-share {
        display: table;
        padding-bottom: 20px;
    }
    .social-share > div {
        float: left;
        padding-right: 10px;
        line-height: 0;
    }
    .clr {
        clear: both;
    }
</style>

<div class="social-share">
    <div class="facebook">
        <div class="fb-share-button" data-href="<%= PageUrl %>" data-layout="button_count"></div>
    </div>
    <div class="google">
        <!-- Place this tag where you want the share button to render. -->
        <div class="g-plus" data-action="share" data-annotation="bubble" data-href="<%= PageUrl %>"></div>
    </div>
    <div class="twitter">
        <a class="twitter-share-button"
            href="https://twitter.com/intent/tweet?url=<%= PageUrl %>"
            data-counturl="<%= PageUrl %>">
            Tweet</a>
    </div>
    <div class="clr"></div>
</div>
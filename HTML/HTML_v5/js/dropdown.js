$(document).ready(function() { 

$("#nav li").hover(function(){ 

        $(this).find('ul:first').css({visibility: "visible",display: "none"}).show(200); 
        },function(){ 
        $(this).find('ul:first').css({visibility: "hidden"}); 

        }); 
});  


			
			function DropDownChatLieu(el) {
				this.ddMaterials  = el;
				this.placeholder = this.ddMaterials.children('span');
				this.opts = this.ddMaterials.find('ul.dropdown > li');
				this.val = '';
				this.index = -1;
				this.initEvents();
			}
			
			function DropDownMau(el) {
				this.ddColor  = el;
				this.placeholder = this.ddColor.children('span');
				this.opts = this.ddColor.find('ul.dropdown > li');
				this.val = '';
				this.index = -1;
				this.initEvents();
			}
			
			DropDownChatLieu.prototype = {
				initEvents : function() {
					var obj = this;

					obj.ddMaterials.on('click', function(event){
						$(this).toggleClass('active');
						return false;
					});
					
					obj.opts.on('click',function(){
						var opt = $(this);
						obj.val = opt.text();
						obj.index = opt.index();
						obj.placeholder.text('' + obj.val);
					});
				},
				getValue : function() {
					return this.val;
				},
				getIndex : function() {
					return this.index;
				}
			}
			
			DropDownMau.prototype = {
				initEvents : function() {
					var obj = this;

					obj.ddColor.on('click', function(event){
						$(this).toggleClass('active');
						return false;
					});

					obj.opts.on('click',function(){
						var opt = $(this);
						obj.val = opt.text();
						obj.index = opt.index();
						obj.placeholder.text('' + obj.val);
					});
				},
				getValue : function() {
					return this.val;
				},
				getIndex : function() {
					return this.index;
				}
			}
			$(function() {

				var ddMaterials = new DropDownChatLieu( $('#ddMaterials') );
				
				var ddMau = new DropDownMau( $('#ddColor') );

				$(document).click(function() {
					// all dropdowns
					$('.wrapper-dropdown-1').removeClass('active');
				});

			});
			
<!---- fix z-index --> 
$(function() {  
    var zIndexNumber = 1000;  
    $('div').each(function() {  
        $(this).css('zIndex', zIndexNumber);  
        zIndexNumber -= 10;  
    });  
});  
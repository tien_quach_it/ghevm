﻿<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>

<%@ Register Src="~/Portals/0/UserControls/CategoriesNavigation.ascx" TagPrefix="dnn" TagName="CategoriesNavigation" %>
<%@ Register Src="~/Portals/0/UserControls/StatisticsBox.ascx" TagPrefix="dnn" TagName="StatisticsBox" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/Header.ascx" TagPrefix="dnn" TagName="Header" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/BottomScripts.ascx" TagPrefix="dnn" TagName="BottomScripts" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/MobileFooterMenu.ascx" TagPrefix="dnn" TagName="MobileFooterMenu" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/SubFooter.ascx" TagPrefix="dnn" TagName="SubFooter" %>

<div class="wrapper push">
    <%--Header--%>
    <dnn:Header runat="server" id="Header" />
    <!--- End Header ---->  
    
    <%--Desktop Content--%>
    <div class="content"><%--hidden-xs--%>
    	<div class="left_column hidden-xs">
            <dnn:CategoriesNavigation runat="server" ID="CategoriesNavigation" />
            
            <dnn:StatisticsBox runat="server" ID="StatisticsBox" />
            <!--- end statistics --->
            
            <div id="LeftPane" runat="server">
            </div>
        </div><!--- end left_column --->
        
        <div class="center_column">
            <div id="ContentPane" runat="server"></div>
            <%--<div class="banner">
            	<img src="<%= SkinPath%>images/banner.png" alt="banner"  />
            </div>--%>
        </div><!--- end center_column --->
        
        <div class="right_column hidden-xs">
        	<div class="search_box">
            	<div class="textbox">
            		<input type="text" name="Your Email" placeholder="Search" class="text_style txtSearch" />
                </div>
                <div class="button btnSearch"> 
                    <a href="#" >
                        <img alt="ico" src="<%= SkinPath%>images/search_btn_ico.png"  />
                    </a></div>
            </div><!--- end_search_box --->
            <div id="RightPane" runat="server"></div>            
            
        </div><!--- end right_column --->
        <div style="clear:left;"></div>
    </div><!--- end content --->
    
    <%--Mobile Sub-Footer--%>
    <dnn:SubFooter runat="server" ID="SubFooter" />
    
    <%--Footer--%>
    <div class="footer">
    	<div class="footer_container" id="BottomPane" runat="server">
        </div>
    </div>
    <%--End Footer--%>

    <%--<div align="center" class="copy_right">
        <p><dnn:COPYRIGHT id="dnnCOPYRIGHT" runat="server" CssClass="copy_right_text" /></p>
        <img src="<%= SkinPath%>images/logo.png" alt="logo" />
    </div>
    <div class="info">
        <p>Xưởng: Lô A211 – A 212, KCN Thái Hòa, Đức Hòa III, Long An</p>
        <p>ĐT: 072.375.9677 – 678 | Fax: 072.375.9676</p>
        <p>Showroom: 926 Nguyễn Chí Thanh, P4, Q.11 -  ĐT: (08)39.561.340</p>
        <p>Thời gian làm việc: Thứ 2 đến thứ 7:  từ 8h đến 18h. Chủ nhật: từ 8h đến 12h</p>
        <p>Xem thêm danh sách cửa hàng trên toàn quốc <a href="/DanhSachCuaHang.aspx">tại đây</a></p>
    </div>--%>
</div><!--- end wrapper --->

<dnn:BottomScripts runat="server" ID="BottomScripts" />


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.UI.Skins;

public partial class Portals_0_Skins_GheVM_Controls_Header : SkinObjectBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected string SkinPath
    {
        get
        {
            return PortalSettings.HomeDirectory + "Skins/GheVM/";
        }
    }
}
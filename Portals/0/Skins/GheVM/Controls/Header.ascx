﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="Portals_0_Skins_GheVM_Controls_Header" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="NAV" Src="~/Admin/Skins/Nav.ascx" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/MobileSearch.ascx" TagPrefix="dnn" TagName="MobileSearch" %>
<%@ Register Src="~/Portals/0/UserControls/MobileCategoriesNavigation.ascx" TagPrefix="dnn" TagName="MobileCategoriesNavigation" %>

<%--<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>--%>

<%--Header--%>
<div class="header hidden-xs">
    <div class="login-panel">
        <%
            if (DNNHelper.CurrentUserID != -1)
            {
        %>
        <a href="/QuanLy/QuanLySanPham.aspx"><strong>Quản Lý</strong></a>
        &nbsp; | &nbsp;
        <dnn:LOGIN ID="dnnLOGIN" runat="server" />
        <%
            }
        %>
    </div>
	<div class="banner" style="display:block; background:url(<%= SkinPath%>images/Bg.jpg) no-repeat;">
        <img alt="Ghe van phong Van Minh" src="<%=SkinPath%>images/banner-header.jpg" width="960" />
    </div>
</div>

<!-- Row 2: Mobile Logo-->
<div class="logoRow hidden-lg hidden-md hidden-sm stickyHeader borderBottom">
    <div class="container">
        <div class="row grid">
            <div class="col-md-3 col-sm-3 row2Left">
                <div class="row mHeader">
                    <div class="col-xs-1 visible-xs mHeaderL"><a data-toggle=".mobileMainNav" class="mOpen open-left-button" href="#"><span class="icon-menu"></span><span class="iconLabel">Menu</span> </a></div>
                    <div class="col-sm-12 col-xs-10 mHeaderM">
                        <a href="/">
                            <img alt="" src="<%= SkinPath %>images/GheVM-mobile.jpg" width="38%"></a>
                    </div>
                    <div class="col-xs-1 visible-xs mHeaderR"><a data-target="#modalContact" data-toggle="modal" class="mCall"><span class="icon-phone"></span><span class="iconLabel">Call</span> </a></div>
                </div>
            </div>            
        </div>
    </div>
</div>

<!-- Row 2: Mobile Logo-->
<div class="logoRow hidden-lg hidden-md hidden-sm">
    <div class="container">
        <div class="row grid">
            <div class="col-md-5 col-sm-4 row2right">
                <div class="input-group">
                    <dnn:MobileSearch runat="server" ID="MobileSearch" />
                </div>
            </div>
        </div>
    </div>
</div>

<%--Menu--%>
<div class="menu hidden-sm hidden-xs">
	<div id="nav">
	    <dnn:NAV ID="dnnNAV" runat="server" ProviderName="DDRMenuNavigationProvider"  IndicateChildren="false" ControlOrientation="Horizontal" CSSControl="mainMenu"
                PopulateNodesFromClient="False" ExpandDepth="2" ></dnn:NAV>
    </div>
</div>

<%--Mobile Menu--%>
<div class="mobileMainNav">
    <div class="container">
        <div class="topMobileNav visible-xs">
            <dnn:MobileCategoriesNavigation runat="server" ID="MobileCategoriesNavigation" />

            <%--<ul class="megamenu" id="megamenu">
                <li class="level0">
                    <a class="level0" href="/physicians">Find a Doctor</a>
                </li>
                <li class="separator"></li>
                <li class="level0">
                    <a class="level0" href="/medical-services">Medical Services</a>
                    <span class="icon-arrow-right-generic childIndicator"></span>
                    <div class="sub">
                        <ul>
                            <li class="back-button"><a href="javascript:void(0);" class="mobileMenuBackBtn"><i class="icon-arrow-left-generic"></i>Back</a></li>
                            <li class="subheader"><a href="/Medical-Services/Cancer">Cancer</a></li>
                            
                        </ul>
                    </div>
                </li>



            </ul>--%>
        </div>
    </div>
</div>

<!-- Modal Contact Box on mobile-->
<div aria-hidden="false" aria-labelledby="modalContact" role="dialog" tabindex="-1" id="modalContact" class="modal">
    <div class="modal-contact">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Điện Thoại</h4>
            </div>
            <div class="modal-body">
                <div class="mBtn">
                    <div class="mBtnLeft"><span class="icon-phone"></span></div>
                    <div class="mBtnRight">
                        Liên hệ mở đại lý<br>
                        <div class="phone-number">
                            <a href="tel:0723759677">Điện Thoại: 072.375.9677</a>
                            <a href="tel:0723759678">Điện Thoại: 072.375.9678</a>
                        </div>
                    </div>
                </div>
                <div class="mBtn">
                    <div class="mBtnLeft"><span class="icon-phone"></span></div>
                    <div class="mBtnRight genInfo">
                        Showroom<br>
                        <div class="phone-number"><a href="tel:0839561340">Điện Thoại: (08)39.561.340</a></div>
                    </div>
                </div>
                <a data-dismiss="modal" class="btnBlue btnMd">Đóng</a>
            </div>
        </div>
    </div>
</div>

<dnn:DnnCssInclude ID="DnnCssInclude1" runat="server" FilePath="css/bootstrap.css" PathNameAlias="SkinPath" Priority="18" />
<dnn:DnnCssInclude ID="DnnCssInclude2" runat="server" FilePath="css/bootstrap-theme.css" PathNameAlias="SkinPath" Priority="19"  />
<dnn:DnnCssInclude ID="DnnCssInclude3" runat="server" FilePath="css/all.css" PathNameAlias="SkinPath" Priority="20"  />
<dnn:DnnCssInclude ID="DnnCssInclude6" runat="server" FilePath="css/desktop.css" PathNameAlias="SkinPath" Priority="21"  />
<dnn:DnnCssInclude ID="DnnCssInclude4" runat="server" FilePath="css/tablet.css" PathNameAlias="SkinPath" Priority="22"  />
<dnn:DnnCssInclude ID="DnnCssInclude5" runat="server" FilePath="css/mobile.css" PathNameAlias="SkinPath" Priority="23"  />

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="js/bootstrap.js" PathNameAlias="SkinPath" Priority="25"  />

<%--<dnn:NAV runat="server" id="dnnNAV"  ProviderName="DNNMenuNavigationProvider"  IndicateChildren="false" ControlOrientation="Horizontal" CSSControl="mainMenu"
                PopulateNodesFromClient="False" ExpandDepth="2" />--%>
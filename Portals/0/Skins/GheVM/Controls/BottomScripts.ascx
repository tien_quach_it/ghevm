﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BottomScripts.ascx.cs" Inherits="Portals_0_Skins_GheVM_Controls_BottomScripts" %>
<%--<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>--%>

<div class="fb_icon_mobile hidden-lg"> <a rel="me" target="_blank" href="https://www.facebook.com/ghevanminh">facebook lifeshop</a> </div>

<script type="text/javascript" language="JavaScript">var hasFlash = false; if (window.ActiveXObject) { try { if (new ActiveXObject("ShockwaveFlash.ShockwaveFlash")) hasFlash = true; } catch (e) { } } else { if (navigator.plugins["Shockwave Flash"]) { hasFlash = true; } } var elems = document.getElementsByTagName("div"); for (var i in elems) { if (!hasFlash && elems[i].className == "aleoflash-gif") elems[i].style.display = "block"; else if ((!hasFlash && elems[i].className == "aleoflash-swf") || elems[i].className == "aleoflash") elems[i].style.display = "none"; }</script>
<script type="text/javascript" src='<%= ResolveUrl("~/js/site.js") %>'></script>
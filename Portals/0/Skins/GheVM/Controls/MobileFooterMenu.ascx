﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MobileFooterMenu.ascx.cs" Inherits="Portals_0_Skins_GheVM_Controls_MobileFooterMenu" %>
<div class="row-subfooter footerRow2 hidden-lg hidden-md hidden-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="box-group footerGroup">
                <div class="footerWrap">
                    <div class="box-heading footerBar hidden-lg hidden-md">
                        <h4 class="box-title"><a href="/"><span class="barspan">Ghế Văn Phòng</span></a></h4>
                    </div>                        
                </div>
            </div>
            <div class="box-group footerGroup">
                <div class="footerWrap">
                    <div class="box-heading footerBar hidden-lg hidden-md">
                        <h4 class="box-title"><a href="/GioiThieu.aspx"><span class="barspan">Giới Thiệu</span></a></h4>
                    </div>                        
                </div>
            </div>
            <div class="box-group footerGroup">
                <div class="footerWrap">
                    <div class="box-heading footerBar hidden-lg hidden-md">
                        <h4 class="box-title"><a href="/SanPham.aspx"><span class="barspan">Sản Phẩm</span></a></h4>
                    </div>                        
                </div>
            </div>
            <div class="box-group footerGroup">
                <div class="footerWrap">
                    <div class="box-heading footerBar hidden-lg hidden-md">
                        <h4 class="box-title"><a href="/HeThongPhanPhoi.aspx"><span class="barspan">Hệ Thống Phân Phối</span></a></h4>
                    </div>                        
                </div>
            </div>
            <div class="box-group footerGroup">
                <div class="footerWrap">
                    <div class="box-heading footerBar hidden-lg hidden-md">
                        <h4 class="box-title"><a href="/DichVu.aspx"><span class="barspan">Dịch Vụ</span></a></h4>
                    </div>                        
                </div>
            </div>
            <div class="box-group footerGroup">
                <div class="footerWrap">
                    <div class="box-heading footerBar hidden-lg hidden-md">
                        <h4 class="box-title"><a href="/TinTucSuKien.aspx"><span class="barspan">Tin Tức - Sự Kiện</span></a></h4>
                    </div>                        
                </div>
            </div>
            <div class="box-group footerGroup">
                <div class="footerWrap">
                    <div class="box-heading footerBar hidden-lg hidden-md">
                        <h4 class="box-title"><a href="/LienHe.aspx"><span class="barspan">Liên Hệ</span></a></h4>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>
</div>
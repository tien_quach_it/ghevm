﻿<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="NAV" Src="~/Admin/Skins/Nav.ascx" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/Header.ascx" TagPrefix="dnn" TagName="Header" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/SubFooter.ascx" TagPrefix="dnn" TagName="SubFooter" %>


<link type="text/css" rel="stylesheet" href="<%= SkinPath%>css/admin.css" />

<script runat="server">
    Function GetPageIcon() As String
        If String.IsNullOrEmpty(DNNHelper.CurrentPortalSettings.ActiveTab.IconFileLarge) Then
            Return "/Portals/0/ico_about.png"
        End If
        Return DNNHelper.CurrentPortalSettings.ActiveTab.IconFileLarge
    End Function
</script>

<div class="wrapper push">
    <%--Header--%>
    <dnn:Header runat="server" id="Header" />
    <!--- End Header ---->
    
    <div class="content">
    	<div class="left_column">
        	<div class="left_menu">
            	<div class="title-bar"> <p> Admin </p> </div>
            	<div class="categories">
            	    <dnn:NAV runat="server" id="dnnSubNAV2" ProviderName="DNNMenuNavigationProvider" IndicateChildren="true" ControlOrientation="Vertical" 
                        ForceDownLevel="true" PopulateNodesFromClient="False" SeparatorRightHTML=" " csscontrol="rmRootGroup" ExpandDepth="2" StartTabId="91">
                    </dnn:NAV>                    
                </div><!--- end categories --->

            </div><!--- end left_menu --->
            
            <%--<div class="access_statistics_box">
            	<div class="title-bar"> <p> Thống Kê Truy Cập </p> </div>
                <div class="statistics">
                	<img src="<%= SkinPath%>images/statistics.png" alt="statistics" />
                </div>
            </div>--%><!--- end statistics --->
            
            <div id="LeftPane" runat="server">
            </div>
        </div><!--- end left_column --->
        
        <div class="center_right_column">
            <div id="ContentPane" runat="server"></div>
        </div><!--- end center_column --->
        
        <div style="clear:left;"></div>
    </div><!--- end content --->
    
    <%--Mobile Sub-Footer--%>
    <dnn:SubFooter runat="server" ID="SubFooter" />
    
    <%--Footer--%>
    <div class="footer">
    	<div class="footer_container" id="BottomPane" runat="server">
        </div>
    </div>
    <%--End Footer--%>
</div>



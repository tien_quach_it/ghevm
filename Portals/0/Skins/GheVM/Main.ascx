﻿<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register Src="~/Portals/0/UserControls/CategoriesNavigation.ascx" TagPrefix="dnn" TagName="CategoriesNavigation" %>
<%@ Register Src="~/Portals/0/UserControls/StatisticsBox.ascx" TagPrefix="dnn" TagName="StatisticsBox" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/Header.ascx" TagPrefix="dnn" TagName="Header" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/BottomScripts.ascx" TagPrefix="dnn" TagName="BottomScripts" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/SubFooter.ascx" TagPrefix="dnn" TagName="SubFooter" %>


<script runat="server">
    Function GetPageIcon() As String
        If String.IsNullOrEmpty(DNNHelper.CurrentPortalSettings.ActiveTab.IconFileLarge) Then
            Return "/Portals/0/ico_about.png"
        End If
        Return DNNHelper.CurrentPortalSettings.ActiveTab.IconFileLarge
    End Function
</script>

<div class="wrapper push">
    <%--Header--%>
    <dnn:Header runat="server" id="Header" />
    <!--- End Header ---->
   
    <div class="content">
    	<div class="left_column hidden-xs">
            <dnn:CategoriesNavigation runat="server" ID="CategoriesNavigation" />
            
            <dnn:StatisticsBox runat="server" ID="StatisticsBox" />
            <!--- end statistics --->
            
            <div id="LeftPane" runat="server">
            </div>
        </div><!--- end left_column --->
        
        <div class="sub_right_column">
        	<div class="sub_title">
            	<img alt="<%= DNNHelper.CurrentPortalSettings.ActiveTab.Title%>" src="<%= GetPageIcon() %>">
                <div class="text_title">
                	<b><%= DNNHelper.CurrentPortalSettings.ActiveTab.Title %></b>
                </div>
            </div>
            <div class="sub_content">
                <div id="ContentPane" runat="server"></div>
            </div>
        </div> 
        <%--<div class="center_right_column">
            <div id="ContentPane" runat="server"></div>
        </div>--%><!--- end center_column --->
        
        <div style="clear:left;"></div>
    </div><!--- end content --->
    
    <%--Mobile Sub-Footer--%>
    <dnn:SubFooter runat="server" ID="SubFooter" />
    
    <%--Footer--%>
    <div class="footer">
    	<div class="footer_container" id="BottomPane" runat="server">
        </div>
    </div>
    <%--End Footer--%>
</div>

<dnn:BottomScripts runat="server" ID="BottomScripts" />

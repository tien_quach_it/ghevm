﻿<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/Header.ascx" TagPrefix="dnn" TagName="Header" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/BottomScripts.ascx" TagPrefix="dnn" TagName="BottomScripts" %>
<%@ Register Src="~/Portals/0/Skins/GheVM/Controls/SubFooter.ascx" TagPrefix="dnn" TagName="SubFooter" %>


<script runat="server">
    Function GetPageIcon() As String
        If String.IsNullOrEmpty(DNNHelper.CurrentPortalSettings.ActiveTab.IconFileLarge) Then
            Return "/Portals/0/ico_about.png"
        End If
        Return DNNHelper.CurrentPortalSettings.ActiveTab.IconFileLarge
    End Function
</script>

<div class="wrapper push">
    <%--Header--%>
    <dnn:Header runat="server" id="Header" />
    <!--- End Header ---->

    <%--<div class="clr"></div>--%>
    <div class="full-content">
        <div class="sub_title">
            <img alt="<%= DNNHelper.CurrentPortalSettings.ActiveTab.Title%>" src="<%= GetPageIcon() %>">
            <div class="text_title">
                <b class="page-title"><%= DNNHelper.CurrentPortalSettings.ActiveTab.Title %></b>
            </div>
        </div>
        <div class="clr"></div>
        <div class="sub_content">
            <div id="ContentPane" runat="server"></div>
        </div>
    </div><!--- end content --->
    
    <%--Mobile Sub-Footer--%>
    <dnn:SubFooter runat="server" ID="SubFooter" />
    
    <%--Footer--%>
    <div class="footer">
    	<div class="footer_container" id="BottomPane" runat="server">
        </div>
    </div>
    <%--End Footer--%>
</div>

<dnn:BottomScripts runat="server" ID="BottomScripts" />

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CustomPagerControl : System.Web.UI.UserControl
{
    const string vsCurrentPageIndex = "CurrentPageIndex";
    const string vsPageCount = "PageCount";

    public int TotalPages
    {
        get
        {
            if (ViewState["TotalPages"] == null)
                return 0;
            return (int)ViewState["TotalPages"];
        }
        set
        {
            ViewState["TotalPages"] = value;
        }
    }

    public bool IsLastPage
    {
        get
        {
            return CurrentPageIndex >= TotalPages;
        }
    }

    public bool IsFirstPage
    {
        get
        {
            return CurrentPageIndex <= 1;
        }
    }

    public int CurrentPageIndex
    {
        get
        {
            if (ViewState[vsCurrentPageIndex] == null)
                return 1;
            return (int)ViewState[vsCurrentPageIndex];
        }
        set
        {
            ViewState[vsCurrentPageIndex] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            txtPage.Text = "1";
            CssGoBox = "pageBox";
        }
    }

    protected void btnJump_Click(object sender, EventArgs e)
    {
        if (((Page)HttpContext.Current.Handler).IsValid && txtPage.Text.Trim() != "")
        {
            if (Convert.ToInt32(txtPage.Text) <= TotalPages && Convert.ToInt32(txtPage.Text) >= 1)
            {
                CurrentPageIndex = Convert.ToInt32(txtPage.Text);
                OnPageChanged(new EventArgs()); 
            }
        }
    }

    protected void btnNext_OnClick(object source, EventArgs e)
    {
        CurrentPageIndex++;
        OnPageChanged(new EventArgs());
    }

    protected void btnPrevious_OnClick(object source, EventArgs e)
    {
        CurrentPageIndex--;
        OnPageChanged(new EventArgs());
    }

    protected void btnFirst_OnClick(object source, EventArgs e)
    {
        CurrentPageIndex = 1;
        OnPageChanged(new EventArgs());
    }

    protected void btnLast_OnClick(object source, EventArgs e)
    {
        CurrentPageIndex = TotalPages;
        OnPageChanged(new EventArgs());
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (this.IsFirstPage)
        {
            btnPrevious.Enabled = false;           
            btnFirst.Enabled = false;
        }
        else
        {
            btnPrevious.Enabled = true;            
            btnFirst.Enabled = true;
        }
        if (this.IsLastPage)
        {
            btnNext.Enabled = false;
            btnLast.Enabled = false;
        }
        else
        {
            btnNext.Enabled = true;
            btnLast.Enabled = true;
        }
        txtPage.Text = CurrentPageIndex.ToString();
        lblTotalPageTop.Text = TotalPages.ToString();
    }

    protected virtual void OnPageChanged(EventArgs e)
    {
        if (PageChanged != null)
        {
            PageChanged(this, e);

        }
    }

    public string CssClass
    {
        get
        {
            if (ViewState["CssClass"] == null)
                return "t_paging";
            return ViewState["CssClass"].ToString();
        }
        set
        {
            ViewState["CssClass"] = value;
        }
    }

    public string CssGoBox
    {
        get
        {
            return txtPage.CssClass;
        }
        set
        {
            txtPage.CssClass = value;
        }
    }

    public event EventHandler PageChanged;

}

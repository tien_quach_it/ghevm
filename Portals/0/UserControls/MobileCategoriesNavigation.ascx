﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MobileCategoriesNavigation.ascx.cs" Inherits="Portals_0_UserControls_MobileCategoriesNavigation" %>
<div class="title-bar"> <p> Danh Mục Sản Phẩm </p> </div>
<ul class="megamenu" id="megamenu">
    <asp:Repeater runat="server" ID="rptCategories" OnItemDataBound="rptCategories_ItemDataBound">
        <ItemTemplate>
            <li class="level0"> 
                <a class="level0" href="<%# UrlHelper.GetCategoryListingsUrl(Eval("ID"), Eval("SeoUrl")) %>"> <%# Eval("CategoryName") %> <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' Visible="False"></asp:Label> </a>
                <%--Submenu--%>
                <asp:Repeater runat="server" ID="rptChildren" Visible="False">
                    <HeaderTemplate>
                        <span class="icon-arrow-right-generic childIndicator"></span>
                        <div class="sub">
                            <ul>
                                <li class="back-button"><a href="javascript:void(0);" class="mobileMenuBackBtn"><i class="icon-arrow-left-generic"></i>Back</a></li>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li class="subheader"> <a href="<%# UrlHelper.GetCategoryListingsUrl(Eval("ID"), Eval("SeoUrl")) %>"> <%# Eval("CategoryName") %> </a> </li>
                    </ItemTemplate>
                    <FooterTemplate>
                            </ul>
                        </div>
                    </FooterTemplate>
                    <SeparatorTemplate>
                        <li class="separator"></li>
                    </SeparatorTemplate>
                </asp:Repeater>
            
            </li>
        </ItemTemplate>
    </asp:Repeater>
    
    <%--Online Catalogue--%>
    <%--<li class="level0"> <a class="level0" href="/Catalogue.aspx"> View Catalogue Online </a></li>--%>
</ul>

<ul class="topMobileOtherNavs visible-xs contactWrapM">
    <li><a href="/Catalogue.aspx"><span class="icon-get-directions"></span><span>Xem Catalogue Online</span><span class="icon-arrow-right-generic"></span></a></li>
    <%--<li><a href="/About-Us/Contact-Us"><span class="icon-contact"></span><span>Contact Us</span><span class="icon-arrow-right-generic"></span></a></li>--%>
</ul>
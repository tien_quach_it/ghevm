using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CustomPagerControl2 : System.Web.UI.UserControl
{
    const string vsCurrentPageIndex = "vsCurrentPageIndex";
    const string vsPageCount = "vsPageCount";
    const string vsPagesShown = "vsPagesShown";
    const string vsCurrentPageGroup = "vsCurrentPageGroup";

    public int TotalPages
    {
        get
        {
            if (ViewState["TotalPages"] == null)
                return 0;
            return (int)ViewState["TotalPages"];
        }
        set
        {
            ViewState["TotalPages"] = value;
        }
    }   

    public int CurrentPageIndex
    {
        get
        {
            if (ViewState[vsCurrentPageIndex] == null)
                return 1;
            return (int)ViewState[vsCurrentPageIndex];
        }
        set
        {
            ViewState[vsCurrentPageIndex] = value;
        }
    }

    public int CurrentPageGroup
    {
        get
        {
            if (ViewState[vsCurrentPageGroup] == null)
                return 1;
            return (int)ViewState[vsCurrentPageGroup];
        }
        set
        {
            ViewState[vsCurrentPageGroup] = value;
        }
    }

    public int PagesShown
    {
        get
        {
            if (ViewState[vsPagesShown] == null)
                return 10;
            return (int)ViewState[vsPagesShown];
        }
        set
        {
            ViewState[vsPagesShown] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack) {
        //    txtPage.Text = "1";
        //}
    }

    protected void btnJump_Click(object sender, EventArgs e)
    {
        if (((Page)HttpContext.Current.Handler).IsValid)
        {
            int page = Convert.ToInt32((sender as LinkButton).Text);
            if (page <= TotalPages && page >= 1)
            {
                CurrentPageIndex = page;
                OnPageChanged(new EventArgs());
            }
        }
    }

    protected string GetActiveCss(object pageIndex)
    {
        if (Convert.ToInt32(pageIndex) == CurrentPageIndex)
            return "active";
        return "";
    }

    protected bool GetEnable(object pageIndex)
    {
        return Convert.ToInt32(pageIndex) != CurrentPageIndex;            
    }

    protected void btnNext_OnClick(object source, EventArgs e)
    {
        CurrentPageGroup++;
        CurrentPageIndex = (CurrentPageGroup - 1) * PagesShown + 1;
        OnPageChanged(new EventArgs());
    }

    protected void btnPrevious_OnClick(object source, EventArgs e)
    {
        CurrentPageGroup--;
        CurrentPageIndex = CurrentPageGroup * PagesShown;
        OnPageChanged(new EventArgs());
    }

    protected int NoOfPages;
    void CreatePager()
    {
        DataTable tblPage = new DataTable();
        tblPage.Columns.Add(new DataColumn("PageIndex"));
        DataRow r;

        NoOfPages = Convert.ToInt32(Math.Ceiling(TotalPages / Convert.ToDouble(PagesShown)));        
        for (int i = 0; i < NoOfPages; i++)
        {
            r = tblPage.NewRow();
            r["PageIndex"] = i;
            tblPage.Rows.Add(r);
        }
        rptPage.DataSource = tblPage;
        rptPage.DataBind();
    }

    Repeater rptPageIndex;
    Label lblPage;
    LinkButton btnNext, btnPrev;
    int pageGroup;
    bool isCurrentPageGroup;
    protected void rptPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            rptPageIndex = e.Item.FindControl("rptPageIndex") as Repeater;
            lblPage = e.Item.FindControl("lblPage") as Label;
            btnPrev = e.Item.FindControl("btnPrev") as LinkButton;
            btnNext = e.Item.FindControl("btnNext") as LinkButton;
            
            pageGroup = Convert.ToInt32(lblPage.Text);

            rptPageIndex.DataSource = GetPageIndexes(pageGroup);
            rptPageIndex.DataBind();

            // Increase 1 to correct the index
            pageGroup = pageGroup + 1;

            isCurrentPageGroup = (pageGroup == CurrentPageGroup);
            rptPageIndex.Visible = isCurrentPageGroup;
            btnPrev.Visible = isCurrentPageGroup && (pageGroup != 1);
            btnNext.Visible = isCurrentPageGroup && (pageGroup < NoOfPages); 
            
        }
    }

    DataTable GetPageIndexes(int page)
    {
        DataTable tblPageIndexes = new DataTable();
        DataRow r;
        int index;
        tblPageIndexes.Columns.Add(new DataColumn("Index"));
        for (int i = 1; i <= PagesShown; i++)
        {
            r = tblPageIndexes.NewRow();
            index = i + page * PagesShown;
            if (index > TotalPages)
                break;
            r["Index"] = index;
            tblPageIndexes.Rows.Add(r);
        }
        return tblPageIndexes;
    }
    
    int PageIndex;
    protected int SetPageIndex(object pageIndex)
    {
        PageIndex = Convert.ToInt32(pageIndex);
        return PageIndex;
    }    

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        CreatePager();
        
    }

    protected virtual void OnPageChanged(EventArgs e)
    {
        if (PageChanged != null)
        {
            PageChanged(this, e);
        }
    }

    public event EventHandler PageChanged;

    public string CssClass
    {
        get
        {
            if (ViewState["CssClass"] == null)
                return "l_paging";
            return ViewState["CssClass"].ToString();
        }
        set
        {
            ViewState["CssClass"] = value;
        }
    }
}

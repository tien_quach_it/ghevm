﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Paging.ascx.cs" Inherits="CustomPagerControl" %>
 <script type="text/javascript" language="javascript">
    function noNumbers(e)
    {
        var keynum;
        var keychar;
        var numcheck;
        if(window.event) // IE
        {
            keynum = e.keyCode;
        }
        else if(e.which) // Netscape/Firefox/Opera
        {
            keynum = e.which;
        }
        else // Phim chuc nang ben FireFox
        {
            return true;
        }
        if(keynum>=48&&keynum<=57)
            return true;
        if(keynum==45 || keynum==46)
            return true;
        if(keynum>=65&&keynum<=90)
            return true;
        if(keynum==8)
            return true;
        return false;
    }
</script>
<table>
    <tr>
        <td align="center" style="padding-top: 5px; padding-bottom: 3px;">
            <table cellpadding="2" cellspacing="2" class="<%= CssClass %>">
                <tr>
                    <td><%--Trang Đầu--%>
                        <asp:LinkButton runat="server"  ID="btnFirst" ToolTip="Trang Đầu" Text=" &nbsp; << &nbsp; " 
                            OnClick="btnFirst_OnClick" /><%--<asp:ImageButton runat="server"  ID="btnFirst" ImageUrl="~/Portals/0/Skins/Website/Images/First.png" ToolTip="First" Height="20px"
                            OnClick="btnFirst_OnClick" />--%></td>
                    <td><%--Trang Trước--%>
                        <asp:LinkButton runat="server"  ID="btnPrevious" ToolTip="Trang Trước" Text=" &nbsp; &nbsp;<&nbsp; &nbsp; " 
                            OnClick="btnPrevious_OnClick" /><%--<asp:ImageButton runat="server"  ID="btnPrevious" ImageUrl="~/Portals/0/Skins/Website/Images/Prev.png" ToolTip="Previous" Height="20px"
                            OnClick="btnPrevious_OnClick" />--%></td>
                    <td>Page</td>
                    <td><asp:TextBox runat="server" onkeypress="return noNumbers(event);" ID="txtPage"
                            Width="30px"></asp:TextBox></td>
                    <td>
                        of <asp:Label runat="server" ID="lblTotalPageTop" /></td>
                    <td>
                        <asp:LinkButton runat="server" 
                            ID="btnJump" Text="Go" OnClick="btnJump_Click" /><%--<asp:ImageButton runat="server" ImageUrl="~/Portals/0/Skins/Website/Images/Go.png"
                            ID="btnJump"  Text="Go" OnClick="btnJump_Click" Height="20px"/>--%></td>
                    <td><%--Trang Sau--%>
                        <asp:LinkButton runat="server" ID="btnNext" ToolTip="Trang Sau" Text=" &nbsp; &nbsp;>&nbsp; &nbsp; " 
                            OnClick="btnNext_OnClick" />
                        <%--<asp:ImageButton runat="server" ID="btnNext" ImageUrl="~/Portals/0/Skins/Website/Images/Next.png" ToolTip="Next" Height="20px"
                            OnClick="btnNext_OnClick" />--%></td>
                    <td><%--Trang Cuối--%>
                        <asp:LinkButton runat="server" ID="btnLast" ToolTip="Trang Cuối" Text=" &nbsp; >> &nbsp; " 
                            OnClick="btnLast_OnClick" /><%--<asp:ImageButton runat="server" ID="btnLast" ImageUrl="~/Portals/0/Skins/Website/Images/Last.png" ToolTip="Last" Height="20px"
                            OnClick="btnLast_OnClick" />--%></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
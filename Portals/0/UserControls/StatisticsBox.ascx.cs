﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portals_0_UserControls_StatisticsBox : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadStatistics();
            LoadCurrentZip();
        }
    }

    private void LoadCurrentZip()
    {
        var zipFolder = Server.MapPath(ImageHelper.GetCatalogueDownloadPath());
        var dir = new DirectoryInfo(zipFolder);
        var file = dir.GetFiles().FirstOrDefault();

        string downloadHtml = "";

        if (file != null)
            downloadHtml = String.Format("<a href='{0}'>Download Catalogue</a>", ResolveUrl(ImageHelper.GetCatalogueDownloadFullPath(file.Name)));

        ltrDownloadCatalogue.Text = downloadHtml;
    }

    private void LoadStatistics()
    {
        lblOnline.Text = "0";

        DataTable statistics = ((IStatistics)this.Page).StatisticsData;
        lblVisits.Text = GetVisitsByType(statistics, 0);
        lblToday.Text = GetVisitsByType(statistics, 1);
        lblYesterday.Text = GetVisitsByType(statistics, 2);
        lblOnline.Text = GetVisitsByType(statistics, 3);
    }

    private string GetVisitsByType(DataTable tbl, int type)
    {
        DataRow[] rs = tbl.Select("TypeID = " + type);
        if (rs.Length > 0)
        {
            return rs[0]["Visits"].ToString();
        }
        return "0";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portals_0_UserControls_CategoriesNavigation : System.Web.UI.UserControl
{
    private List<CategoryEntity> lstCategory = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        lstCategory = LoadCategories();
        rptCategories.DataSource = lstCategory.Where(c => c.ParentID == 0);
        rptCategories.DataBind();
    }

    private List<CategoryEntity> LoadCategories()
    {
        string CacheKey = "CategoriesList";
        if (Cache["CategoriesList"] == null)
        {
            CategoryManager manager = new CategoryManager();
            var categories = manager.GetCategorys().OrderBy(c => c.DisplayOrder).ToList();
            string a = "lock";
            lock (a)
            {
                a = "lock";
                Cache["CategoriesList"] = categories;
            }
        }

        return (List<CategoryEntity>)Cache["CategoriesList"];

    }

    private Repeater rptChildren;
    protected void rptCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            rptChildren = e.Item.FindControl("rptChildren") as Repeater;
            int parentID = Convert.ToInt32((e.Item.FindControl("lblID") as Label).Text);
            var lstChildren = lstCategory.Where(c => c.ParentID == parentID);
            rptChildren.Visible = lstChildren.Count() > 0;
            rptChildren.DataSource = lstChildren.ToList();
            rptChildren.DataBind();
        }
    }
}
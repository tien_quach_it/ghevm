﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StatisticsBox.ascx.cs" Inherits="Portals_0_UserControls_StatisticsBox" %>

<div class="access_statistics_box">
    <div class="title-bar"> <p> Online Catalogue </p> </div>
    <div class="statistics download-sidebar">
        <a href="/Catalogue.aspx"><img src="/portals/0/images/catalogue.jpg" alt="" width="168" border="0" /></a>
        <div class="download-link">
            <a href="/Catalogue.aspx">View Online</a>
        </div>
        <div class="download-link">
            <asp:Literal runat="server" ID="ltrDownloadCatalogue"></asp:Literal>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="access_statistics_box">
    <div class="title-bar"> <p> Thống Kê Truy Cập </p> </div>
    <div class="statistics">
        <ul>
            <li>
                <div class="userolIcon">
                    <img src="/Portals/0/Skins/GheVM/images/ico_useronl.png" alt="ico" />
                </div>
                <div class="userolInfo">
                    <p>Đang Online :</p>
                    <asp:Label ID="lblOnline" runat="server"></asp:Label>
                </div>
            </li>
                        
            <li>
                <div class="oltodayIcon">
                    <img src="/Portals/0/Skins/GheVM/images/ico_today.png" alt="ico" />
                </div>
                <div class="oltodayInfo">
                    <p> Hôm nay :</p>
                    <asp:Label ID="lblToday" runat="server"></asp:Label>
                </div>
            </li>
                        
            <li>
                <div class="olyesterdayIcon">
                    <img src="/Portals/0/Skins/GheVM/images/ico_yesterday.png" alt="ico" />
                </div>
                <div class="olyesterdayInfo">
                    <p> Hôm Qua :</p>
                    <asp:Label ID="lblYesterday" runat="server"></asp:Label>
                </div>
            </li>
                        
            <li>
                <div class="olstatisticsIcon">
                    <img src="/Portals/0/Skins/GheVM/images/ico_statistics.png" alt="ico" />
                </div>
                <div class="olstatisticsInfo">
                    <p> Lượt Truy cập :</p>
                    <asp:Label ID="lblVisits" runat="server"></asp:Label>
                </div> 
            </li>
        </ul> 
        <div class="clear"></div>
    </div>
</div>
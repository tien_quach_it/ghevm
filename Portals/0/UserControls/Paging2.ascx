﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Paging2.ascx.cs" Inherits="CustomPagerControl2" %>
<div class="paging">
    <ul>
        <asp:Repeater ID="rptPage" runat="server" OnItemDataBound="rptPage_ItemDataBound">
            <ItemTemplate>
                <asp:Label ID="lblPage" runat="server" Text='<%# Eval("PageIndex") %>' Visible="false"></asp:Label>
                <li class="ico_prev">
                    <asp:LinkButton ID="btnPrev" runat="server" Text="<< Trang Trước" CssClass='<%# CssClass %>' OnClick="btnPrevious_OnClick"></asp:LinkButton>
                </li>
                <asp:Repeater ID="rptPageIndex" runat="server">
                    <ItemTemplate>
                        <li>
                            <asp:LinkButton ID="btnPage" runat="server" Text='<%# Eval("Index")%>' OnClick="btnJump_Click"
                                CssClass='<%# CssClass + " " + GetActiveCss(Eval("Index")) %>' Enabled='<%# GetEnable(Eval("Index")) %>'></asp:LinkButton>
                        </li>
                        
                    </ItemTemplate>
                </asp:Repeater>
                <li class="ico_next">
                    <asp:LinkButton ID="btnNext" runat="server" Text="Trang Sau >>" CssClass='<%# CssClass %>' OnClick="btnNext_OnClick"></asp:LinkButton>
                </li>
                <%--<li class="select_number">
                    <select>
                        <option> 1 </option>
                        <option> 2 </option>
                        <option> 3 </option>
                        <option> 4 </option>
                        <option> 5 </option>
                        <option> 6 </option>
                        <option> 7 </option>
                        <option> 8 </option>
                        <option> 9 </option>
                    </select>
                    Sản Phẩm / Trang
                </li>--%>
            </ItemTemplate>
        </asp:Repeater> 
    </ul>
</div>

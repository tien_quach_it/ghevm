﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategoriesNavigation.ascx.cs" Inherits="Portals_0_UserControls_CategoriesNavigation" %>
<div class="left_menu">
    <div class="title-bar"> <p> Danh Mục Sản Phẩm </p> </div>
    <div class="categories">
        <ul>
            <asp:Repeater runat="server" ID="rptCategories" OnItemDataBound="rptCategories_ItemDataBound">
                <ItemTemplate>
                    <li> 
                        <a href="<%# UrlHelper.GetCategoryListingsUrl(Eval("ID"), Eval("SeoUrl")) %>"> <%# Eval("CategoryName") %> <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' Visible="False"></asp:Label> </a>
                        <asp:Repeater runat="server" ID="rptChildren" Visible="False">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li> <a href="<%# UrlHelper.GetCategoryListingsUrl(Eval("ID"), Eval("SeoUrl")) %>"> <%# Eval("CategoryName") %> </a> </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                        </li>
                </ItemTemplate>
            </asp:Repeater>
            <%--<li> <a href="#"> Sản Phẩm Bán Chạy </a> </li>
            <li> <a href="#"> Ghế Lãnh Đạo Cao Cấp </a ></li>
            <li> <a href="#"> Ghế giám đốc </a> 
                <ul>
                    <li> <a href="#"> Ghế giám đốc cao cấp </a> </li>
                    <li> <a href="#"> Ghế giám đốc trung cấp </a> </li>
                </ul><!--- sub menu ---->
            </li>
            <li> <a href="#"> Ghế trưởng phòng </a> 
                <ul>
                    <li> <a href="#"> Ghế trưởng phòng cao cấp </a> </li>
                    <li> <a href="#"> Ghế trưởng phòng trung cấp </a> </li>
                </ul><!--- sub menu ---->
            </li>
            --%>
        </ul>
    </div><!--- end categories --->
</div><!--- end left_menu --->
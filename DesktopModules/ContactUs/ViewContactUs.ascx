﻿<%@ Control Language="C#" Inherits="Philip.Modules.ContactUs.ViewContactUs" CodeFile="ViewContactUs.ascx.cs"
    AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<%--<div class="subContactContent">
    <div>
        <img src="/portals/0/images/contact_banner_new.jpg" alt="banner" />
    </div>
    <center>
        <img src="/portals/0/images/logo.png" alt="logo" />
        <h1>
            Công ty TNHH SX - TM VĂN MINH</h1>
    </center>
    <div class="contact_container">
        <div class="company_info">
            <ul>
                <li>Xưởng: Lô A211-A212, KCN Thái Hòa, Đức Hòa III, Long An</li>
                <li>Điện thoại: 072.375.9677-678 </li>
                <li>Showroom: 926 Nguyễn Chí Thanh, P.4, Q.11, Tp.HCM</li>
                <li>Điện thoại: (08)39.561.340 </li>
                <li>Fax: 072 375 9679 </li>
                <li>Email: vanminh676@vnn.vn </li>
                <li>Website: www.ghevm.com </li>
                <li>Thời gian làm việc: Mỗi ngày từ 8h15 đến 17h (Chủ nhật nghỉ) </li>
                <li></li>
            </ul>
        </div>
        <div class="form">--%>
<asp:Panel ID="pnlForm" runat="server">
    <form class="form-horizontal">
        <h4>Liên Hệ</h4>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Họ Tên*</label>
            <div class="col-sm-9"><asp:TextBox ID="txtName" CssClass="form-control" runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                ControlToValidate="txtName" Display="Dynamic"
                ErrorMessage="*" ForeColor="White"></asp:RequiredFieldValidator></div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Địa Chỉ*</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtAddress" CssClass="form-control" runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                    ControlToValidate="txtAddress" Display="Dynamic"
                    ErrorMessage="*" ForeColor="White"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Email*</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                    ControlToValidate="txtEmail" Display="Dynamic"
                    ErrorMessage="*" ForeColor="White"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="SomeID"
                        runat="server"
                        ControlToValidate="txtEmail" Display="Dynamic"
                        ErrorMessage="*"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        EnableClientScript="true" ForeColor="White" />
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Điện Thoại*</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                    ControlToValidate="txtPhone" Display="Dynamic"
                    ErrorMessage="*" ForeColor="White"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Tiêu Đề*</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtTitle" CssClass="form-control" runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                    ControlToValidate="txtTitle" Display="Dynamic"
                    ErrorMessage="*" ForeColor="White"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Nội Dung*</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtMessage" CssClass="form-control" TextMode="MultiLine" Rows="5" runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                    ControlToValidate="txtMessage" Display="Dynamic"
                    ErrorMessage="*" ForeColor="White"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <asp:Button ID="btnSubmit" runat="server" Text="Gửi" CssClass="contact-button btn btn-default" OnClick="btnSubmit_Click" />
            </div>
        </div>
    </form>
</asp:Panel>
<asp:Panel ID="pnlSuccess" runat="server" Visible="false">
    <div class="contactUsResult">
        Cám ơn bạn đã quan tâm, chúng tôi sẽ liên lạc với bạn sớm nhất khi có thể.
    </div>
</asp:Panel>
<%--</div>
        <div class="clear">
        </div>
    </div>
</div>--%>

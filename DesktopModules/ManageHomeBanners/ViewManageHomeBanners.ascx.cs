﻿/*
' DotNetNuke® - http://www.dotnetnuke.com
' Copyright (c) 2002-2006
' by Perpetual Motion Interactive Systems Inc. ( http://www.perpetualmotion.ca )
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
' documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
' the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
' to permit persons to whom the Software is furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial portions 
' of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
' DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;

using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;

namespace Philip.Modules.ManageHomeBanners
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewManageHomeBanners class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewManageHomeBanners : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                lblErr.Text = "";
                if (!IsPostBack)
                {
                    IsAddNew = false;
                    vws.ActiveViewIndex = 0;
                    Initialize();
                    ResetAllSort();
                    BindGrid();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region Generated

        void Initialize()
        {
            LoadHomeBannerSetting();
        }

        public List<HomeBannerViewEntity> LoadData()
        {
            try
            {
                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_HomeBannerWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString);

                List<HomeBannerViewEntity> lst = results.GetResult<HomeBannerViewEntity>().ToList();
                int NumRows = results.GetResult<int>().Single();

                // Paging

                int totalpage = NumRows / PageSize;
                if (NumRows % PageSize > 0)
                    totalpage = totalpage + 1;
                iexPaging.TotalPages = totalpage;
                iexPaging.Visible = totalpage > 1;

                return lst;
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
                return null;
            }
        }

        bool hasNoRow;
        public void BindGrid()
        {
            try
            {
                var lst = LoadData();
                rpt.DataSource = lst;
                rpt.DataBind();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        private bool IsAddNew
        {
            get
            {
                object o = Session["IsAddNew"];
                if (o != null)
                {
                    return Convert.ToBoolean(o);
                }
                return false;
            }
            set { Session["IsAddNew"] = value; }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                int editindex = Convert.ToInt32(lnk.CommandArgument);
                EditID = editindex;
                vws.ActiveViewIndex = 1;

                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_HomeBannerWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString);

                HomeBannerViewEntity obj = results.GetResult<HomeBannerViewEntity>().Where(o => o.ID == EditID).First();
                int count = results.GetResult<int>().Single();

                if (String.IsNullOrEmpty(obj.Picture))
                    imgPicture.Visible = false;
                else
                {
                    imgPicture.ImageUrl = ImageHelper.GetHomeBannerImageFullPath(obj.Picture);
                    imgPicture.Visible = true;
                }

                txtLink.Text = obj.Link;
                txtDisplayOrder.Text = obj.DisplayOrder.ToString();

            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = sender as LinkButton;
                int id = Convert.ToInt32(lnk.CommandArgument);
                //Delete
                HomeBannerManager dal = new HomeBannerManager();
                //dal.Delete(id);

                HomeBannerEntity obj = dal.GetHomeBannerById(id);
                obj.Deleted = true;
                dal.Update(obj);

                //EndDelete
                BindGrid();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            EditID = 0;
            ClearForm();
            SetNextDisplayOrder();
            vws.ActiveViewIndex = 1;
        }

        private void SetNextDisplayOrder()
        {
            txtDisplayOrder.Text = DBHelper.ExecuteScalar("select IsNull(max(DisplayOrder), 0) + 1 from HomeBanner where Deleted = 0");
        }

        bool ValidateForm()
        {
            // DisplayOrder
            if (String.IsNullOrEmpty(txtDisplayOrder.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập STT!";
                return false;
            }
            Int32 tmpDisplayOrder;
            if (!Int32.TryParse(txtDisplayOrder.Text.Trim(), out tmpDisplayOrder))
            {
                lblErr.Text = "STT không hợp lệ!";
                return false;
            }


            return true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            HomeBannerManager dal = new HomeBannerManager();
            HomeBannerEntity obj;

            if (EditID == 0)
                obj = new HomeBannerEntity();
            else
            {
                obj = dal.GetHomeBannerById(EditID);
            }

            if (fupPicture.HasFile)
            {
                // Delete old file
                if (!String.IsNullOrEmpty(obj.Picture))
                {
                    string oldFile = String.Format("{0}{1}", Server.MapPath(ImageHelper.GetHomeBannerImagePath()), obj.Picture);
                    if (File.Exists(oldFile))
                        File.Delete(oldFile);
                }
                // Upload new file
                obj.Picture = DNNHelper.UploadFile(fupPicture.PostedFile, Server.MapPath(ImageHelper.GetHomeBannerImagePath()));
            }

            if (txtLink.Text.StartsWith("/"))
                obj.Link = txtLink.Text;
            else
            {
                if (txtLink.Text != "")
                    obj.Link = txtLink.Text.Contains("http") ? txtLink.Text : "http://" + txtLink.Text;
                else
                    obj.Link = "";
            }

            obj.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text);


            if (EditID == 0)
                dal.Insert(obj);
            else
            {
                obj.IsStaticShow = obj.IsStaticShow;
                dal.Update(obj);
            }
            vws.ActiveViewIndex = 0;

            BindGrid();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            vws.ActiveViewIndex = 0;
        }

        void ClearForm()
        {
            imgPicture.Visible = false;
            txtLink.Text = "";
            txtDisplayOrder.Text = "";

        }

        protected int EditID
        {
            get
            {
                if (ViewState["EditID"] == null)
                    return 0;
                return Convert.ToInt32(ViewState["EditID"]);
            }
            set
            {
                ViewState["EditID"] = value;
            }
        }

        LinkButton btnSort;
        protected void btnSort_Click(object sender, EventArgs e)
        {
            ResetAllSort();

            btnSort = (sender as LinkButton);
            SortString = String.Format("{0} {1}", btnSort.CommandArgument, btnSort.CommandName);
            if (String.IsNullOrEmpty(btnSort.CommandName))
            {
                btnSort.CssClass = "sortAsc";
                btnSort.CommandName = "Desc";
            }
            else
            {
                btnSort.CssClass = "sortDesc";
                btnSort.CommandName = "";
            }

            BindGrid();
        }

        void ResetAllSort()
        {
            btnSortPicture.CssClass = "sortNone";
            btnSortLink.CssClass = "sortNone";
            btnSortIsStaticShow.CssClass = "sortNone";

            //btnSortID.CssClass = "sortNone";            
        }

        protected string SortString
        {
            get
            {
                if (ViewState["SortString"] == null)
                    return "DisplayOrder";
                return ViewState["SortString"].ToString();
            }
            set
            {
                ViewState["SortString"] = value;
            }
        }

        protected void iexPaging_PageChanged(object sender, EventArgs e)
        {
            CurrentPage = iexPaging.CurrentPageIndex - 1;

            BindGrid();
        }

        protected int PageSize
        {
            get
            {
                return 20;
            }
        }

        protected int CurrentPage
        {
            get
            {
                if (ViewState["CurPage"] == null)
                    ViewState["CurPage"] = 0;
                return Convert.ToInt32(ViewState["CurPage"]);
            }
            set
            {
                ViewState["CurPage"] = value;
            }
        }

        protected void OnCheckedChanged_Click(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            int editindex = Convert.ToInt32(chk.ToolTip);

            //Reset all Static show to false
            DBHelper.ExecuteSQL(String.Format("Update HomeBanner set IsStaticShow = 0"));

            //Set this checked to Static show
            if (chk.Checked)
            {
                DBHelper.ExecuteSQL(String.Format("Update HomeBanner set IsStaticShow = 1 Where ID = {0}", editindex));
            }
            BindGrid();
        }

        protected void lnkSettingUpdate_Click(object sender, EventArgs e)
        {
            HomeBannerSettingEntity obj;
            HomeBannerSettingManager dal = new HomeBannerSettingManager();
            string id = DBHelper.ExecuteScalar("Select top 1 id from HomeBannerSetting");
            if (id == "")
            {
                obj = new HomeBannerSettingEntity();
                obj.Duration = txtDuration.Text;
                obj.IsStatic = Convert.ToBoolean(chkIsStatic.Checked);
                dal.Insert(obj);
            }
            else
            {
                obj = dal.GetHomeBannerSettingById(Convert.ToInt32(id));
                obj.Duration = txtDuration.Text;
                obj.IsStatic = Convert.ToBoolean(chkIsStatic.Checked);
                dal.Update(obj);
            }
            LoadHomeBannerSetting();
        }
        protected void LoadHomeBannerSetting()
        {
            HomeBannerSettingEntity obj;
            HomeBannerSettingManager dal = new HomeBannerSettingManager();
            obj = dal.GetHomeBannerSettings().FirstOrDefault();
            if (obj != null)
            {
                txtDuration.Text = obj.Duration;
                chkIsStatic.Checked = obj.IsStatic;
            }
        }
        #endregion
    }
}


﻿<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Settings.ascx.cs" Inherits="Philip.Modules.ManageHomeBanners.Settings" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<div class="mspsContent dnnClear">
    <h2 class="dnnFormSectionHead">
        <a class="dnnSectionExpanded" href="">Home Banner Setting</a></h2>
    <fieldset style="display: block;">
        <div class="dnnFormItem">
            <div class="dnnTooltip">
                Thời gian chuyển động ảnh(s)
            </div>
            <asp:TextBox ID="txtDuration" runat="server"></asp:TextBox>
        </div>
        <div class="dnnFormItem">
            <div class="dnnTooltip">
                Chỉ hiện 1 banner được chọn:
            </div>
            <asp:CheckBox ID="chkStatic" runat="server" />
        </div>        
    </fieldset>    
</div>
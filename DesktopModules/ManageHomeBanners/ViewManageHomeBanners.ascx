﻿<%@ Control Language="C#" Inherits="Philip.Modules.ManageHomeBanners.ViewManageHomeBanners" CodeFile="ViewManageHomeBanners.ascx.cs" AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<script type="text/javascript">
    $(function () {
        $('.hpLink').click(function () {
            $(".hpSetting").slideToggle("slow");
            return false;
        });
    });
</script>
<div style="padding: 10px 10px 10px 5px">
    <a href="#" class="hpLink button">Cài Đặt Banner</a>
    <div class="hpSetting" style="text-align: left"><%--display: none; --%>
        <table cellpadding="3" cellspacing="3" class="t_admintable">
            <tr>
                <th>
                    Chỉ hiển thị 1 banner được chọn?
                </th>
                <td>
                    <asp:CheckBox ID="chkIsStatic" runat="server" />
                </td>
            </tr>
            <tr>
                <th>
                    Thời gian chuyển động của banner(s):
                </th>
                <td>
                    <asp:TextBox ID="txtDuration" runat="server"></asp:TextBox>
                </td>
            </tr>            
            <tr>
                <td></td>
                <td><asp:LinkButton ID="lnkSettingUpdate" Text="Cập Nhật" runat="server" CssClass="button" OnClick="lnkSettingUpdate_Click"></asp:LinkButton></td>
            </tr>
        </table>
    </div>
</div>
<div>
    <asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red"></asp:Label>
    <asp:MultiView ID="vws" runat="server">
        <asp:View ID="vwView" runat="server">
            <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkAddNew" Text="Thêm Mới" runat="server" CssClass="button" OnClick="lnkAddNew_Click"></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                <tr class="t_admin_table_header">
                    <th></th>
                    <th></th>
                    <th>
                        <asp:LinkButton ID="btnSortDisplayOrder" runat="server" Text="STT" CommandArgument="DisplayOrder" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th>
                        <asp:LinkButton ID="btnSortPicture" runat="server" Text="Banner" CommandArgument="Picture" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th>
                        <asp:LinkButton ID="btnSortLink" runat="server" Text="Link" CommandArgument="Link" OnClick="btnSort_Click"></asp:LinkButton></th>                    
                    <th>
                        <asp:LinkButton ID="btnSortIsStaticShow" runat="server" Text="Hiển Thị Duy Nhất" CommandArgument="IsStaticShow" OnClick="btnSort_Click"></asp:LinkButton></th>

                </tr>
                <asp:Repeater ID="rpt" runat="server">
                    <ItemTemplate>
                        <tr class="row<%# Container.ItemIndex % 2 %>">
                            <td>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Chỉnh Sửa" OnClick="lnkEdit_Click" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton></th>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>'
                                    Text="Xoá" OnClientClick="return confirm('Bạn có muốn xoá banner này hay không?')"
                                    OnClick="lnkDelete_Click"></asp:LinkButton>
                            </td>
                            <td><%# Eval("DisplayOrder") %></td>
                            <td>
                                <img width='80px' src='<%# ResolveUrl(ImageHelper.GetHomeBannerImageFullPath(Eval("Picture").ToString())) %>' alt="" /></td>
                            <td><%# Eval("Link") %></td>
                            <td><asp:CheckBox ID="chkIsStaticShow" runat="server" ToolTip='<%# Eval("ID") %>' AutoPostBack="true" OnCheckedChanged="OnCheckedChanged_Click" Checked='<%# Convert.ToBoolean(Eval("IsStaticShow")) %>' /></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <div>
                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
            </div>
        </asp:View>
        <asp:View ID="vwEdit" runat="server">
            <table cellpadding="2" cellspacing="2" class="t_admintable">
                <tr class='row0'>
                    <td>Banner *</td>
                    <td>
                        <asp:FileUpload ID="fupPicture" runat="server" /><br />
                        <asp:Image ID="imgPicture" runat="server" Width="150px" /></td>
                </tr>
                <tr class='row1'>
                    <td>Link</td>
                    <td>
                        <asp:TextBox ID="txtLink" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr class='row0'>
                    <td>STT *</td>
                    <td>
                        <asp:TextBox ID="txtDisplayOrder" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Lưu Lại" CssClass="button" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Huỷ" CssClass="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;

public partial class DesktopModules_ManageProducts_Popups_ManageProductImages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblErr.Text = "";
        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(ProductID))
                return;

            IsAddNew = false;
            vws.ActiveViewIndex = 0;
            Initialize();
            ResetAllSort();
            BindGrid();
        }
    }

    #region Generated

    void Initialize()
    {
        LoadProductInfo();
    }

    public List<ProductImageEntity> LoadData()
    {
        try
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProductImageWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString, ProductID);

            List<ProductImageEntity> lst = results.GetResult<ProductImageEntity>().ToList();
            int NumRows = results.GetResult<int>().Single();

            // Paging

            int totalpage = NumRows / PageSize;
            if (NumRows % PageSize > 0)
                totalpage = totalpage + 1;
            iexPaging.TotalPages = totalpage;
            iexPaging.Visible = totalpage > 1;

            return lst;
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
            return null;
        }
    }

    bool hasNoRow;
    public void BindGrid()
    {
        try
        {
            var lst = LoadData();
            rpt.DataSource = lst;
            rpt.DataBind();
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    private bool IsAddNew
    {
        get
        {
            object o = Session["IsAddNew"];
            if (o != null)
            {
                return Convert.ToBoolean(o);
            }
            return false;
        }
        set { Session["IsAddNew"] = value; }
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            int editindex = Convert.ToInt32(lnk.CommandArgument);
            EditID = editindex;
            vws.ActiveViewIndex = 1;

            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProductImageWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString, ProductID);

            ProductImageEntity obj = results.GetResult<ProductImageEntity>().Where(o => o.ID == EditID).First();
            int count = results.GetResult<int>().Single();


            if (!String.IsNullOrEmpty(obj.Picture))
            {
                imgPicture.Visible = true;
                //btnRemovePicture.Visible = true;
                imgPicture.ImageUrl = UrlHelper.GetProductThumbUrl(obj.Picture, 220); //ImageHelper.GetProductImageFullPath(obj.Picture);
            }
            else
            {
                imgPicture.Visible = false;
                //btnRemovePicture.Visible = false;
            }

            txtDisplayOrder.Text = obj.DisplayOrder.ToString();

        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = sender as LinkButton;
            int id = Convert.ToInt32(lnk.CommandArgument);

            //Delete
            ProductImageManager dal = new ProductImageManager();
            ProductImageEntity obj = dal.GetProductImageById(id);
            dal.Delete(id);

            // Delete old file
            if (!String.IsNullOrEmpty(obj.Picture))
            {
                string imagePath = Server.MapPath(ImageHelper.GetProductImageFullPath(obj.Picture));
                if (File.Exists(imagePath))
                    File.Delete(imagePath);
            }
       
            //EndDelete
            BindGrid();
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        EditID = 0;
        ClearForm();
        SetDefaultValues();
        vws.ActiveViewIndex = 1;
    }

    private void SetDefaultValues()
    {
        MappingStoredProcedure dal = new MappingStoredProcedure();
        txtDisplayOrder.Text = dal.SP_GetProductImageNextDisplayOrder(ProductID);
    }

    bool ValidateForm()
    {
        if (EditID == 0 && !fupPicture.HasFile)
        {
            lblErr.Text = "Vui lòng nhập Hình Ảnh!";
            return false;
        }

        if (String.IsNullOrEmpty(txtDisplayOrder.Text.Trim()))
        {
            lblErr.Text = "Vui lòng nhập STT!";
            return false;
        }
        Int32 tmpDisplayOrder;
        if (!Int32.TryParse(txtDisplayOrder.Text.Trim(), out tmpDisplayOrder))
        {
            lblErr.Text = "STT không hợp lệ!";
            return false;
        }

        return true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!ValidateForm())
        {
            return;
        }

        ProductImageManager dal = new ProductImageManager();
        ProductImageEntity obj;

        if (EditID == 0)
        {
            obj = new ProductImageEntity();
            obj.Picture = "";
        }
        else
        {
            obj = dal.GetProductImageById(EditID);
        }

        string imagePath = Server.MapPath(ImageHelper.GetProductImagePath());
        if (!Directory.Exists(imagePath))
            Directory.CreateDirectory(imagePath);

        obj.ProductID = Convert.ToInt32(ProductID);

        if (fupPicture.HasFile)
        {
            // Delete old file
            if (!String.IsNullOrEmpty(obj.Picture))
            {
                string oldFile = String.Format("{0}{1}", imagePath, obj.Picture);
                if (File.Exists(oldFile))
                    File.Delete(oldFile);
            }
            // Upload new file
            obj.Picture = DNNHelper.UploadFile(fupPicture.PostedFile, imagePath, "detail");
        }

        obj.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text);

        if (EditID == 0)
            dal.Insert(obj);
        else
            dal.Update(obj);

        vws.ActiveViewIndex = 0;

        BindGrid();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        vws.ActiveViewIndex = 0;
    }

    void ClearForm()
    {
        imgPicture.Visible = false;
        txtDisplayOrder.Text = "";

    }

    protected int EditID
    {
        get
        {
            if (ViewState["EditID"] == null)
                return 0;
            return Convert.ToInt32(ViewState["EditID"]);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }

    LinkButton btnSort;
    protected void btnSort_Click(object sender, EventArgs e)
    {
        ResetAllSort();

        btnSort = (sender as LinkButton);
        SortString = String.Format("{0} {1}", btnSort.CommandArgument, btnSort.CommandName);
        if (String.IsNullOrEmpty(btnSort.CommandName))
        {
            btnSort.CssClass = "sortAsc";
            btnSort.CommandName = "Desc";
        }
        else
        {
            btnSort.CssClass = "sortDesc";
            btnSort.CommandName = "";
        }

        BindGrid();
    }

    void ResetAllSort()
    {
        btnSortPicture.CssClass = "sortNone";
        btnSortDisplayOrder.CssClass = "sortNone";

        //btnSortID.CssClass = "sortNone";            
    }

    protected string SortString
    {
        get
        {
            if (ViewState["SortString"] == null)
                return "DisplayOrder";
            return ViewState["SortString"].ToString();
        }
        set
        {
            ViewState["SortString"] = value;
        }
    }

    protected void iexPaging_PageChanged(object sender, EventArgs e)
    {
        CurrentPage = iexPaging.CurrentPageIndex - 1;

        BindGrid();
    }

    protected int PageSize
    {
        get
        {
            return 20;
        }
    }

    protected int CurrentPage
    {
        get
        {
            if (ViewState["CurPage"] == null)
                ViewState["CurPage"] = 0;
            return Convert.ToInt32(ViewState["CurPage"]);
        }
        set
        {
            ViewState["CurPage"] = value;
        }
    }

    #endregion

    private string ProductID
    {
        get
        {
            return Request.QueryString["ProductID"];
        }
    }

    private void LoadProductInfo()
    {
        ProductManager manager = new ProductManager();
        var product = manager.GetProductById(Convert.ToInt32(ProductID));
        ltrProductName.Text = String.Format("{0} ({1})", product.ProductName, product.ProductCode);
    }
}
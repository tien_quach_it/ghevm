﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManageProductImages.aspx.cs" Inherits="DesktopModules_ManageProducts_Popups_ManageProductImages" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <title>Quản Lý Hình Chi Tiết Sản Phẩm</title>
    <link type="text/css" rel="stylesheet" href="<%= ResolveUrl("~/Portals/0/Skins/GheVM/css/admin.css") %>" />
</head>
<body>
    <form id="form1" runat="server">
        <h1 class="t_title">Hình Ảnh Sản Phẩm <asp:Literal runat="server" ID="ltrProductName"></asp:Literal></h1>
        <div>
            <%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
            <div>
                <asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red" CssClass="error-msg"></asp:Label>
                <asp:MultiView ID="vws" runat="server">
                    <asp:View ID="vwView" runat="server">
                        <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkAddNew" Text="Thêm Mới" runat="server" CssClass="button" OnClick="lnkAddNew_Click"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                            <tr class="t_admin_table_header">
                                <th></th>
                                <th></th>
                                <th>
                                    <asp:LinkButton ID="btnSortDisplayOrder" runat="server" Text="STT" CommandArgument="DisplayOrder" OnClick="btnSort_Click"></asp:LinkButton></th>
                                <th>
                                    <asp:LinkButton ID="btnSortPicture" runat="server" Text="Hình Ảnh" CommandArgument="Picture" OnClick="btnSort_Click"></asp:LinkButton></th>

                            </tr>
                            <asp:Repeater ID="rpt" runat="server">
                                <ItemTemplate>
                                    <tr class="row<%# Container.ItemIndex % 2 %>">
                                        <td>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Chỉnh Sửa" OnClick="lnkEdit_Click" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton></th>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>'
                                                Text="Xóa" OnClientClick="return confirm('Bạn có chắc muốn xóa dòng dữ liệu này không?')"
                                                OnClick="lnkDelete_Click"></asp:LinkButton>
                                        </td>
                                        <td><%# Eval("DisplayOrder") %></td>
                                        <td>
                                            <img width="80px" alt="" src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 220) %>"></td>

                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                        <div>
                            <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
                        </div>
                    </asp:View>
                    <asp:View ID="vwEdit" runat="server">
                        <table cellpadding="2" cellspacing="2" class="t_admintable">
                            <tr class='row0'>
                                <td>Hình Ảnh *</td>
                                <td>
                                    <asp:FileUpload ID="fupPicture" runat="server" /><br />
                                    <asp:Image ID="imgPicture" runat="server" Width="150px" /></td>
                            </tr>
                            <tr class='row1'>
                                <td>STT *</td>
                                <td>
                                    <asp:TextBox ID="txtDisplayOrder" runat="server" Width="400"></asp:TextBox></td>
                            </tr>

                            <tr>
                                <td></td>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Lưu Lại" CssClass="button" OnClick="btnSave_Click" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Hủy" CssClass="button" OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;


public partial class DesktopModules_ManageProducts_Popups_ManageProductColors : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblErr.Text = "";
        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(ProductID))
                return;

            IsAddNew = false;
            vws.ActiveViewIndex = 0;
            Initialize();
            ResetAllSort();
            BindGrid();
        }
    }

    #region Generated

    void Initialize()
    {
        LoadMaterials();
        LoadProductInfo();
    }

    public List<ProductColorViewEntity> LoadData()
    {
        try
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProductColorWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString, ProductID);

            List<ProductColorViewEntity> lst = results.GetResult<ProductColorViewEntity>().ToList();
            int NumRows = results.GetResult<int>().Single();

            // Paging

            int totalpage = NumRows / PageSize;
            if (NumRows % PageSize > 0)
                totalpage = totalpage + 1;
            iexPaging.TotalPages = totalpage;
            iexPaging.Visible = totalpage > 1;

            return lst;
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
            return null;
        }
    }

    bool hasNoRow;
    public void BindGrid()
    {
        try
        {
            var lst = LoadData();
            rpt.DataSource = lst;
            rpt.DataBind();
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    private bool IsAddNew
    {
        get
        {
            object o = Session["IsAddNew"];
            if (o != null)
            {
                return Convert.ToBoolean(o);
            }
            return false;
        }
        set { Session["IsAddNew"] = value; }
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            int editindex = Convert.ToInt32(lnk.CommandArgument);
            EditID = editindex;
            vws.ActiveViewIndex = 1;

            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProductColorWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString, ProductID);

            ProductColorEntity obj = results.GetResult<ProductColorEntity>().Where(o => o.ID == EditID).First();
            int count = results.GetResult<int>().Single();

            if (ddlMaterial.Items.FindByValue(obj.MaterialID.ToString()) != null)
                ddlMaterial.SelectedValue = obj.MaterialID.ToString();
            txtColorName.Text = obj.ColorName;
            if (!String.IsNullOrEmpty(obj.Picture))
            {
                imgPicture.Visible = true;
                //btnRemovePicture.Visible = true;
                imgPicture.ImageUrl = UrlHelper.GetProductThumbUrl(obj.Picture, 220); //ImageHelper.GetProductImageFullPath(obj.Picture);
            }
            else
            {
                imgPicture.Visible = false;
                //btnRemovePicture.Visible = false;
            }


        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = sender as LinkButton;
            int id = Convert.ToInt32(lnk.CommandArgument);
            //Delete
            ProductColorManager dal = new ProductColorManager();
            //dal.Delete(id);

            ProductColorEntity obj = dal.GetProductColorById(id);
            obj.Deleted = true;
            dal.Update(obj);

            // Delete old file
            if (!String.IsNullOrEmpty(obj.Picture))
            {
                string imagePath = Server.MapPath(ImageHelper.GetProductImageFullPath(obj.Picture));
                if (File.Exists(imagePath))
                    File.Delete(imagePath);
            }

            //EndDelete
            BindGrid();
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        EditID = 0;
        ClearForm();
        vws.ActiveViewIndex = 1;
    }

    bool ValidateForm()
    {
        // ColorName
        if (String.IsNullOrEmpty(txtColorName.Text.Trim()))
        {
            lblErr.Text = "Vui lòng nhập Màu Sản Phẩm!";
            return false;
        }

        if (EditID == 0 && !fupPicture.HasFile)
        {
            lblErr.Text = "Vui lòng nhập Hình Ảnh!";
            return false;
        }

        return true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!ValidateForm())
        {
            return;
        }

        ProductColorManager dal = new ProductColorManager();
        ProductColorEntity obj;

        if (EditID == 0)
        {
            obj = new ProductColorEntity();
            obj.Picture = "";
        }
        else
        {
            obj = dal.GetProductColorById(EditID);
        }

        obj.MaterialID = Convert.ToInt32(ddlMaterial.SelectedValue);
        obj.ProductID = Convert.ToInt32(ProductID);
        obj.ColorName = txtColorName.Text;

        string imagePath = Server.MapPath(ImageHelper.GetProductImagePath());
        if (!Directory.Exists(imagePath))
            Directory.CreateDirectory(imagePath);

        if (fupPicture.HasFile)
        {
            // Delete old file
            if (!String.IsNullOrEmpty(obj.Picture))
            {
                string oldFile = String.Format("{0}{1}", imagePath, obj.Picture);
                if (File.Exists(oldFile))
                    File.Delete(oldFile);
            }
            // Upload new file
            obj.Picture = DNNHelper.UploadFile(fupPicture.PostedFile, imagePath, "color");
        }

        if (EditID == 0)
            dal.Insert(obj);
        else
            dal.Update(obj);

        vws.ActiveViewIndex = 0;

        BindGrid();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        vws.ActiveViewIndex = 0;
    }

    void ClearForm()
    {
        ddlMaterial.SelectedIndex = 0;
        txtColorName.Text = "";
        imgPicture.Visible = false;

    }

    protected int EditID
    {
        get
        {
            if (ViewState["EditID"] == null)
                return 0;
            return Convert.ToInt32(ViewState["EditID"]);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }

    LinkButton btnSort;
    protected void btnSort_Click(object sender, EventArgs e)
    {
        ResetAllSort();

        btnSort = (sender as LinkButton);
        SortString = String.Format("{0} {1}", btnSort.CommandArgument, btnSort.CommandName);
        if (String.IsNullOrEmpty(btnSort.CommandName))
        {
            btnSort.CssClass = "sortAsc";
            btnSort.CommandName = "Desc";
        }
        else
        {
            btnSort.CssClass = "sortDesc";
            btnSort.CommandName = "";
        }

        BindGrid();
    }

    void ResetAllSort()
    {
        btnSortColorName.CssClass = "sortNone";
        btnSortPicture.CssClass = "sortNone";
        btnSortMaterial.CssClass = "sortNone";
        //btnSortID.CssClass = "sortNone";            
    }

    protected string SortString
    {
        get
        {
            if (ViewState["SortString"] == null)
                return "ID";
            return ViewState["SortString"].ToString();
        }
        set
        {
            ViewState["SortString"] = value;
        }
    }

    protected void iexPaging_PageChanged(object sender, EventArgs e)
    {
        CurrentPage = iexPaging.CurrentPageIndex - 1;

        BindGrid();
    }

    protected int PageSize
    {
        get
        {
            return 20;
        }
    }

    protected int CurrentPage
    {
        get
        {
            if (ViewState["CurPage"] == null)
                ViewState["CurPage"] = 0;
            return Convert.ToInt32(ViewState["CurPage"]);
        }
        set
        {
            ViewState["CurPage"] = value;
        }
    }

    #endregion

    private string ProductID
    {
        get
        {
            return Request.QueryString["ProductID"];
        }
    }

    private void LoadMaterials()
    {
        MaterialManager manager = new MaterialManager();
        ddlMaterial.DataSource = manager.GetMaterials().ToList();
        ddlMaterial.DataTextField = "MaterialName";
        ddlMaterial.DataValueField = "ID";
        ddlMaterial.DataBind();


    }

    private void LoadProductInfo()
    {
        ProductManager manager = new ProductManager();
        var product = manager.GetProductById(Convert.ToInt32(ProductID));
        ltrProductName.Text = String.Format("{0} ({1})", product.ProductName, product.ProductCode);
    }
}
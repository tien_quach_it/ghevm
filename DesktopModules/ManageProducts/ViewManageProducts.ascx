﻿<%@ Control Language="C#" Inherits="Philip.Modules.ManageProducts.ViewManageProducts" CodeFile="ViewManageProducts.ascx.cs" AutoEventWireup="true" %>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/texteditor.ascx" %>
<div>
    <asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red"></asp:Label>
    <asp:MultiView ID="vws" runat="server">
        <asp:View ID="vwView" runat="server">
            <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                <tr>
                    <td align="center">
                        Danh Mục: <asp:DropDownList runat="server" ID="ddlCategoryFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryFilter_SelectedIndexChanged" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkAddNew" Text="Thêm Mới" runat="server" CssClass="button" OnClick="lnkAddNew_Click"></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                <tr class="t_admin_table_header">
                    <th></th>
                    <th></th>
                    <th>
                        <asp:LinkButton ID="btnSortDisplayOrder" runat="server" Text="STT" CommandArgument="DisplayOrder" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th>
                        <asp:LinkButton ID="btnSortProductName1" runat="server" Text="Hình Ảnh" CommandArgument="ProductName" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <%--<th>
                        <asp:LinkButton ID="btnSortProductName" runat="server" Text="Sản Phẩm" CommandArgument="ProductName" OnClick="btnSort_Click"></asp:LinkButton></th>--%>
                    <th>
                        <asp:LinkButton ID="btnSortProductCode" runat="server" Text="Mã Sản Phẩm" CommandArgument="ProductCode" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <%--<th>
                        <asp:LinkButton ID="btnSortSpecsPicture" runat="server" Text="Hình Ảnh Chi Tiết" CommandArgument="SpecsPicture" OnClick="btnSort_Click"></asp:LinkButton></th>--%>
                    <%--<th>
                        <asp:LinkButton ID="btnSortDescription" runat="server" Text="Description" CommandArgument="Description" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th>
                        <asp:LinkButton ID="btnSortMaterials" runat="server" Text="Materials" CommandArgument="Materials" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th>
                        <asp:LinkButton ID="btnSortWarranty" runat="server" Text="Warranty" CommandArgument="Warranty" OnClick="btnSort_Click"></asp:LinkButton></th>--%>
                    <th>
                        <asp:LinkButton ID="btnSortCategoryName" runat="server" Text="Danh Mục" CommandArgument="CategoryName" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th>
                        <asp:LinkButton ID="btnSortPrice" runat="server" Text="Giá" CommandArgument="Price" OnClick="btnSort_Click"></asp:LinkButton></th>

                </tr>
                <asp:Repeater ID="rpt" runat="server">
                    <ItemTemplate>
                        <tr class="row<%# Container.ItemIndex % 2 %>">
                            <td nowrap>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Chỉnh Sửa" OnClick="lnkEdit_Click" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton></th>
                                <br/><br/>
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>'
                                    Text="Xóa" OnClientClick="return confirm('Bạn có chắc là muốn xóa dữ liệu này không?')"
                                    OnClick="lnkDelete_Click"></asp:LinkButton>
                            </td>
                            <td nowrap>
                                <a href="javascript:void(0)" onclick='dnnModal.show("<%= ResolveUrl("~/DesktopModules/ManageProducts/Popups/ManageProductColors.aspx?ProductID=") %><%# Eval("ID") %>&popUp=true",/*showReturn*/true,550,950,false,"")'>Màu Sắc</a>
                                <br/><br/>
                                <a href="javascript:void(0)" onclick='dnnModal.show("<%= ResolveUrl("~/DesktopModules/ManageProducts/Popups/ManageProductImages.aspx?ProductID=") %><%# Eval("ID") %>&popUp=true",/*showReturn*/true,550,950,false,"")'>Chi Tiết</a>
                                <br/><br/>
                                <a href="javascript:void(0)" onclick='dnnModal.show("<%= ResolveUrl("~/DesktopModules/ManageProducts/Popups/ManageProductSizeImages.aspx?ProductID=") %><%# Eval("ID") %>&popUp=true",/*showReturn*/true,550,950,false,"")'>Kích Thước</a>
                            </td>
                            <td><%# Eval("DisplayOrder") %></td>
                            <td><img width="80px" alt="<%# Eval("ProductCode") %>" src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 220) %>"></td>
                            <%--<td><%# Eval("ProductName") %></td>--%>
                            <td><%# Eval("ProductCode") %></td>
                            <%--<td>
                                <img width='80px' src='<%# ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("SpecsPicture").ToString())) %>' alt="" /></td>
                            <td><%# Eval("Description") %></td>
                            <td><%# Eval("Materials") %></td>
                            <td><%# Eval("Warranty") %></td>--%>
                            
                            <td><%# Eval("CategoryName") %></td>
                            <td><%# Eval("Price") %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <div>
                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
            </div>
        </asp:View>
        <asp:View ID="vwEdit" runat="server">
            <table width="100%" cellpadding="2" cellspacing="2" class="t_admintable">
                <tr class='row1'>
                    <td>STT *</td>
                    <td>
                        <asp:TextBox ID="txtDisplayOrder" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr class='row0'>
                    <td>Sản Phẩm *</td>
                    <td>
                        <asp:TextBox ID="txtProductName" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr class='row1'>
                    <td>Mã Sản Phẩm *</td>
                    <td>
                        <asp:TextBox ID="txtProductCode" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr class='row0'>
                    <td>Danh Mục *</td>
                    <td>
                        <asp:DropDownList ID="ddlCategoryID" runat="server"></asp:DropDownList>
                        <div>
                            <asp:DropDownList ID="ddlCategoryUrl" runat="server"></asp:DropDownList>
                        </div>
                    </td>
                </tr>
                <tr class='row1'>
                    <td>Hình Ảnh *</td>
                    <td>
                        <asp:FileUpload ID="fupPicture" runat="server" /><br />
                        <asp:Image ID="imgPicture" runat="server" Width="150px" /></td>
                </tr>
                <tr class='row0'>
                    <td>Giá Liên Hệ *</td>
                    <td>
                        <asp:TextBox ID="txtPrice" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <%--<tr class='row1'>
                    <td>Hình Kích Thước</td>
                    <td>
                        <asp:FileUpload ID="fupSpecsPicture" runat="server" /><br />
                        <asp:Image ID="imgSpecsPicture" runat="server" Width="150px" /></td>
                </tr>--%>
                <tr class='row1'>
                    <td>Mô Tả Sản Phẩm *</td>
                    <td>
                        <dnn:TextEditor ID="txtDescription" runat="server" Width="500" height="400"></dnn:TextEditor></td>
                </tr>
                <tr class='row0'>
                    <td>Chất liệu *</td>
                    <td>
                        <dnn:TextEditor ID="txtMaterials" runat="server" Width="500" height="400"></dnn:TextEditor></td>
                </tr>
                <tr class='row1'>
                    <td>Màu Sắc *</td>
                    <td>
                        <dnn:TextEditor ID="txtColors" runat="server" Width="500" height="400"></dnn:TextEditor></td>
                </tr>
                <tr class='row0'>
                    <td>Bảo Hành</td>
                    <td>
                        <dnn:TextEditor ID="txtWarranty" runat="server" Width="500" height="400"></dnn:TextEditor></td>
                </tr>
                <tr class='row1'>
                    <td>Là SP Bán Chạy?</td>
                    <td>
                        <asp:CheckBox ID="chkIsBestSell" runat="server"></asp:CheckBox></td>
                </tr>
                <tr class='row0'>
                    <td>Là SP Tiểu Biểu?</td>
                    <td>
                        <asp:CheckBox ID="chkIsFeatured" runat="server"></asp:CheckBox> (Hiển Thị Ở Trang Chủ)</td>
                </tr>
                <tr class='row1'>
                    <td>Là SP Mới?</td>
                    <td>
                        <asp:CheckBox ID="chkIsNew" runat="server"></asp:CheckBox> (Hiển Thị Ở Trang Chủ)</td>
                </tr>
                <tr class='row0'>
                    <td>SEO Url</td>
                    <td>
                        <asp:TextBox ID="txtSeoUrl" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr class='row1'>
                    <td>SEO Title</td>
                    <td>
                        <asp:TextBox ID="txtSeoTitle" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr class='row0'>
                    <td>SEO Keywords</td>
                    <td>
                        <asp:TextBox ID="txtSeoKeywords" runat="server" Width="400" TextMode="MultiLine" Rows="3"></asp:TextBox></td>
                </tr>
                <tr class='row1'>
                    <td>SEO Description</td>
                    <td>
                        <asp:TextBox ID="txtSeoDescription" runat="server" Width="400" TextMode="MultiLine" Rows="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Lưu Lại" CssClass="button" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Hủy" CssClass="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
            
            <script type="text/javascript">
                var txtProductCodeID = '#<%= txtProductCode.ClientID %>';
                var ddlCategoryIDID = '#<%= ddlCategoryID.ClientID %>';
                

                $(function () {
                    $(ddlCategoryIDID).change(function (e) {
                        populateSeoUrl();
                    });

                    $(txtProductCodeID).keyup(function (e) {
                        populateSeoUrl();
                    });

                    $(txtProductCodeID).blur(function (e) {
                        populateSeoUrl();
                    });

                    function populateSeoUrl() {
                        var url = "";

                        var selectedCategory = $(ddlCategoryIDID).val();
                        var categoryUrl = $("#<%= ddlCategoryUrl.ClientID %> option[value='" + selectedCategory + "']").text();
                        url += categoryUrl.replace(".aspx", "/");

                        url = convertToUnsign(url + $(txtProductCodeID).val()) + '.aspx';

                        $('#<%= txtSeoUrl.ClientID %>').val(url);
                    }
                });
            </script>
        </asp:View>
    </asp:MultiView>
</div>

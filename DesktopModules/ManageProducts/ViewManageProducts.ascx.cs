﻿// 
// DotNetNuke® - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;

using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;


namespace Philip.Modules.ManageProducts
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewManageProducts class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewManageProducts : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                lblErr.Text = "";
                if (!IsPostBack)
                {
                    txtSeoUrl.Attributes.Add("readonly", "readonly");

                    IsAddNew = false;
                    vws.ActiveViewIndex = 0;
                    Initialize();
                    ResetAllSort();
                    BindGrid();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region Generated

        void Initialize()
        {
            // CategoryID
            CategoryManager dalCategoryID = new CategoryManager();
            var lstCategoryID = dalCategoryID.GetCategorys().ToList();

            foreach (var category in lstCategoryID)
            {
                if (category.ParentID > 0)
                {
                    category.CategoryName = String.Format("|__{0}", category.CategoryName);
                }
            }

            ddlCategoryID.DataSource = lstCategoryID;
            ddlCategoryID.DataTextField = "CategoryName";
            ddlCategoryID.DataValueField = "ID";
            ddlCategoryID.DataBind();
            ddlCategoryID.Items.Insert(0, new ListItem("", "0"));

            ddlCategoryUrl.DataSource = lstCategoryID;
            ddlCategoryUrl.DataTextField = "SeoUrl";
            ddlCategoryUrl.DataValueField = "ID";
            ddlCategoryUrl.DataBind();
            ddlCategoryUrl.Items.Insert(0, new ListItem("", "0"));

            ddlCategoryFilter.DataSource = lstCategoryID;
            ddlCategoryFilter.DataTextField = "CategoryName";
            ddlCategoryFilter.DataValueField = "ID";
            ddlCategoryFilter.DataBind();
            //ddlCategoryFilter.Items.Insert(0, new ListItem("- Tất Cả -", ""));
        }

        public List<ProductViewEntity> LoadData()
        {
            try
            {
                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_ProductWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString, FilterCategoryID);

                List<ProductViewEntity> lst = results.GetResult<ProductViewEntity>().ToList();
                int NumRows = results.GetResult<int>().Single();

                // Paging

                int totalpage = NumRows / PageSize;
                if (NumRows % PageSize > 0)
                    totalpage = totalpage + 1;
                iexPaging.TotalPages = totalpage;
                iexPaging.Visible = totalpage > 1;

                return lst;
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
                return null;
            }
        }

        bool hasNoRow;
        public void BindGrid()
        {
            try
            {
                var lst = LoadData();
                rpt.DataSource = lst;
                rpt.DataBind();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        private bool IsAddNew
        {
            get
            {
                object o = Session["IsAddNew"];
                if (o != null)
                {
                    return Convert.ToBoolean(o);
                }
                return false;
            }
            set { Session["IsAddNew"] = value; }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                int editindex = Convert.ToInt32(lnk.CommandArgument);
                EditID = editindex;
                vws.ActiveViewIndex = 1;

                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_ProductWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString, FilterCategoryID);

                ProductEntity obj = results.GetResult<ProductEntity>().Where(o => o.ID == EditID).First();
                int count = results.GetResult<int>().Single();

                txtProductName.Text = obj.ProductName;
                txtProductCode.Text = obj.ProductCode;
                if (ddlCategoryID.Items.FindByValue(obj.CategoryID.ToString()) != null)
                    ddlCategoryID.SelectedValue = obj.CategoryID.ToString();
                txtPrice.Text = obj.Price.ToString();

                if (!String.IsNullOrEmpty(obj.Picture))
                {
                    imgPicture.Visible = true;
                    //btnRemovePicture.Visible = true;
                    imgPicture.ImageUrl = UrlHelper.GetProductThumbUrl(obj.Picture, 220); //ImageHelper.GetProductImageFullPath(obj.Picture);
                }
                else
                {
                    imgPicture.Visible = false;
                    //btnRemovePicture.Visible = false;
                }

                //if (!String.IsNullOrEmpty(obj.SpecsPicture))
                //{
                //    imgSpecsPicture.Visible = true;
                //    //btnRemovePicture.Visible = true;
                //    imgSpecsPicture.ImageUrl = UrlHelper.GetProductThumbUrl(obj.Picture, 220); //ImageHelper.GetProductImageFullPath(obj.SpecsPicture);
                //}
                //else
                //{
                //    imgSpecsPicture.Visible = false;
                //    //btnRemovePicture.Visible = false;
                //}

                txtDescription.Text = obj.Description;
                txtMaterials.Text = obj.Materials;
                txtColors.Text = obj.Colors;
                txtWarranty.Text = obj.Warranty;
                txtDisplayOrder.Text = obj.DisplayOrder.ToString();
                chkIsFeatured.Checked = obj.IsFeatured;
                chkIsBestSell.Checked = obj.IsBestSell;
                chkIsNew.Checked = obj.IsNew;

                txtSeoUrl.Text = obj.SeoUrl;
                txtSeoTitle.Text = obj.SeoTitle;
                txtSeoKeywords.Text = obj.SeoKeywords;
                txtSeoDescription.Text = obj.SeoDescription;
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = sender as LinkButton;
                int id = Convert.ToInt32(lnk.CommandArgument);
                //Delete
                ProductManager dal = new ProductManager();
                //dal.Delete(id);

                ProductEntity obj = dal.GetProductById(id);
                obj.Deleted = true;
                dal.Update(obj);

                // Delete old file
                if (!String.IsNullOrEmpty(obj.Picture))
                {
                    string imagePath = Server.MapPath(ImageHelper.GetProductImageFullPath(obj.Picture));
                    if (File.Exists(imagePath))
                        File.Delete(imagePath);
                }

                //EndDelete
                BindGrid();

                SeoHelper.RemoveProductFromSiteUrlsConfig(obj);
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            EditID = 0;
            ClearForm();
            SetDefaultValues();
            vws.ActiveViewIndex = 1;
        }

        bool ValidateForm()
        {
            // ProductName
            if (String.IsNullOrEmpty(txtProductName.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Sản Phẩm!";
                return false;
            }

            // ProductCode
            if (String.IsNullOrEmpty(txtProductCode.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Mã Sản Phẩm!";
                return false;
            }

            if (ddlCategoryID.SelectedIndex == 0)
            {
                lblErr.Text = "Vui lòng chọn Danh Mục!";
                return false;
            }

            if (String.IsNullOrEmpty(txtPrice.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Giá Liên Hệ!";
                return false;
            }
            Double tmpPrice;
            if (!Double.TryParse(txtPrice.Text.Trim(), out tmpPrice))
            {
                lblErr.Text = "Giá Liên Hệ không hợp lệ!";
                return false;
            }

            if (EditID == 0 && !fupPicture.HasFile)
            {
                lblErr.Text = "Vui lòng nhập Hình Ảnh!";
                return false;
            }

            // Description
            if (String.IsNullOrEmpty(txtDescription.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Mô Tả Sản Phẩm!";
                return false;
            }

            // Materials
            if (String.IsNullOrEmpty(txtMaterials.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Chất liệu!";
                return false;
            }

            // Materials
            if (String.IsNullOrEmpty(txtColors.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Màu Sắc!";
                return false;
            }

            // Warranty
            //if (String.IsNullOrEmpty(txtWarranty.Text.Trim()))
            //{
            //    lblErr.Text = "Vui lòng nhập Bảo Hành!";
            //    return false;
            //}

            // DisplayOrder
            if (String.IsNullOrEmpty(txtDisplayOrder.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập STT!";
                return false;
            }
            Int32 tmpDisplayOrder;
            if (!Int32.TryParse(txtDisplayOrder.Text.Trim(), out tmpDisplayOrder))
            {
                lblErr.Text = "STT không hợp lệ!";
                return false;
            }


            return true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            ProductManager dal = new ProductManager();
            ProductEntity obj;

            if (EditID == 0)
            {
                obj = new ProductEntity();
                obj.Picture = "";
                obj.SpecsPicture = "";
            }
            else
            {
                obj = dal.GetProductById(EditID);
            }

            obj.ProductName = txtProductName.Text;
            obj.ProductCode = txtProductCode.Text;
            obj.CategoryID = Convert.ToInt32(ddlCategoryID.SelectedItem.Value);
            obj.Price = Convert.ToDouble(txtPrice.Text);

            string imagePath = Server.MapPath(ImageHelper.GetProductImagePath());
            if (!Directory.Exists(imagePath))
                Directory.CreateDirectory(imagePath);

            if (fupPicture.HasFile)
            {
                // Delete old file
                if (!String.IsNullOrEmpty(obj.Picture))
                {
                    string oldFile = String.Format("{0}{1}", imagePath, obj.Picture);
                    if (File.Exists(oldFile))
                        File.Delete(oldFile);
                }
                // Upload new file
                obj.Picture = DNNHelper.UploadFile(fupPicture.PostedFile, imagePath);
            }

            //if (fupSpecsPicture.HasFile)
            //{
            //    // Delete old file
            //    if (!String.IsNullOrEmpty(obj.SpecsPicture))
            //    {
            //        string oldFile = String.Format("{0}{1}", imagePath, obj.SpecsPicture);
            //        if (File.Exists(oldFile))
            //            File.Delete(oldFile);
            //    }
            //    // Upload new file
            //    obj.SpecsPicture = DNNHelper.UploadFile(fupSpecsPicture.PostedFile, imagePath, "specs");
            //}

            obj.Description = txtDescription.Text;
            obj.Materials = txtMaterials.Text;
            obj.Colors = txtColors.Text;
            obj.Warranty = txtWarranty.Text;
            obj.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text);
            obj.IsFeatured = chkIsFeatured.Checked;
            obj.IsBestSell = chkIsBestSell.Checked;
            obj.IsNew = chkIsNew.Checked;

            bool isNewSeoUrl = (obj.SeoUrl != txtSeoUrl.Text.Trim());
            obj.SeoUrl = txtSeoUrl.Text.Trim();
            obj.SeoTitle = txtSeoTitle.Text.Trim();
            obj.SeoKeywords = txtSeoKeywords.Text.Trim();
            obj.SeoDescription = txtSeoDescription.Text.Trim();

            if (EditID == 0)
                dal.Insert(obj);
            else
                dal.Update(obj);

            vws.ActiveViewIndex = 0;

            BindGrid();

            if (isNewSeoUrl)
                SeoHelper.UpdateSiteUrlsConfig(obj);
        }

        

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            vws.ActiveViewIndex = 0;
        }

        void ClearForm()
        {
            txtProductName.Text = "";
            txtProductCode.Text = "";
            ddlCategoryID.SelectedIndex = 0;
            txtPrice.Text = "";
            imgPicture.Visible = false;
            //imgSpecsPicture.Visible = false;
            txtDescription.Text = "";
            txtMaterials.Text = "";
            txtColors.Text = "";
            txtWarranty.Text = "";
            txtDisplayOrder.Text = "";
            chkIsBestSell.Checked = false;
            chkIsFeatured.Checked = false;
            chkIsNew.Checked = false;

        }

        protected int EditID
        {
            get
            {
                if (ViewState["EditID"] == null)
                    return 0;
                return Convert.ToInt32(ViewState["EditID"]);
            }
            set
            {
                ViewState["EditID"] = value;
            }
        }

        LinkButton btnSort;
        protected void btnSort_Click(object sender, EventArgs e)
        {
            ResetAllSort();

            btnSort = (sender as LinkButton);
            SortString = String.Format("{0} {1}", btnSort.CommandArgument, btnSort.CommandName);
            if (String.IsNullOrEmpty(btnSort.CommandName))
            {
                btnSort.CssClass = "sortAsc";
                btnSort.CommandName = "Desc";
            }
            else
            {
                btnSort.CssClass = "sortDesc";
                btnSort.CommandName = "";
            }

            BindGrid();
        }

        void ResetAllSort()
        {
            //btnSortProductName.CssClass = "sortNone";
            btnSortProductName1.CssClass = "sortNone";
            btnSortProductCode.CssClass = "sortNone";
            //btnSortSpecsPicture.CssClass = "sortNone";
            //btnSortDescription.CssClass = "sortNone";
            //btnSortMaterials.CssClass = "sortNone";
            //btnSortWarranty.CssClass = "sortNone";
            btnSortDisplayOrder.CssClass = "sortNone";
            btnSortCategoryName.CssClass = "sortNone";
            btnSortPrice.CssClass = "sortNone";
            //btnSortID.CssClass = "sortNone";            
        }

        protected string SortString
        {
            get
            {
                if (ViewState["SortString"] == null)
                    return "ID desc";
                return ViewState["SortString"].ToString();
            }
            set
            {
                ViewState["SortString"] = value;
            }
        }

        protected void iexPaging_PageChanged(object sender, EventArgs e)
        {
            CurrentPage = iexPaging.CurrentPageIndex - 1;

            BindGrid();
        }

        protected int PageSize
        {
            get
            {
                return 20;
            }
        }

        protected int CurrentPage
        {
            get
            {
                if (ViewState["CurPage"] == null)
                    ViewState["CurPage"] = 0;
                return Convert.ToInt32(ViewState["CurPage"]);
            }
            set
            {
                ViewState["CurPage"] = value;
            }
        }

        #endregion

        private string FilterCategoryID
        {
            get
            {
                return ddlCategoryFilter.SelectedItem.Value;
            }
        }

        protected void ddlCategoryFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        void SetDefaultValues()
        {
            //if (ddlCategoryFilter.SelectedIndex > 0) {
            txtProductName.Text = ddlCategoryFilter.SelectedItem.Text.Replace("|__", "");
            ddlCategoryID.SelectedValue = ddlCategoryFilter.SelectedItem.Value;
            //}
            txtProductCode.Text = "VM ";
            //txtDescription.Text = "Mô Tả Sản Phẩm";
            //txtMaterials.Text = "Chất liệu";
            //txtColors.Text = "Màu Sắc";
            txtPrice.Text = "0";

            MappingStoredProcedure dal = new MappingStoredProcedure();
            txtDisplayOrder.Text = dal.SP_GetProdctNextDisplayOrder(FilterCategoryID);
        }
    }
}


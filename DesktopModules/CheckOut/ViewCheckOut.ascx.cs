﻿// 
// DotNetNuke® - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke;
using DotNetNuke.Common.Controls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;

namespace Philip.Modules.CheckOut
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewCheckOut class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewCheckOut : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    Initialize();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        private void Initialize()
        {
            vws.ActiveViewIndex = 0;
        }

        protected void btnPlaceOrder_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            int cartItems =
                Convert.ToInt32(DBHelper.ExecuteScalarFormat("Select count(*) from ShoppingCart where SessionID = {0}",
                    DBHelper.SQuote(Session.SessionID)));

            if (cartItems > 0)
            {
                OrderManager manager = new OrderManager();
                OrderEntity order = new OrderEntity();
                order.FullName = txtFullName.Text;
                order.Address = txtAddress.Text;
                order.Phone = txtPhone.Text;
                order.Fax = "";
                order.Email = txtEmail.Text;
                order.Comment = txtComment.Text;
                order.OnDate = DateTime.Now;
                order.Total = 0;
                order.StatusID = Convert.ToInt32(eOrderStatus.Pending);
                manager.Insert(order);


                string sql =
                    String.Format(
                        "Insert into OrderDetail (OrderID, ProductID, ColorID, Price, Quantity) select {0}, ProductID, ColorID, Price, Quantity from ShoppingCart where SessionID = {1}; update [Order] set Total = (Select sum(Price * Quantity) from OrderDetail where OrderID = {0}) where ID = {0}; Delete from ShoppingCart where SessionID = {1}; exec SP_DeleteOldCart;",
                        order.ID, DBHelper.SQuote(Session.SessionID));

                DBHelper.ExecuteSQL(sql);

                SendNotification(order);
            }

            vws.ActiveViewIndex = 1;

            Page.MaintainScrollPositionOnPostBack = false;
        }

        private void SendNotification(OrderEntity order)
        {
            double total = 0;
            string subject = "www.ghevm.com - Đơn Hàng Mới";
            string body = File.ReadAllText(Server.MapPath("~/DesktopModules/CheckOut/EmailTemplate.htm"));
            body = body.Replace("[OrderID]", order.ID.ToString()); 
            body = body.Replace("[Name]", order.FullName);
            body = body.Replace("[Address]", order.Address);
            body = body.Replace("[Phone]", order.Phone);
            body = body.Replace("[Email]", order.Email);
            body = body.Replace("[Comment]", order.Comment);
            
            body = body.Replace("[Details]", GenerateOrderDetailHtml(order, out total));
            body = body.Replace("[Total]", total > 0 ? total.ToString() : "Liên Hệ");
            string email = ConfigurationManager.AppSettings["EmailOrder"];
            //DNNHelper.SendEmail("", email, "", subject, body, "");
            DNNHelper.SendMailWithGoogle("", email, ConfigurationManager.AppSettings["CC"], "", subject, body);
        }

        private string GenerateOrderDetailHtml(OrderEntity order, out double total)
        {
            total = 0;
            DataTable tblDetails = LoadOrderDetails(order.ID);
            StringBuilder html = new StringBuilder();
            foreach (DataRow row in tblDetails.Rows)
            {
                total += Convert.ToInt32(row["Quantity"])*Convert.ToDouble(row["Price"]);
                html.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>", row["ProductCode"], row["MaterialName"], row["ColorName"], row["Quantity"], Convert.ToInt32(row["Price"]) > 0 ? row["Price"] : "Liên Hệ");
            }
            return html.ToString();
        }

        private DataTable LoadOrderDetails(int orderID)
        {
            string sql = String.Format("select * from vw_OrderDetail where OrderID = {0}", orderID);
            return DBHelper.GetTable(sql);
        }
    }
}


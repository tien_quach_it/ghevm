﻿<%@ Control language="C#" Inherits="Philip.Modules.CheckOut.ViewCheckOut" CodeFile="ViewCheckOut.ascx.cs" AutoEventWireup="true"%>

    <asp:MultiView id="vws" runat="server">
        <asp:View ID="vwPlaceOrder" runat="server">
			<div class="title_bar_order">
                <h2> Đặt Hàng </h2>
            </div>  
                
            <div class="form_order">
                <div class="form_name">
                    <p>
                        Họ Tên 
                        <asp:RequiredFieldValidator ID="reqFullName" runat="server" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtFullName" ForeColor="Red"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        Địa Chỉ 
                        <asp:RequiredFieldValidator ID="reqAddress" runat="server" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtAddress" ForeColor="Red"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        Điện Thoại 
                        <asp:RequiredFieldValidator ID="reqPhone" runat="server" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtPhone" ForeColor="Red"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        Email 
                        <asp:RequiredFieldValidator ID="reqEmail" runat="server" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtEmail" ForeColor="Red"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        Ghi Chú 
                        <asp:RequiredFieldValidator ID="reqComment" runat="server" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtComment" ForeColor="Red"></asp:RequiredFieldValidator>
                    </p>
                </div>
                <div>
                    <asp:TextBox ID="txtFullName" runat="server"></asp:TextBox>

                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>

                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>

                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    
                    <asp:TextBox ID="txtComment" runat="server" Columns="25" Rows="8" TextMode="Multiline"></asp:TextBox>
                    
                    <asp:LinkButton ID="btnPlaceOrder" runat="server" OnClick="btnPlaceOrder_Click" Text="Đặt Hàng" CssClass="red-button" />

                </div>
                

                <div class="clear"></div>
            </div>     
        </asp:View>
        <asp:View ID="vwThanks" runat="server">
            <h1>Cảm ơn bạn. </h1>
            <strong>Bạn đã đặt hàng thành công. Chúng tôi sẽ liên hệ với bạn trong thời gian ngắn nhất.</strong>
            <%--<script type="text/javascript">
                $(function () {
                    setTimeout(function () {
                        window.scrollTo(0, 0);
                    }, 1000);
                })
            </script>--%>
        </asp:View>
    </asp:MultiView>


﻿<%@ Control language="C#" Inherits="Modules.ManageCatalogue.ViewManageCatalogue" CodeFile="ViewManageCatalogue.ascx.cs" AutoEventWireup="true"%>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<div>
	<asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red"></asp:Label>
    <asp:MultiView ID="vws" runat="server">
        <asp:View ID="vwView" runat="server">
            <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkAddNew" Text="Thêm Mới" runat="server" CssClass="button" OnClick="lnkAddNew_Click"></asp:LinkButton>
                    </td>  
                    <td align="right">
                        <asp:LinkButton ID="btnManageZip" Text="Upload File Zip" runat="server" CssClass="button" OnClick="btnManageZip_Click"></asp:LinkButton>
                    </td>                  
                </tr>
            </table>
            <br />
            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                <tr class="t_admin_table_header">
                    <th>
                    
                    </th>    
                    <th>
                        
                    </th>                    
                    <th><asp:LinkButton ID="btnSortPicture" runat="server" Text="Hình Ảnh" CommandArgument="Picture" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th><asp:LinkButton ID="btnSortDisplayOrder" runat="server" Text="STT" CommandArgument="DisplayOrder" OnClick="btnSort_Click"></asp:LinkButton></th>
                    
                </tr>
                <asp:Repeater ID="rpt" runat="server">
                    <ItemTemplate>                        
                        <tr class="row<%# Container.ItemIndex % 2 %>">
                            <td>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Chỉnh Sửa" OnClick="lnkEdit_Click" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton></th>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>'
                                     Text="Xoá" OnClientClick="return confirm('Bạn có muốn xoá hình này hay không?')"
                                    OnClick="lnkDelete_Click"></asp:LinkButton>
                            </td>
                            <td><img width='80px' src='<%# ResolveUrl(ImageHelper.GetCatalogueImageFullPath(Eval("Picture"))) %>' alt="" /></td>
                            <td><%# Eval("DisplayOrder") %></td>
                            
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>      
            <div>
                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
            </div>      
        </asp:View>
        <asp:View ID="vwEdit" runat="server">
            <table cellpadding="2" cellspacing="2" class="t_admintable">
                <tr class='row0'><td>Hình Ảnh *</td><td><asp:FileUpload ID="fupPicture" runat="server" /><br /><asp:Image ID="imgPicture" runat="server" Width="150px" /></td></tr>
                <tr class='row1'><td>STT *</td><td><asp:TextBox ID="txtDisplayOrder" runat="server" Width="400"></asp:TextBox></td></tr>
                
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Lưu Lại" CssClass="button" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Huỷ" CssClass="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="vwManageZip" runat="server">
            <table cellpadding="2" cellspacing="2">
                <tr>
                    <td>Upload File Zip:</td>
                    <td><asp:FileUpload runat="server" ID="fupZip"/></td>
                    <td><asp:Literal runat="server" ID="ltrDownload"></asp:Literal></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnUploadZip" runat="server" Text="Upload" CssClass="button" OnClick="btnUploadZip_Click" />
                        <asp:Button ID="btnCancel1" runat="server" Text="Huỷ" CssClass="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>
/*
' DotNetNukeŽ - http://www.dotnetnuke.com
' Copyright (c) 2002-2006
' by Perpetual Motion Interactive Systems Inc. ( http://www.perpetualmotion.ca )
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
' documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
' the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
' to permit persons to whom the Software is furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial portions 
' of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
' DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;

using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;

namespace Philip.Modules.HomeBanners
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewHomeBanners class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewHomeBanners : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        //public string SlideDuration
        //{
        //    get
        //    {
        //        return DNNHelper.GetModuleSetting(Settings, "SlideDuration", "");
        //    }
        //}
        //public bool IsStatic
        //{
        //    get
        //    {
        //        return DNNHelper.GetModuleSettingAsBool(Settings, "IsStatic", false);
        //    }
        //}

        public string SlideDuration
        {
            get
            {
                var obj = ViewState["SlideDuration"];
                if (obj == null)
                    return "3";
                return ViewState["SlideDuration"].ToString();
            }
            set
            {
                ViewState["SlideDuration"] = value;
            }
        }
        public bool IsStatic
        {
            get
            {
                var obj = ViewState["IsStatic"];
                if (obj == null)
                    return false;
                return Convert.ToBoolean(ViewState["IsStatic"]);
            }
            set
            {
                ViewState["IsStatic"] = value;
            }
        }

        public string ImageUrl = "";

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    LoadHomeBannerSetting();
                    LoadData();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region Ha.Huynh Added
        public void LoadData()
        {
            if (IsStatic)
            {
                DataTable tbl = DBHelper.GetTable("Select * from HomeBanner Where IsStaticShow = 1");
                if (tbl.Rows.Count == 0)
                {
                    tbl = DBHelper.GetTable("Select top 1 * from HomeBanner Order by DisplayOrder asc");
                }
                ImageUrl = ImageHelper.GetHomeBannerImageFullPath(tbl.Rows[0]["Picture"].ToString());
                rptBanners.DataSource = tbl;
            }
            else
            {
                HomeBannerManager dal = new HomeBannerManager();
                IList<HomeBannerEntity> lst = dal.GetHomeBanners().OrderBy(o => o.DisplayOrder).ToList();
                rptBanners.DataSource = lst;
            }
            rptBanners.DataBind();

            rptItemList.DataSource = rptBanners.DataSource;
            rptItemList.DataBind();
            
        }

        public string GetMobileDisplay(int type)
        {
            if (IsStatic)
            {
                if (type == 0)
                {
                    return "none";
                }
                else
                    return "block";
            }
            return "";
        }

        protected void LoadHomeBannerSetting()
        {
            HomeBannerSettingEntity obj;
            HomeBannerSettingManager dal = new HomeBannerSettingManager();
            obj = dal.GetHomeBannerSettings().FirstOrDefault();
            if (obj != null)
            {
                SlideDuration = obj.Duration;
                IsStatic = obj.IsStatic;
            }
        }

        public string CheckTargetLink(object link)
        {
            if (link.ToString().StartsWith("/"))
                return "_self";
            return "_blank";
        }
        #endregion
    }
}


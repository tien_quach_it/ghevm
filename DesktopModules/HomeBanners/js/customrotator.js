﻿$(document).ready(function () {
    if (isStatic.toLowerCase() == "false") {
        /*Home Rotator*/
        var banner = $("#rotator");
        banner.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigation: true,
            autoPlay: 7000
        });
        $("#rotatorNav .next").click(function () {
            banner.trigger('owl.next');
        })
        $("#rotatorNav .prev").click(function () {
            banner.trigger('owl.prev');
        });
    }
});


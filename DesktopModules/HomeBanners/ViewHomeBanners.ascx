﻿<%@ Control Language="C#" Inherits="Philip.Modules.HomeBanners.ViewHomeBanners" CodeFile="ViewHomeBanners.ascx.cs" AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<script type="text/javascript">
    var isStatic = '<%= IsStatic %>';
</script>
<link href="<%= ResolveUrl("~/DesktopModules/HomeBanners/css/owl.carousel.css") %>" rel="stylesheet" />
<script src="<%= ResolveUrl("~/DesktopModules/HomeBanners/js/owl.carousel.min.js") %>"></script>
<script src="<%= ResolveUrl("~/DesktopModules/HomeBanners/js/customrotator.js") %>"></script>
<style type="text/css">
    .banners {
        position: relative;
        width: 500px;
        height: 350px;
    }
    .banners img {
        position: absolute;
        left: 0;
        top: 0;
        z-index: 10;
    }
</style>
<div class="banners hidden-xs">
    <asp:Repeater ID="rptBanners" runat="server">
        <ItemTemplate>
            <a href="<%# Eval("Link") %>" target="<%# CheckTargetLink(Eval("Link")) %>">            
                <img width="500px" height="350px" src='<%# ResolveUrl(ImageHelper.GetHomeBannerImageFullPath(Eval("Picture").ToString())) %>' alt="" /></a>
        </ItemTemplate>
    </asp:Repeater>
</div>
<div class="hidden-lg hidden-md hidden-sm" style="display: <%= GetMobileDisplay(0) %>">
    <asp:Repeater ID="rptItemList" runat="server">
        <HeaderTemplate>
            <div class="rotatorRow">
                <div id="rotatorNav"><a class="btn prev"><img src="/DesktopModules/HomeBanners/images/h-arrow-left.png" /></a><a class="btn next"><img src="/DesktopModules/HomeBanners/images/h-arrow-right.png" /></a></div>
                <div class="rotatorWrap">
                    <div class="owl-carousel owl-theme" id="rotator">
        </HeaderTemplate>
        <ItemTemplate>
            <div class="item" id="homebanner<%# Container.ItemIndex + 1 %>">
                <img width="100%" height="100%" src='<%# ResolveUrl(ImageHelper.GetHomeBannerImageFullPath(Eval("Picture").ToString())) %>' alt="" />                
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div>
            </div>
        </div>
        </FooterTemplate>
    </asp:Repeater>
</div>
<div style="display: <%= GetMobileDisplay(1) %>">
    <img width="100%" height="100%" src='<%= ResolveUrl(ImageUrl) %>' alt="" />
</div>
<script type="text/javascript">
    $(function () {        
        if (isStatic.toLowerCase() == "false") {
            var duration = '<%= SlideDuration %>';

            var $galitem = $('.banners').children();

            // Đếm các ảnh trong gallery
            var $galsize = $('.banners img').size();

            // Ẩn tất cả các ảnh và hiện ảnh đầu tiên
            $('.banners img:gt(0)').hide();
            $currentimg = 0;

            // Thêm id để phân biệt riêng từng ảnh
            $galitem.children().attr("id", function (arr) {
                return "galleryitem" + arr;
            });

            // Tự động chuyển ảnh duration trong setting
            setInterval(function () {
                nextimg($currentimg, $galsize);
                $currentimg = $currentimg + 1;
            }, duration * 1000);
        }

        $('.banners a').click(function () {
            if ($(this).attr('href') == "") {
                return false;
            }
        });
        $('.banners a').each(function (index) {
            if ($(this).attr('href') == "") {
                $(this).css('cursor', 'default');
            }
        });
    })
    
    // Hàm xử lý tự động chuyển ảnh
    function nextimg($img, $size) {
        $n_img = $img + 1;
        if ($n_img < $size) {
            $('#galleryitem' + $img).fadeOut();
            $('#galleryitem' + $n_img).fadeIn();
        }
        else {
            $('#galleryitem' + ($size - 1)).fadeOut();
            $('#galleryitem0').fadeIn();
            $currentimg = -1;
        }
    }
</script>

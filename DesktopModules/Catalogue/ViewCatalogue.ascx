<%@ Control Language="C#" Inherits="Modules.Catalogue.ViewCatalogue" CodeFile="ViewCatalogue.ascx.cs" AutoEventWireup="true" %>
<script src="/js/Carousel/jquery.jcarousel.min.js"></script>
<script src="/js/Carousel/jcarousel.basic.js"></script>
<link href="/js/Carousel/jcarousel.basic.css" rel="stylesheet" />

<div class="jcarousel-wrapper">
    <div class="jcarousel">
        <ul>
            <asp:Repeater ID="rpt" runat="server">
                <ItemTemplate>
                    <li><img width='100%' src='<%# ResolveUrl(ImageHelper.GetCatalogueImageFullPath(Eval("Picture"))) %>' alt="" /></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>

    <%--<p class="photo-credits">
        Photos by <a href="http://www.mw-fotografie.de">Ghevm.com</a>
    </p>--%>

    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
    <a href="#" class="jcarousel-control-next">&rsaquo;</a>
</div>
<div class="download-container">
    <table width="100%" cellpadding="4" cellspacing="4">
        <tr>
            <td class="photo-credit">Photos by <a href="http://www.mw-fotografie.de">Ghevm.com</a></td>
            <td class="download-link" align="right">
                <asp:Literal ID="ltrDownload" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</div>

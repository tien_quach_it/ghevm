﻿<%@ Control language="C#" Inherits="Modules.Partners.ViewPartners" CodeFile="ViewPartners.ascx.cs" AutoEventWireup="true"%>
<div class="hot_product_box">
    <div class="title-bar">
        <img src="/portals/0/Skins/GheVM/images/partners_ico.png" alt="" class="partner-icon">
        <p> Đối Tác </p>
    </div>
    <div class="partnes_container">
        <div class="partners-inner">
            <marquee width="190" height="350" onmouseout="this.start()" onmouseover="this.stop()" direction="up" scrollamount="8" style="overflow: hidden">
                <asp:Repeater ID="rptPartners" runat="server">
                    <ItemTemplate>
                        <div class="partner-item">
                            <a href="<%# UrlHelper.EnsureHttp(Eval("LinkUrl")) %>" target="_blank">
                                <img src="<%# ResolveUrl(ImageHelper.GetPartnerImageFullPath(Eval("Picture"))) %>" alt='<%# Eval("Name") %>' width="188" /></a></div>
                    </ItemTemplate>
                    <SeparatorTemplate>
                        <div class="partner-item-saparator"></div>
                    </SeparatorTemplate>
                </asp:Repeater>
            </marquee>
        </div>
    </div>
</div>
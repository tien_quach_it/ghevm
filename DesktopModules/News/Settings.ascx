﻿<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Settings.ascx.cs" Inherits="Philip.Modules.News.Settings" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<div class="mspsContent dnnClear">
    <h2 class="dnnFormSectionHead">
        <a class="dnnSectionExpanded" href="">News Setting</a></h2>
    <fieldset style="display: block;">
        <div class="dnnFormItem">
            <div class="dnnTooltip">
                Hiển thị trang chi tiết:
            </div>
            <asp:CheckBox ID="chkDetail" runat="server" />
        </div>        
    </fieldset>    
</div>
// 
// DotNetNukeŽ - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;

namespace Philip.Modules.News
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewNews class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewNews : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        public bool IsDetail
        {
            get
            {
                return DNNHelper.GetModuleSettingAsBool(Settings, "IsDetail", false);
            }
        }

        public int NewsID
        {
            get
            {
                var obj = ViewState["NewsID"];
                if (obj == null)
                    return 0;
                return Convert.ToInt32(ViewState["NewsID"].ToString());
            }
            set
            {
                ViewState["NewsID"] = value;
            }
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (IsDetail)
                    {
                        if (Request.QueryString["ID"] != null && Request.QueryString["ID"].ToString() != "")
                        {
                            NewsID = Convert.ToInt32(Request.QueryString["ID"].ToString());
                        }
                        vws.ActiveViewIndex = 1;
                        LoadDetail();
                    }
                    else
                    {
                        vws.ActiveViewIndex = 0;
                        BindGrid();
                    }                    
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region Ha.Huynh Added
        public List<NewsViewEntity> LoadData()
        {
            try
            {
                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_NewsWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString);

                List<NewsViewEntity> lst = results.GetResult<NewsViewEntity>().ToList();
                int NumRows = results.GetResult<int>().Single();

                // Paging

                int totalpage = NumRows / PageSize;
                if (NumRows % PageSize > 0)
                    totalpage = totalpage + 1;
                iexPaging.TotalPages = totalpage;
                iexPaging.Visible = totalpage > 1;

                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void BindGrid()
        {
            try
            {
                var lst = LoadData();
                rptNews.DataSource = lst;
                rptNews.DataBind();
            }
            catch (Exception ex)
            {
                
            }
        }
        protected string SortString
        {
            get
            {
                if (ViewState["SortString"] == null)
                    return "DisplayOrder";
                return ViewState["SortString"].ToString();
            }
            set
            {
                ViewState["SortString"] = value;
            }
        }
        protected void iexPaging_PageChanged(object sender, EventArgs e)
        {
            CurrentPage = iexPaging.CurrentPageIndex - 1;

            BindGrid();
        }

        protected int PageSize
        {
            get
            {
                return 20;
            }
        }

        protected int CurrentPage
        {
            get
            {
                if (ViewState["CurPage"] == null)
                    ViewState["CurPage"] = 0;
                return Convert.ToInt32(ViewState["CurPage"]);
            }
            set
            {
                ViewState["CurPage"] = value;
            }
        }


        public void LoadDetail()
        {
            if (NewsID != 0)
            {
                NewsEntity obj = new NewsManager().GetNewsById(NewsID);
                if (obj != null)
                {
                    lblTitle.Text = obj.Title;
                    lblDetail.Text = obj.Description;

                    DotNetNuke.Framework.CDefault control = ((DotNetNuke.Framework.CDefault)this.Page);
                    control.Title = obj.Title;

                    // Social share
                    var pageUrl = String.Format("http://{0}/ChiTietTinTucSuKien.aspx?id={1}", Request.Url.Host, NewsID);
                    ctrSocialShare.PageUrl = pageUrl;
                    ((DotNetNuke.Framework.CDefault)this.Page).Header.Controls.Add(new LiteralControl("<meta property=\"og:title\" content=\"" + obj.Title + "\" />"));
                    ((DotNetNuke.Framework.CDefault)this.Page).Header.Controls.Add(new LiteralControl("<meta property=\"og:description\" content=\"" + obj.Summary + "\" />"));
                    ((DotNetNuke.Framework.CDefault)this.Page).Header.Controls.Add(new LiteralControl("<meta property=\"og:image\" content=\"http://" + Request.Url.Host + UrlHelper.ResolveUrl(ImageHelper.GetNewsImageFullPath(obj.Picture)) + "\" />"));
                    ((DotNetNuke.Framework.CDefault)this.Page).Header.Controls.Add(new LiteralControl("<meta property=\"og:url\" content=\"" + pageUrl + "\" />"));
                }                
            }
        }        
        #endregion
    }
}


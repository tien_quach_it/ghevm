﻿<%@ Control Language="C#" Inherits="Philip.Modules.News.ViewNews" CodeFile="ViewNews.ascx.cs"
    AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<%@ Register Src="~/controls/SocialShare.ascx" TagPrefix="dnn" TagName="SocialShare" %>	
<div class="sub_n-eContent">
    <div class="n-eContent">
        <asp:MultiView ID="vws" runat="server">
            <asp:View ID="vwView" runat="server">
                <asp:Repeater ID="rptNews" runat="server">
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="cover">
                                <img alt="cover" src='<%# ResolveUrl(ImageHelper.GetNewsImageFullPath(Eval("Picture").ToString())) %>'
                                    alt="" class="news" />
                            </div>
                            <div class="summary">
                                <h1>
                                    <a href="/ChiTietTinTucSuKien.aspx?id=<%# Eval("ID") %>">
                                        <%# Eval("Title") %></a>
                                </h1>
                                <p>
                                    <%# DNNHelper.TruncateText(Eval("Summary").ToString(), 80) %><a class="ico_more"
                                        href="/ChiTietTinTucSuKien.aspx?id=<%# Eval("ID") %>"></a>
                                </p>
                            </div>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
                <div class="clr"></div>
                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
            </asp:View>
            <asp:View ID="vwEdit" runat="server">
                <div>
                    <asp:Label ID="lblTitle" runat="server" CssClass="newsTitle" Font-Bold="true"></asp:Label>
                </div>
                <div class="newsDetails">
                    <asp:Label ID="lblDetail" runat="server"></asp:Label>
                </div>
                <br /><br />
                <dnn:SocialShare Runat="server" ID="ctrSocialShare"></dnn:SocialShare><br />
                <a href="/TinTucSuKien.aspx" class="newsLink"><< Quay Lại</a>
            </asp:View>
        </asp:MultiView>
    </div>
    <div class="clear">
    </div>
</div>
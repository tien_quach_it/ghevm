﻿<%@ Control language="C#" Inherits="Philip.Modules.ProductDetails.ViewProductDetails" CodeFile="Copy of ViewProductDetails.ascx.cs" AutoEventWireup="true"%>
<%--References: http://www.mind-projects.it/projects/jqzoom/--%>
<script src="<%= ResolveUrl("~/js/jqzoom/jquery.jqzoom-core.js") %>" type="text/javascript"></script>
<link href="<%= ResolveUrl("~/js/jqzoom/jquery.jqzoom.css") %>" rel="stylesheet" />
<script type="text/javascript">
    $(document).ready(function () {


$("#<%= imgPicture.ClientID %>").bind('load', function() {
 
    $('a.main-photo').jqzoom({
            zoomType: 'innerzoom',
            title: false,	    
	    preloadImages: true
        });


}).attr('src', $("#<%= imgPicture.ClientID %>").attr("src"));


        
        
    });
</script>

    <div class="mainImgConatiner">
        <asp:Repeater id="rptOtherImages" runat="server">
            <HeaderTemplate>
                <div class="moreImgThumbContainer">
            </HeaderTemplate>
            <ItemTemplate>
                <div class="imageThumb"><%--class="zoomThumbActive" --%>
                    <a href="javascript:void(0);" rel="{gallery: 'gal1', smallimage: '<%# UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("Picture"))) %>', largeimage: '<%# UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("Picture"))) %>'}">
                        <img alt="" src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 50) %>">
                    </a>
                </div>
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:Repeater>
        <!------ end more Img Thumb Container ----->
                    
        <div class="imgThumbContainer ">
            <a href="<%= ResolveUrl(imgPicture.ImageUrl) %>" class="main-photo" rel="gal1"><%--   title="MYTITLE"--%>
                <asp:Image ID="imgPicture" runat="server" Width="398" />
            </a>
        </div><!----- end imgThumbContainer ------->
             
        <div class="prodColor">
            <asp:Literal runat="server" id="ltrColorsTitle2" Text="<p> Màu Sắc : </p>"></asp:Literal>
            <asp:Repeater id="rptColors" runat="server">
                <HeaderTemplate>
                    <div class="colorProdImgthumb">
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="imageThumb">
                        <a href="javascript:void(0);" rel="{gallery: 'gal1', smallimage: '<%# UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("Picture"))) %>', largeimage: '<%# UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("Picture"))) %>'}">
                            <img alt="<%# Eval("ColorName") %>" src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 50) %>">
                        </a>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            <!---- color Product Img ------->
        </div><!---- End product color ------>
    </div><!---- End main Img Conatiner ------>
                
    <div class="ProdInfoContainer">
        <div class="nameIDcontainer">
            <p class="prod_name"> <asp:Literal runat="server" id="ltrProductName"></asp:Literal> </p>
            <p class="prodID"> <asp:Literal runat="server" id="ltrProductCode"></asp:Literal> </p>
        </div><!---- End info Product Container ------>
                    
        <div class="info">
            <p> Giá : <asp:Label ID="lblPriceValue" runat="server" Visible="False"></asp:Label><asp:Label ID="lblPrice" runat="server"></asp:Label> <span> </span></p>
            <p class="order"> Đặt hàng : 072.375.9677 - 678 </p>
            <div class="materials">
                <p> <asp:Literal runat="server" id="ltrColorMaterialsTitle" Text="Chất Liệu :"></asp:Literal>  </p>
                <asp:DropDownList runat="server" ID="ddlColorMaterials" DataTextField="MaterialName" DataValueField="ID" AutoPostBack="true" OnSelectedIndexChanged="ddlColorMaterials_SelectedIndexChanged"></asp:DropDownList>
            </div>
            <div class="color">
                <p> <asp:Literal runat="server" id="ltrColorsTitle" Text="Màu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:"></asp:Literal> </p>
                <asp:DropDownList runat="server" ID="ddlColors" DataTextField="ColorName" DataValueField="ID"></asp:DropDownList>
            </div>
            
            
            <asp:LinkButton id="btnAddToCart" runat="server" Text="Mua Hàng" OnClick="btnAddToCart_Click" CssClass="red-button"  />
            
            
            <%--Size--%>
            <asp:Repeater id="rptSizes" runat="server">
                <HeaderTemplate>
                    <div class="size">
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="prod_size">
                        <a href="javascript:void(0);" rel="{gallery: 'gal1', smallimage: '<%# UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("Picture"))) %>', largeimage: '<%# UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("Picture"))) %>'}">
                            <img alt="" src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 100) %>">
                        </a>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            
        </div><!---- End info ------>
    </div><!---- End Product info Conatiner ------>
                
    <div class="ProdInfoContainer2">
        <p class="title_info">Mô Tả Sản Phẩm</p>
        <p>
            <asp:Literal runat="server" id="ltrDescription"></asp:Literal>
        </p>
        <p class="title_info">Chất Liệu</p>
        <p>
            <asp:Literal runat="server" id="ltrMaterials"></asp:Literal>
        </p>
        <p class="title_info">Màu Sắc</p>
        <p>
            <asp:Literal runat="server" id="ltrColors"></asp:Literal>
        </p>
        <p class="title_info">Quy Định Bảo Hành</p>
        <p>
            <asp:Literal runat="server" id="ltrWarranty"></asp:Literal>
        </p>
    </div>

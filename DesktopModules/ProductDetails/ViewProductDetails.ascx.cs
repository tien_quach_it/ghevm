﻿// 
// DotNetNuke® - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Linq;
using System.Text;

namespace Philip.Modules.ProductDetails
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewProductDetails class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewProductDetails : PortalModuleBase, IActionable
    {

        #region Private Members

        private List<ProductColorViewEntity> lstColors;
        private string mainProductPicture = "";

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        public string ProductSeoUrl;

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (String.IsNullOrEmpty(ProductID))
                        return;

                    Initialize();                    
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }


        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        void Initialize()
        {
            // Check System Settings
            SystemSettingsManager manager = new SystemSettingsManager();
            SystemSettingsEntity settings = manager.GetSystemSettingsById(1);
            btnAddToCart.Visible = settings.EnableShoppingCart;
            EnableShoppingCart = settings.EnableShoppingCart;
            NoPrice = settings.NoPrice;

            // Start Session
            Session["StartShopping"] = 1;

            LoadProductDetails();
        }

        private void LoadBreadcrumbs(CategoryEntity category, ProductEntity product)
        {
            TabController controller = new TabController();

            // Home Tab
            var homeTab = controller.GetTab(PortalSettings.HomeTabId, PortalId, false);
            ArrayList arr = new ArrayList();
            //arr.Add(new ListItem(homeTab.Title, homeTab.FullUrl));
            arr.Add(new ListItem(homeTab.Title, DNNHelper.GetHomePageUrl()));

            if (category.ParentID == 0)
                arr.Add(new ListItem(category.CategoryName, category.SeoUrl));
            else
            {
                // Parent Category
                var manager = new CategoryManager();
                var parentCategory = manager.GetCategoryById(category.ParentID);
                arr.Add(new ListItem(parentCategory.CategoryName, parentCategory.SeoUrl));

                // Current Category
                arr.Add(new ListItem(category.CategoryName, category.SeoUrl));
            }

            // Current Product
            arr.Add(new ListItem(product.ProductCode, product.SeoUrl));

            rptBreadcrumbs.DataSource = arr;
            rptBreadcrumbs.DataBind();
        }

        private bool EnableShoppingCart
        {
            get
            {
                if (ViewState["EnableShoppingCart"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["EnableShoppingCart"]);
            }
            set
            {
                ViewState["EnableShoppingCart"] = value;
            }
        }

        private bool NoPrice
        {
            get
            {
                if (ViewState["NoPrice"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["NoPrice"]);
            }
            set
            {
                ViewState["NoPrice"] = value;
            }
        }

        private void LoadProductDetails()
        {
            ProductManager manager = new ProductManager();
            var product = manager.GetProductById(Convert.ToInt32(ProductID));
            if (product == null)
                return;

            ltrProductName.Text = product.ProductName;
            ltrProductCode.Text = product.ProductCode;
            imgPicture.ImageUrl = ImageHelper.GetProductImageFullPath(product.Picture);

            mainProductPicture = product.Picture;

            ltrDescription.Text = Server.HtmlDecode(product.Description);
            ltrMaterials.Text = Server.HtmlDecode(product.Materials);
            ltrColors.Text = Server.HtmlDecode(product.Colors);
            ltrWarranty.Text = Server.HtmlDecode(product.Warranty);

            if (NoPrice)
                lblPriceValue.Text = "0";
            else
                lblPriceValue.Text = product.Price.ToString();
            SetPriceLabel(product.Price);

            //DotNetNuke.Framework.CDefault control = ((DotNetNuke.Framework.CDefault)this.Page);
            //control.Title = String.Format("{0} - {1} - {2}", control.Title, product.ProductName, product.ProductCode);

            LoadProductImages();

            // Colors and Materials
            LoadColorMaterials();
            LoadColorsBySelectedMaterial();

            LoadProductSizes();

            // Must reset the Main Image at the first load
            imgPicture.ImageUrl = ImageHelper.GetProductImageFullPath(product.Picture);

            // Breadcrumbs
            LoadBreadcrumbs(new CategoryManager().GetCategoryById(product.CategoryID), product);

            // SEO
            var page = ((DotNetNuke.Framework.CDefault) this.Page);
            if (!String.IsNullOrEmpty(product.SeoTitle))
                page.Title = product.SeoTitle;
            else
                page.Title = GetSeoTitle(product);
            if (!String.IsNullOrEmpty(product.SeoKeywords))
                page.KeyWords = product.SeoKeywords;

            string desctionNoHTML = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(product.Description), @"<[^>]+>|&nbsp;", "").Trim();

            if (!String.IsNullOrEmpty(product.SeoDescription))
                page.Description = product.SeoDescription;
            else
                page.Description = desctionNoHTML;
            
            // Social share
            var pageUrl = String.Format("http://{0}{1}", Request.Url.Host, product.SeoUrl);
            ctrSocialShare.PageUrl = pageUrl;
            ((DotNetNuke.Framework.CDefault)this.Page).Header.Controls.Add(new LiteralControl("<meta property=\"og:title\" content=\"" + page.Title + "\" />"));
            ((DotNetNuke.Framework.CDefault)this.Page).Header.Controls.Add(new LiteralControl("<meta property=\"og:description\" content=\"" + desctionNoHTML + "\" />"));
            ((DotNetNuke.Framework.CDefault)this.Page).Header.Controls.Add(new LiteralControl("<meta property=\"og:image\" content=\"http://" + Request.Url.Host + UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(product.Picture)) + "\" />"));
            ((DotNetNuke.Framework.CDefault)this.Page).Header.Controls.Add(new LiteralControl("<meta property=\"og:url\" content=\"" + pageUrl + "\" />"));
        }

        private string GetSeoTitle(ProductEntity product)
        {
            var manager = new CategoryManager();
            var category = manager.GetCategoryById(product.CategoryID);
            if (category.ParentID == 0)
                return String.Format("{0} | {1} | Ghế Văn Phòng Văn Minh", product.ProductCode, product.ProductName);
            
            var parentCategory = manager.GetCategoryById(category.ParentID);
            return String.Format("{0} | {1} | {2} | Ghế Văn Phòng Văn Minh", product.ProductCode, product.ProductName, parentCategory.CategoryName);
        }

        private void SetPriceLabel(double price)
        {
            lblPrice.Text = "Liên Hệ";

            if (NoPrice)
                return;

            if (price > 0 && EnableShoppingCart)
            {
                lblPrice.Text = DNNHelper.FormatPriceWithNote(price);
            }
        }

        private void LoadProductColors()
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProductColorWithCount("0", "1000", "ID", ProductID);
            lstColors = results.GetResult<ProductColorViewEntity>().ToList();
        }
        
        private void LoadColorMaterials()
        {
            LoadProductColors();
            if (lstColors.Count == 0)
            {
                ltrColorMaterialsTitle.Visible = false;
                ddlColorMaterials.Visible = false;
                ltrColorsTitle.Visible = false;
                ddlColors.Visible = false;
                ltrColorsTitle2.Visible = false;
                rptColors.Visible = false;
            }

            var lstMaterials = lstColors.GroupBy(color => new { MaterialID = color.MaterialID, MaterialName = color.MaterialName }).Select(m => new MaterialEntity
            {
                ID = m.Key.MaterialID,
                MaterialName = m.Key.MaterialName
            });

            ddlColorMaterials.DataSource = lstMaterials;
            ddlColorMaterials.DataBind();
            if (ddlColorMaterials.Items.Count > 0)
                ddlColorMaterials.SelectedIndex = 0;

            // All Colors Text
            //StringBuilder html = new StringBuilder();
            //foreach (var material in lstMaterials)
            //{
            //    html.AppendFormat("<p><strong>{0}:</strong> ", material.MaterialName);
            //    var lst = lstColors.Where(color => color.MaterialID == material.ID).Select(c => c.ColorName).ToList();
            //    html.Append(String.Join(" ,", lst.ToArray()));
            //    html.Append("</p>");
            //}
            //ltrColors.Text = html.ToString();
        }

        private void LoadColorsBySelectedMaterial()
        {
            if (lstColors == null)
                LoadProductColors();

            if (lstColors.Count == 0)
                return;

            int selectedMaterialID = Convert.ToInt32(ddlColorMaterials.SelectedItem.Value);
            var lst = lstColors.Where(color => color.MaterialID == selectedMaterialID).ToList();
            ddlColors.DataSource = lst;
            ddlColors.DataBind();

            rptColors.DataSource = lst;
            rptColors.DataBind();

            // Set Main Image with the first Color image
            imgPicture.ImageUrl = ImageHelper.GetProductImageFullPath(lst[0].Picture);
        }

        private void LoadProductImages()
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProductImageWithCount("0", "1000", "DisplayOrder", ProductID);

            List<ProductImageEntity> lst = results.GetResult<ProductImageEntity>().ToList();

            // Added Main Picture to the list
            lst.Insert(0, new ProductImageEntity { Picture = mainProductPicture });

            rptOtherImages.DataSource = lst;
            rptOtherImages.DataBind();
        }

        private void LoadProductSizes()
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProductSizeImageWithCount("0", "1000", "DisplayOrder", ProductID);

            List<ProductSizeImageEntity> lst = results.GetResult<ProductSizeImageEntity>().ToList();

            rptSizes.DataSource = lst;
            rptSizes.DataBind();
        }

        private string ProductID
        {
            get
            {
                return Request.QueryString["ID"];
            }
        }

        protected void ddlColorMaterials_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadColorsBySelectedMaterial();
            
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "zoom", "initialize();", true);
        }

        protected void btnAddToCart_Click(object sender, EventArgs e)
        {
            string colorID = "0";
            if (ddlColors.Items.Count > 0)
                colorID = ddlColors.SelectedValue;

            int quantity = 1;
            DBHelper.ExecuteSQLFormat("exec SP_AddToCart {0}, {1}, {2}, {3}, {4}", DBHelper.SQuote(Session.SessionID), ProductID, colorID, lblPriceValue.Text, quantity);

            Response.Redirect("~/GioHang.aspx");
        
        }
    }
}


﻿<%@ Control language="C#" Inherits="Philip.Modules.ProductDetails.ViewProductDetails" CodeFile="ViewProductDetails.ascx.cs" AutoEventWireup="true"%>
<%--References: http://www.jacklmoore.com/zoom/ --%>
<%@ Register Src="~/controls/SocialShare.ascx" TagPrefix="dnn" TagName="SocialShare" %>	
<script src="<%= ResolveUrl("~/js/zoom-master/jquery.zoom.js") %>" type="text/javascript"></script>
<link href="<%= ResolveUrl("~/js/zoom-master/zoom.css") %>" rel="stylesheet" />
<script type="text/javascript">
    $(document).ready(function () {
        initialize();
    });

    function initialize() {
        initZoom();

        $(".photos").click(function () {
            $('.zoom').find("img").attr("src", $(this).find("img").attr("src"));
            $('.zoom').trigger('zoom.destroy'); // remove zoom
            initZoom();
        });

        $("#<%= ddlColors.ClientID %>").change(function () {
            $('.zoom').find("img").attr("src", $("#color_" + $(this).val()).attr("src"));
        });

        alignLandscapeImages();
    }

    function initZoom() {
        if (isMobile.any())
            return;

        $('.zoom').zoom();
    }

    function alignLandscapeImages() {
        var ImageHeight = 50;
        
        // For cache images like on IE
        if (isIE()) {
            $(".photos img").each(function() {
                var $img = $(this);
                if ($img.width() > $img.height()) {
                    $img.closest("div").css("position", "relative");
                    $img.parent().css("position", "absolute");
                    $img.parent().css("left", "0px");
                    $img.parent().css("top", ((ImageHeight - $img.height()) / 2) + "px");
                }
            });
        }
        
        // For new loaded images
        $(".photos img").load(function () {
            var $img = $(this);
            if ($img.width() > $img.height()) {
                $img.closest("div").css("position", "relative");
                $img.parent().css("position", "absolute");
                $img.parent().css("left", "0px");
                $img.parent().css("top", ((ImageHeight - $img.height()) / 2) + "px");
            }
        });
        
    }

</script>

    <div class="breadcrumbs">
        <asp:Repeater id="rptBreadcrumbs" runat="server">
            <ItemTemplate>
                <a href='<%# Eval("Value") %>'><%# Eval("Text") %></a>
            </ItemTemplate>
            <SeparatorTemplate> &gt; </SeparatorTemplate>
        </asp:Repeater>
    </div>

    <div class="mainImgConatiner">
        <asp:Repeater id="rptOtherImages" runat="server">
            <HeaderTemplate>
                <div class="moreImgThumbContainer">
            </HeaderTemplate>
            <ItemTemplate>
                <div class="imageThumb">
                    <a href="javascript:void(0);" class="photos">
                        <img alt="" src="<%# UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("Picture"))) %>"><%--<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 50) %>--%>
                    </a>
                </div>
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:Repeater>
        <!------ end more Img Thumb Container ----->
                    
        <div class="imgThumbContainer ">
            <div class="main-photo zoom"><%-- <%= ResolveUrl(imgPicture.ImageUrl) %>  title="MYTITLE"--%>
                <asp:Image ID="imgPicture" runat="server" />
            </div>
        </div><!----- end imgThumbContainer ------->
             
        <div class="prodColor">
            <asp:Literal runat="server" id="ltrColorsTitle2" Text="<p class='title_info'>Màu Sắc</p>"></asp:Literal>
            <asp:Repeater id="rptColors" runat="server">
                <HeaderTemplate>
                    <div class="colorProdImgthumb">
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="imageThumb">
                        <a href="javascript:void(0);" class="photos">
                            <img id="color_<%# Eval("ID") %>" alt="<%# Eval("ColorName") %>" src="<%# UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("Picture"))) %>">
                        </a>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            <!---- color Product Img ------->
        </div><!---- End product color ------>
    </div><!---- End main Img Conatiner ------>
                
    <div class="ProdInfoContainer">
        <div class="nameIDcontainer">
            <p class="prod_name"> <asp:Literal runat="server" id="ltrProductName"></asp:Literal> </p>
            <p class="prodID"> <asp:Literal runat="server" id="ltrProductCode"></asp:Literal> </p>
        </div><!---- End info Product Container ------>
                    
        <div class="info">
            <p> <strong>Giá :</strong> <asp:Label ID="lblPriceValue" runat="server" Visible="False"></asp:Label><asp:Label ID="lblPrice" runat="server"></asp:Label> <span> </span></p>
            <p class="order"> Đặt hàng : (08) 39.561.340  </p>
            <div class="materials">
                <table width="270px" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="80px"><p> <strong><asp:Literal runat="server" id="ltrColorMaterialsTitle" Text="Chất Liệu :"></asp:Literal></strong>  </p></td>
                        <td style="padding-bottom: 5px;"><asp:DropDownList runat="server" ID="ddlColorMaterials" DataTextField="MaterialName" DataValueField="ID" AutoPostBack="true" OnSelectedIndexChanged="ddlColorMaterials_SelectedIndexChanged"></asp:DropDownList></td>
                    </tr>
                </table>
            </div>
            <div class="color">
                <table width="270px" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="80px"><p> <strong><asp:Literal runat="server" id="ltrColorsTitle" Text="Màu :"></asp:Literal></strong> </p></td>
                        <td style="padding-bottom: 5px;"><asp:DropDownList runat="server" ID="ddlColors" DataTextField="ColorName" DataValueField="ID"></asp:DropDownList></td>
                    </tr>
                </table>
            </div>
            
            
            <asp:LinkButton id="btnAddToCart" runat="server" Text="Mua Hàng" OnClick="btnAddToCart_Click" CssClass="red-button btnOrder"  />
            
            
            <%--Size--%>
            <asp:Repeater id="rptSizes" runat="server">
                <HeaderTemplate>
                    <div class="size">
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="prod_size">
                        <a href="javascript:void(0);"> <%--class="photos"--%>
                            <img alt="" src="<%# UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(Eval("Picture"))) %>" width="85"> <%--<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 100) %>--%>
                        </a>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            
        </div><!---- End info ------>
    </div><!---- End Product info Conatiner ------>
                
    <div class="ProdInfoContainer2">
        <p class="title_info">Mô Tả Sản Phẩm</p>
        <p>
            <asp:Literal runat="server" id="ltrDescription"></asp:Literal>
        </p>
        <p class="title_info margin-top-15">Chất Liệu</p>
        <p>
            <asp:Literal runat="server" id="ltrMaterials"></asp:Literal>
        </p>
        <p class="title_info margin-top-15">Màu Sắc</p>
        <p>
            <asp:Literal runat="server" id="ltrColors"></asp:Literal>
        </p>
        <p class="title_info margin-top-15">Quy Định Bảo Hành</p>
        <p>
            <asp:Literal runat="server" id="ltrWarranty"></asp:Literal>
        </p>
        <br />

        <dnn:SocialShare Runat="server" ID="ctrSocialShare"></dnn:SocialShare>
    </div>
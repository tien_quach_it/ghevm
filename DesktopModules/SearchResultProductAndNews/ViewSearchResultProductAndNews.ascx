<%@ Control Language="C#" Inherits="Philip.Modules.SearchResultProductAndNews.ViewSearchResultProductAndNews"
    CodeFile="ViewSearchResultProductAndNews.ascx.cs" AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<div style="padding-bottom: 10px; padding-top: 10px;">
    <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
</div>
<div>
    <asp:Repeater ID="rptSearchPages" runat="server">
        <ItemTemplate>
            <div class="searchLeft">
                <%# RenderPicture(Eval("Picture"), Eval("ID"), Eval("Type"), Eval("SeoUrl"))%>
            </div>
            <div class="searchRight">
                <h1 class="calDataRow">
                    <%# RenderTitle(Eval("Title"), Eval("ID"), Eval("Type"), Eval("SeoUrl"))%></h1>
                <p class="calData">
                    <%# DNNHelper.TruncateText(FormatSearch(Eval("Summary").ToString()), 200)%></p>
            </div>
            <div class="clr"></div>
        </ItemTemplate>
    </asp:Repeater>
</div>
<div>
    <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
</div>

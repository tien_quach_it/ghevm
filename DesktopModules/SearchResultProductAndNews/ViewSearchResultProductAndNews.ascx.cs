﻿// 
// DotNetNuke® - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
namespace Philip.Modules.SearchResultProductAndNews
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewSearchResultProductAndNews class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewSearchResultProductAndNews : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        string Term
        {
            get
            {
                object o = ViewState["Term"];
                if (o == null)
                    return "";
                return ViewState["Term"].ToString();
            }
            set
            {
                ViewState["Term"] = value;
            }
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["term"] != null && Request.QueryString["term"].ToString() != "")
                    {
                        Term = Request.QueryString["term"].ToString();
                        LoadSearch(Term);
                    }
                    else
                    {
                        Response.Redirect("~/default.aspx");
                    }
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region LoadData
        public List<SearchProductAndNews> LoadData(string title)
        {
            try
            {
                title = title.Trim().Replace("'", "''");

                string noOfSearch = "20";

                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_SearchProductAndNews(CurrentPage.ToString(), noOfSearch, SortString, title);

                List<SearchProductAndNews> lst = results.GetResult<SearchProductAndNews>().ToList();
                int NumRows = results.GetResult<int>().Single();

                // Paging

                int totalpage = NumRows / Convert.ToInt16(noOfSearch);
                if (NumRows % Convert.ToInt16(noOfSearch) > 0)
                    totalpage = totalpage + 1;
                iexPaging.TotalPages = totalpage;
                iexPaging.Visible = totalpage > 1;

                lblError.Text = NumRows + " Kết quả tìm kiếm cho <span class='searchHighlight'>" + title + "</span>";

                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public void LoadSearch(string title)
        {
            var lst = LoadData(title);
            if (lst.Count > 0)
            {
                rptSearchPages.DataSource = lst;
                rptSearchPages.DataBind();
            }
            else
            {
                rptSearchPages.DataSource = null;
                rptSearchPages.DataBind();
                lblError.Text = "Không tìm thấy dữ liệu!";
            }
        }
        protected int CurrentPage
        {
            get
            {
                if (ViewState["CurPage"] == null)
                    ViewState["CurPage"] = 0;
                return Convert.ToInt32(ViewState["CurPage"]);
            }
            set
            {
                ViewState["CurPage"] = value;
            }
        }
        protected string SortString
        {
            get
            {
                if (ViewState["SortString"] == null)
                    return "Title";
                return ViewState["SortString"].ToString();
            }
            set
            {
                ViewState["SortString"] = value;
            }
        }
        protected void iexPaging_PageChanged(object sender, EventArgs e)
        {
            CurrentPage = iexPaging.CurrentPageIndex - 1;

            LoadSearch(Term);
        }
        protected string RenderTitle(object title, object id, object type, object seoUrl)
        {
            string url = RederLinkUrl(id, type, seoUrl);
            
            return String.Format(url, FormatSearch(title.ToString()));
        }
        protected string RenderPicture(object picture, object id, object type, object seoUrl)
        {
            string imageUrl = "<img width='80px' src='{0}' alt='' />";
            switch (type.ToString().ToLower())
            {
                case "product":
                    imageUrl = String.Format("<img src='{0}' alt='' />", UrlHelper.GetProductThumbUrl(Eval("Picture"), 80));
                    break;
                case "service":
                    imageUrl = String.Format(imageUrl, ResolveUrl(ImageHelper.GetServiceImageFullPath(picture.ToString())));
                    break;
                default:
                    imageUrl = String.Format(imageUrl, ResolveUrl(ImageHelper.GetNewsImageFullPath(picture.ToString())));
                    break;
            }
            string url = RederLinkUrl(id, type, seoUrl);

            return String.Format(url, imageUrl);
        }
        protected string RederLinkUrl(object id, object type, object seoUrl)
        {
            string url = "";

            if (!String.IsNullOrEmpty((string) seoUrl))
                url = seoUrl.ToString();
            else
            {
                switch (type.ToString().ToLower())
                {
                    case "product":
                        url = String.Format("/SanPham/ChiTiet.aspx?ID={0}", id.ToString());
                        break;
                    case "service":
                        url = String.Format("/DichVu/ChiTietDichVu.aspx?id={0}", id.ToString());
                        break;
                    default:
                        url = String.Format("/ChiTietTinTucSuKien.aspx?id={0}", id.ToString());
                        break;
                }
            }
            return String.Format("<a href='{0}'>{1}</a>", url, "{0}");
        }
        public string FormatSearch(string txt)
        {
            //string txtFormat = Regex.Replace(txt, Term, String.Format("<span class='searchHighlight'>{0}</span>", Term), RegexOptions.IgnoreCase);
            //return txtFormat;
            return txt;
        }
        #endregion
    }
}


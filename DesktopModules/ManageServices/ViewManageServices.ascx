﻿<%@ Control Language="C#" Inherits="Philip.Modules.ManageServices.ViewManageServices"
    CodeFile="ViewManageServices.ascx.cs" AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/texteditor.ascx" %>
<div>
    <asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red"></asp:Label>
    <asp:MultiView ID="vws" runat="server">
        <asp:View ID="vwView" runat="server">
            <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkAddNew" Text="Thêm Mới" runat="server" CssClass="button"
                            OnClick="lnkAddNew_Click"></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                <tr class="t_admin_table_header">
                    <th>
                    </th>
                    <th>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSortDisplayOrder" runat="server" Text="STT" CommandArgument="DisplayOrder"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSortTitle" runat="server" Text="Tiêu Đề" CommandArgument="Title"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSortPicture" runat="server" Text="Hình Ảnh" CommandArgument="Picture"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>
                    <%--<th>
                        <asp:LinkButton ID="btnSortSummary" runat="server" Text="Sơ Lược" CommandArgument="Summary"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th> --%>                   
                </tr>
                <asp:Repeater ID="rpt" runat="server">
                    <ItemTemplate>
                        <tr class="row<%# Container.ItemIndex % 2 %>">
                            <td>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Chỉnh Sửa" OnClick="lnkEdit_Click"
                                    CommandArgument='<%# Eval("ID") %>'></asp:LinkButton></th>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>'
                                    Text="Xóa" OnClientClick="return confirm('Bạn có muốn xóa dịch vụ này hay không?')"
                                    OnClick="lnkDelete_Click"></asp:LinkButton>
                            </td>
                            <td>
                                <%# Eval("DisplayOrder") %>
                            </td>
                            <td>
                                <%# Eval("Title") %>
                            </td>
                            <td>
                                <img width='80px' src='<%# ResolveUrl(ImageHelper.GetServiceImageFullPath(Eval("Picture").ToString())) %>'
                                    alt="" />
                            </td>
                            <%--<td>
                                <%# Eval("Summary") %>
                            </td>   --%>                         
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <div>
                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
            </div>
        </asp:View>
        <asp:View ID="vwEdit" runat="server">
            <table cellpadding="2" cellspacing="2" class="t_admintable">
                <tr class='row0'>
                    <td>
                        STT *
                    </td>
                    <td>
                        <asp:TextBox ID="txtDisplayOrder" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row1'>
                    <td>
                        Tiêu Đề *
                    </td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row0'>
                    <td>
                        Hình Ảnh *
                    </td>
                    <td>
                        <asp:FileUpload ID="fupPicture" runat="server" /><br />
                        <asp:Image ID="imgPicture" runat="server" Width="150px" />
                    </td>
                </tr>
                <tr class='row1'>
                    <td>
                        Sơ Lược *
                    </td>
                    <td>
                        <asp:TextBox ID="txtSummary" runat="server" Width="400" TextMode="MultiLine" Rows="10"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row0'>
                    <td>
                        Chi Tiết *
                    </td>
                    <td>
                        <dnn:TextEditor ID="txtDescription" runat="server" Width="500" height="400"></dnn:TextEditor>
                    </td>
                </tr>                
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Lưu Lại" CssClass="button" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Hủy" CssClass="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>

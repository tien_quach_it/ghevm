<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Settings.ascx.cs" Inherits="Philip.Modules.Shops.Settings" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<div class="mspsContent dnnClear">
    <h2 class="dnnFormSectionHead">
        <a class="dnnSectionExpanded" href="">Map Layout</a></h2>
    <fieldset style="display: block;">
        <div class="dnnFormItem">
            <div class="dnnTooltip">
                Width:
            </div>
            <asp:TextBox ID="txtWidth" runat="server" Width="100px"></asp:TextBox>
        </div>
        <div class="dnnFormItem">
            <div class="dnnTooltip">
                Height:
            </div>
            <asp:TextBox ID="txtHeight" runat="server" Width="100px"></asp:TextBox>
        </div>
        <div class="dnnFormItem">
            <div class="dnnTooltip">
                Zoom:
            </div>
            <asp:TextBox ID="txtZoom" runat="server" Width="100px"></asp:TextBox>
        </div>
    </fieldset>
</div>

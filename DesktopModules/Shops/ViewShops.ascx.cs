﻿// 
// DotNetNuke® - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Web.Script.Serialization;
using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;

namespace Philip.Modules.Shops
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewShops class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewShops : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    Initialize();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region Ha.Huynh Added

        void Initialize()
        {
            // ProvinceID
            ProvinceManager dalProvinceID = new ProvinceManager();
            var lstProvinceID = dalProvinceID.GetProvinces().OrderBy(o=>o.ProvinceName).ToList();

            ddlProvinceID.DataSource = lstProvinceID;
            ddlProvinceID.DataTextField = "ProvinceName";
            ddlProvinceID.DataValueField = "ID";
            ddlProvinceID.DataBind();
            ddlProvinceID.Items.Insert(0, new ListItem("Tỉnh / Thành", "0"));

            // RegionID
            //RegionManager dalRegionID = new RegionManager();
            //var lstRegionID = dalRegionID.GetRegions().ToList();

            //ddlRegionID.DataSource = lstRegionID;
            //ddlRegionID.DataTextField = "RegionName";
            //ddlRegionID.DataValueField = "ID";
            //ddlRegionID.DataBind();
            ddlRegionID.Items.Insert(0, new ListItem("Khu Vực", "0"));


        }

        public string GetMapList()
        {
            ShopManager dal = new ShopManager();
            IList<ShopEntity> lst = dal.GetShops();

            var json = new JavaScriptSerializer().Serialize(lst);
            return json;
        }

        public string MapWidth
        {
            get
            {
                return DNNHelper.GetModuleSetting(Settings, "MapWidth", "720px");
            }
        }

        public string MapHeight
        {
            get
            {
                return DNNHelper.GetModuleSetting(Settings, "MapHeight", "630px");
            }
        }

        public string MapZoom
        {
            get
            {
                return DNNHelper.GetModuleSetting(Settings, "MapZoom", "10");
            }
        }
        #endregion

    }
}


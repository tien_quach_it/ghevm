﻿<%@ Control language="C#" Inherits="Philip.Modules.Shops.ViewShops" CodeFile="ViewShops.ascx.cs" AutoEventWireup="true"%>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
var mZoom = <%= MapZoom %>;
</script>
<script src="<%= ResolveUrl("~/js/map/maps.js")%>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('html, body').animate({
            scrollTop: 275
        }, 1000);

        var aShops = <%= GetMapList() %>;        
        $(function () {  
            // Not load data by default for mobile
            if (isMobile.any()) {
                getShopsMap([]);
                return;
            }

            getShopsMap(aShops);
        });

        $('.submit').click(function() {
            var selectedProvince = $('#<%= ddlProvinceID.ClientID %>').val();
            var selectedRegion = $('#<%= ddlRegionID.ClientID %>').val();
            if (selectedProvince == "0")
            {
                alert("Vui lòng nhập Tỉnh Thành");
                return false;
            }

            if (selectedRegion == "0")
            {
                alert("Vui lòng nhập Khu Vực");
                return false;
            }

            var obj = { province: selectedProvince, region: selectedRegion };
            $.ajax({
                type: "POST",
                url: "/getShopList.asmx/GetMapList",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = JSON.parse(data.d);
                    if(result.length == 0)
                    {
                        alert('Không tồn tại cửa hàng ở khu vực này');
                        return false;
                    }
                    else
                    {
                        getShopsMap(JSON.parse(data.d));                         
                    }
                },
                error: function (data, errorThrown) {
                    alert("Fail");
                    alert(errorThrown);
                }
            })
            return false;
        });   
        
        $('.province').change(function(){
            var obj = { province: $('#<%= ddlProvinceID.ClientID %>').val() };
            $.ajax({
                type: "POST",
                url: "/getShopList.asmx/GetRegionByProvince",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = JSON.parse(data.d);
                    var $ddlRegionID = $("#<%= ddlRegionID.ClientID %>");
                    var options = ''; 
                    options += '<option value="0">Khu Vực</option>';
                    for(var i = 0; i< result.length; i++)
                    {
                        options += '<option value="' + result[i].ID + '">' + result[i].RegionName + '</option>';
                    }
                    $ddlRegionID.html(options);
                },
                error: function (data, errorThrown) {
                    alert("Fail");
                    alert(errorThrown);
                }
            })
            return false;
        });     
    })
</script>
<script src="<%= ResolveUrl("~/js/map/markerclusterer.js")%>" type="text/javascript"></script>
<div style="position: relative;">
    <div id="shop_map_canvas" style="overflow: hidden;
        position: relative; background-color: rgb(229, 227, 223);"><%--width: <%= MapWidth %>; height: <%= MapHeight %>;--%>
    </div>
    <div class="topSearchMap">
        <div class="contentSearchMap">
            <div class="searchMapItem">
                <strong>Tìm Kiếm</strong>
            </div>
            <div class="searchMapItem">
                <asp:DropDownList ID="ddlProvinceID" CssClass="province" Width="150px" runat="server"></asp:DropDownList>
            </div>
            <div class="searchMapItem">
                <asp:DropDownList ID="ddlRegionID" Width="150px" runat="server"></asp:DropDownList>
            </div>
            <div style="text-align: right">
                <input type="submit" class="submit shop-button" value="Tìm Kiếm" />
            </div>
        </div>
    </div>
</div>

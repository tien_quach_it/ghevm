﻿// 
// DotNetNuke® - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;

namespace Philip.Modules.ManageShops
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewManageShops class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewManageShops : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                // Set Title
                //ModuleConfiguration.ModuleTitle = Localization.GetString("ManageRegions", this.LocalResourceFile);
                //ModuleConfiguration.ModuleTitle = Localization.GetString("ManageProvinces", this.LocalResourceFile);

                lblErr.Text = "";
                if (!IsPostBack)
                {
                    IsAddNew = false;
                    vws.ActiveViewIndex = 0;
                    Initialize();
                    ResetAllSort();
                    BindGrid();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString("ManageProvinces", this.LocalResourceFile), ModuleActionType.EditContent, "", "", this.EditUrl("ManageProvinces"), false, SecurityAccessLevel.Edit, true, false);
                //Actions.Add(this.GetNextActionID(), Localization.GetString("ManageRegions", this.LocalResourceFile), ModuleActionType.EditContent, "", "", this.EditUrl("ManageRegions"), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region Generated
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, ""));
        }
        void Initialize()
        {
            // ProvinceID
            ProvinceManager dalProvinceID = new ProvinceManager();
            var lstProvinceID = dalProvinceID.GetProvinces().OrderBy(o => o.ProvinceName).ToList();

            ddlProvinceID.DataSource = lstProvinceID;
            ddlProvinceID.DataTextField = "ProvinceName";
            ddlProvinceID.DataValueField = "ID";
            ddlProvinceID.DataBind();
            ddlProvinceID.Items.Insert(0, new ListItem("", "0"));

            ddlProvinceFilter.DataSource = lstProvinceID;
            ddlProvinceFilter.DataTextField = "ProvinceName";
            ddlProvinceFilter.DataValueField = "ID";
            ddlProvinceFilter.DataBind();
            ddlProvinceFilter.Items.Insert(0, new ListItem("Chọn Tỉnh Thành", ""));

            // RegionID
            //RegionManager dalRegionID = new RegionManager();
            //var lstRegionID = dalRegionID.GetRegions().ToList();

            //ddlRegionFilter.DataSource = lstRegionID;
            //ddlRegionFilter.DataTextField = "RegionName";
            //ddlRegionFilter.DataValueField = "ID";
            //ddlRegionFilter.DataBind();
            ddlRegionFilter.Items.Insert(0, new ListItem("Chọn Khu Vực", ""));
            ddlRegionID.Items.Insert(0, new ListItem("", "0"));
        }

        public List<ShopViewEntity> LoadData()
        {
            try
            {
                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_ShopWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString, ddlProvinceFilter.SelectedValue, ddlRegionFilter.SelectedValue);

                List<ShopViewEntity> lst = results.GetResult<ShopViewEntity>().ToList();
                int NumRows = results.GetResult<int>().Single();

                // Paging

                int totalpage = NumRows / PageSize;
                if (NumRows % PageSize > 0)
                    totalpage = totalpage + 1;
                iexPaging.TotalPages = totalpage;
                iexPaging.Visible = totalpage > 1;

                return lst;
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
                return null;
            }
        }

        bool hasNoRow;
        public void BindGrid()
        {
            try
            {
                var lst = LoadData();
                rpt.DataSource = lst;
                rpt.DataBind();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        private bool IsAddNew
        {
            get
            {
                object o = Session["IsAddNew"];
                if (o != null)
                {
                    return Convert.ToBoolean(o);
                }
                return false;
            }
            set { Session["IsAddNew"] = value; }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                int editindex = Convert.ToInt32(lnk.CommandArgument);
                EditID = editindex;
                vws.ActiveViewIndex = 1;

                //MappingStoredProcedure dal = new MappingStoredProcedure();
                //IMultipleResults results = dal.SP_ShopWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString, ddlProvinceFilter.SelectedValue, ddlRegionFilter.SelectedValue);

                //ShopViewEntity obj = results.GetResult<ShopViewEntity>().Where(o => o.ID == EditID).First();

                ShopEntity obj = new ShopManager().GetShopById(EditID);

                if (obj == null)
                    return;

                if (ddlProvinceID.Items.FindByValue(obj.ProvinceID.ToString()) != null)
                {
                    ddlProvinceID.SelectedValue = obj.ProvinceID.ToString();
                    LoadRegionByProvinceID(obj.ProvinceID);
                }
                if (ddlRegionID.Items.FindByValue(obj.RegionID.ToString()) != null)
                    ddlRegionID.SelectedValue = obj.RegionID.ToString();
                txtName.Text = obj.Name;
                txtAddress.Text = obj.Address;
                txtLongitude.Text = obj.Longitude;
                txtLatitude.Text = obj.Latitude;
                txtForceLatitude.Text = obj.ForceLatitude;
                txtForceLongitude.Text = obj.ForceLongitude;
                txtPhone.Text = obj.Phone;
                txtDisplayOrder.Text = obj.DisplayOrder.ToString();
                txtEmail.Text = obj.Email;

            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = sender as LinkButton;
                int id = Convert.ToInt32(lnk.CommandArgument);
                //Delete
                ShopManager dal = new ShopManager();
                //dal.Delete(id);

                ShopEntity obj = dal.GetShopById(id);
                obj.Deleted = true;
                dal.Update(obj);

                //EndDelete
                BindGrid();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            EditID = 0;
            ClearForm();
            SetNextDisplayOrder();
            vws.ActiveViewIndex = 1;
        }

        private void SetNextDisplayOrder()
        {
            txtDisplayOrder.Text = DBHelper.ExecuteScalar("select IsNull(max(DisplayOrder), 0) + 1 from Shop where Deleted = 0");
        }

        bool ValidateForm()
        {
            // DisplayOrder
            if (String.IsNullOrEmpty(txtDisplayOrder.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập STT!";
                return false;
            }
            Int32 tmpDisplayOrder;
            if (!Int32.TryParse(txtDisplayOrder.Text.Trim(), out tmpDisplayOrder))
            {
                lblErr.Text = "STT không đúng!";
                return false;
            }

            if (ddlProvinceID.SelectedIndex == 0)
            {
                lblErr.Text = "Vui lòng chọn Tỉnh Thành!";
                return false;
            }
            // Name
            if (String.IsNullOrEmpty(txtName.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Tên Cửa Hàng!";
                return false;
            }

            // Address
            if (String.IsNullOrEmpty(txtAddress.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Địa chỉ!";
                return false;
            }

            return true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            ShopManager dal = new ShopManager();
            ShopEntity obj;

            if (EditID == 0)
                obj = new ShopEntity();
            else
            {
                obj = dal.GetShopById(EditID);
            }

            obj.ProvinceID = Convert.ToInt32(ddlProvinceID.SelectedItem.Value);
            if (ddlRegionID.SelectedItem.Value != "")
                obj.RegionID = Convert.ToInt32(ddlRegionID.SelectedItem.Value);
            obj.Name = txtName.Text;
            obj.Address = txtAddress.Text;
            obj.Longitude = txtLongitude.Text;
            obj.Latitude = txtLatitude.Text;
            obj.Phone = txtPhone.Text;
            obj.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text);
            obj.Email = txtEmail.Text;
            obj.ForceLatitude = txtForceLatitude.Text;
            obj.ForceLongitude = txtForceLongitude.Text;

            if (EditID == 0)
                dal.Insert(obj);
            else
                dal.Update(obj);

            vws.ActiveViewIndex = 0;

            BindGrid();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            vws.ActiveViewIndex = 0;
        }

        void ClearForm()
        {
            ddlProvinceID.SelectedIndex = 0;
            ddlRegionID.SelectedIndex = 0;
            txtName.Text = "";
            txtAddress.Text = "";
            txtLongitude.Text = "";
            txtLatitude.Text = "";
            txtForceLongitude.Text = "";
            txtForceLatitude.Text = "";
            txtPhone.Text = "";
            txtDisplayOrder.Text = "";
            txtEmail.Text = "";

        }

        protected int EditID
        {
            get
            {
                if (ViewState["EditID"] == null)
                    return 0;
                return Convert.ToInt32(ViewState["EditID"]);
            }
            set
            {
                ViewState["EditID"] = value;
            }
        }

        LinkButton btnSort;
        protected void btnSort_Click(object sender, EventArgs e)
        {
            ResetAllSort();

            btnSort = (sender as LinkButton);
            SortString = String.Format("{0} {1}", btnSort.CommandArgument, btnSort.CommandName);
            if (String.IsNullOrEmpty(btnSort.CommandName))
            {
                btnSort.CssClass = "sortAsc";
                btnSort.CommandName = "Desc";
            }
            else
            {
                btnSort.CssClass = "sortDesc";
                btnSort.CommandName = "";
            }

            BindGrid();
        }

        void ResetAllSort()
        {
            btnSortName.CssClass = "sortNone";
            btnSortAddress.CssClass = "sortNone";
            btnSortPhone.CssClass = "sortNone";
            //btnSortDisplayOrder.CssClass = "sortNone";
            btnSortProvinceName.CssClass = "sortNone";
            btnSortRegionName.CssClass = "sortNone";
            btnSortEmail.CssClass = "sortNone";
        }

        protected string SortString
        {
            get
            {
                if (ViewState["SortString"] == null)
                    return "DisplayOrder";
                return ViewState["SortString"].ToString();
            }
            set
            {
                ViewState["SortString"] = value;
            }
        }

        protected void iexPaging_PageChanged(object sender, EventArgs e)
        {
            CurrentPage = iexPaging.CurrentPageIndex - 1;

            BindGrid();
        }

        protected int PageSize
        {
            get
            {
                return 20;
            }
        }

        protected int CurrentPage
        {
            get
            {
                if (ViewState["CurPage"] == null)
                    ViewState["CurPage"] = 0;
                return Convert.ToInt32(ViewState["CurPage"]);
            }
            set
            {
                ViewState["CurPage"] = value;
            }
        }

        public void LoadRegionByProvinceID(int provinceID)
        {
            RegionManager dalRegionID = new RegionManager();
            var lstRegionID = dalRegionID.GetRegions().Where(o => o.ProvinceID == provinceID).ToList();

            ddlRegionID.DataSource = lstRegionID;
            ddlRegionID.DataTextField = "RegionName";
            ddlRegionID.DataValueField = "ID";
            ddlRegionID.DataBind();
            ddlRegionID.Items.Insert(0, new ListItem("", "0"));
        }

        protected void ddlProvinceID_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRegionByProvinceID(Convert.ToInt32(ddlProvinceID.SelectedValue));
        }

        public void LoadFilterRegionByProvinceID(int provinceID)
        {
            RegionManager dalRegionID = new RegionManager();
            var lstRegionID = dalRegionID.GetRegions().Where(o => o.ProvinceID == provinceID).OrderBy(o => o.RegionName).ToList();

            ddlRegionFilter.DataSource = lstRegionID;
            ddlRegionFilter.DataTextField = "RegionName";
            ddlRegionFilter.DataValueField = "ID";
            ddlRegionFilter.DataBind();
            ddlRegionFilter.Items.Insert(0, new ListItem("Chọn Khu Vực", ""));
        }
        protected void ddlProvinceFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProvinceFilter.SelectedValue != "")
            {
                LoadFilterRegionByProvinceID(Convert.ToInt32(ddlProvinceFilter.SelectedValue));
            }
            BindGrid();
        }
        protected void ddlRegionFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
        protected void lnkFilter_Click(object sender, EventArgs e)
        {
            BindGrid();
        }
        #endregion
    }
}


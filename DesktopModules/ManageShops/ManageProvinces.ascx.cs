﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;

using System.Data;
using System.Linq;
using System.Data.Linq;
using System.IO;
using Core.DAL;

public partial class DesktopModules_ManageShops_ManageProvinces : PortalModuleBase, IActionable
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Set Title
            ModuleConfiguration.ModuleTitle = Localization.GetString("ManageProvinces", this.LocalResourceFile);

            lblErr.Text = "";
            if (!IsPostBack)
            {
                IsAddNew = false;
                vws.ActiveViewIndex = 0;
                Initialize();
                ResetAllSort();
                BindGrid();
            }
        }
        catch (Exception exc) //Module failed to load
        {
            Exceptions.ProcessModuleLoadException(this, exc);
        }

    }

    #region Optional Interfaces

    /// -----------------------------------------------------------------------------
    /// <summary>
    /// Registers the module actions required for interfacing with the portal framework
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    public ModuleActionCollection ModuleActions
    {
        get
        {
            ModuleActionCollection Actions = new ModuleActionCollection();
            Actions.Add(this.GetNextActionID(), Localization.GetString("ManageRegions", this.LocalResourceFile), ModuleActionType.EditContent, "", "", this.EditUrl("ManageRegions"), false, SecurityAccessLevel.Edit, true, false);
            return Actions;
        }
    }

    #endregion

    #region Generated
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, ""));
    }
    void Initialize()
    {

    }

    public List<ProvinceViewEntity> LoadData()
    {
        try
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProvinceWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString);

            List<ProvinceViewEntity> lst = results.GetResult<ProvinceViewEntity>().ToList();
            int NumRows = results.GetResult<int>().Single();

            // Paging

            int totalpage = NumRows / PageSize;
            if (NumRows % PageSize > 0)
                totalpage = totalpage + 1;
            iexPaging.TotalPages = totalpage;
            iexPaging.Visible = totalpage > 1;

            return lst;
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
            return null;
        }
    }

    bool hasNoRow;
    public void BindGrid()
    {
        try
        {
            var lst = LoadData();
            rpt.DataSource = lst;
            rpt.DataBind();
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    private bool IsAddNew
    {
        get
        {
            object o = Session["IsAddNew"];
            if (o != null)
            {
                return Convert.ToBoolean(o);
            }
            return false;
        }
        set { Session["IsAddNew"] = value; }
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            int editindex = Convert.ToInt32(lnk.CommandArgument);
            EditID = editindex;
            vws.ActiveViewIndex = 1;

            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProvinceWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString);

            ProvinceViewEntity obj = results.GetResult<ProvinceViewEntity>().Where(o => o.ID == EditID).First();
            int count = results.GetResult<int>().Single();

            txtProvinceName.Text = obj.ProvinceName;
            txtDisplayOrder.Text = obj.DisplayOrder.ToString();

        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = sender as LinkButton;
            int id = Convert.ToInt32(lnk.CommandArgument);
            //Delete
            ProvinceManager dal = new ProvinceManager();
            //dal.Delete(id);

            ProvinceEntity obj = dal.GetProvinceById(id);
            obj.Deleted = true;
            dal.Update(obj);

            //EndDelete
            BindGrid();
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        EditID = 0;
        ClearForm();
        vws.ActiveViewIndex = 1;
    }

    bool ValidateForm()
    {
        // ProvinceName
        if (String.IsNullOrEmpty(txtProvinceName.Text.Trim()))
        {
            lblErr.Text = "Vui lòng nhập tỉnh thành!";
            return false;
        }

        // DisplayOrder
        if (String.IsNullOrEmpty(txtDisplayOrder.Text.Trim()))
        {
            lblErr.Text = "Vui lòng nhập STT!";
            return false;
        }
        Int32 tmpDisplayOrder;
        if (!Int32.TryParse(txtDisplayOrder.Text.Trim(), out tmpDisplayOrder))
        {
            lblErr.Text = "STT không đúng!";
            return false;
        }


        return true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!ValidateForm())
        {
            return;
        }

        ProvinceManager dal = new ProvinceManager();
        ProvinceEntity obj;

        if (EditID == 0)
            obj = new ProvinceEntity();
        else
        {
            obj = dal.GetProvinceById(EditID);
        }

        obj.ProvinceName = txtProvinceName.Text;
        obj.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text);


        if (EditID == 0)
            dal.Insert(obj);
        else
            dal.Update(obj);

        vws.ActiveViewIndex = 0;

        BindGrid();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        vws.ActiveViewIndex = 0;
    }

    void ClearForm()
    {
        txtProvinceName.Text = "";
        txtDisplayOrder.Text = "";

    }

    protected int EditID
    {
        get
        {
            if (ViewState["EditID"] == null)
                return 0;
            return Convert.ToInt32(ViewState["EditID"]);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }

    LinkButton btnSort;
    protected void btnSort_Click(object sender, EventArgs e)
    {
        ResetAllSort();

        btnSort = (sender as LinkButton);
        SortString = String.Format("{0} {1}", btnSort.CommandArgument, btnSort.CommandName);
        if (String.IsNullOrEmpty(btnSort.CommandName))
        {
            btnSort.CssClass = "sortAsc";
            btnSort.CommandName = "Desc";
        }
        else
        {
            btnSort.CssClass = "sortDesc";
            btnSort.CommandName = "";
        }

        BindGrid();
    }

    void ResetAllSort()
    {
        btnSortProvinceName.CssClass = "sortNone";
        btnSortDisplayOrder.CssClass = "sortNone";

        //btnSortID.CssClass = "sortNone";            
    }

    protected string SortString
    {
        get
        {
            if (ViewState["SortString"] == null)
                return "DisplayOrder";
            return ViewState["SortString"].ToString();
        }
        set
        {
            ViewState["SortString"] = value;
        }
    }

    protected void iexPaging_PageChanged(object sender, EventArgs e)
    {
        CurrentPage = iexPaging.CurrentPageIndex - 1;

        BindGrid();
    }

    protected int PageSize
    {
        get
        {
            return 20;
        }
    }

    protected int CurrentPage
    {
        get
        {
            if (ViewState["CurPage"] == null)
                ViewState["CurPage"] = 0;
            return Convert.ToInt32(ViewState["CurPage"]);
        }
        set
        {
            ViewState["CurPage"] = value;
        }
    }

    #endregion
}
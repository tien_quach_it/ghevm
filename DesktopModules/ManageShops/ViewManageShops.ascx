﻿<%@ Control Language="C#" Inherits="Philip.Modules.ManageShops.ViewManageShops" CodeFile="ViewManageShops.ascx.cs"
    AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<div>
    <asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red"></asp:Label>
    <asp:MultiView ID="vws" runat="server">
        <asp:View ID="vwView" runat="server">
            <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                <tr>
                    <td align="center">
                        Tỉnh Thành: <asp:DropDownList runat="server" ID="ddlProvinceFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlProvinceFilter_SelectedIndexChanged" /> &nbsp;
                        Khu Vực: <asp:DropDownList runat="server" ID="ddlRegionFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlRegionFilter_SelectedIndexChanged" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkAddNew" Text="Thêm Mới" runat="server" CssClass="button"
                            OnClick="lnkAddNew_Click"></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                <tr class="t_admin_table_header">
                    <th>
                    </th>
                    <th>
                    </th>
                    <%--<th>
                        <asp:LinkButton ID="btnSortDisplayOrder" runat="server" Text="STT" CommandArgument="DisplayOrder"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>--%>
                    <th>
                        <asp:LinkButton ID="btnSortName" runat="server" Text="Tên Cửa Hàng" CommandArgument="Name"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSortEmail" runat="server" Text="Email" CommandArgument="Email"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSortAddress" runat="server" Text="Địa Chỉ" CommandArgument="Address"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSortPhone" runat="server" Text="ĐT" CommandArgument="Phone"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSortRegionName" runat="server" Text="Khu Vực" CommandArgument="RegionName"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>                    
                    <th>
                        <asp:LinkButton ID="btnSortProvinceName" runat="server" Text="Tỉnh Thành" CommandArgument="ProvinceName"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>                    
                </tr>
                <asp:Repeater ID="rpt" runat="server">
                    <ItemTemplate>
                        <tr class="row<%# Container.ItemIndex % 2 %>">
                            <td>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Chỉnh Sửa" OnClick="lnkEdit_Click"
                                    CommandArgument='<%# Eval("ID") %>'></asp:LinkButton></th>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>'
                                    Text="Xóa" OnClientClick="return confirm('Bạn có muốn xóa cửa hàng này không?')"
                                    OnClick="lnkDelete_Click"></asp:LinkButton>
                            </td>
                            <%--<td>
                                <%# Eval("DisplayOrder") %>
                            </td>--%>
                            <td>
                                <%# Eval("Name") %>
                            </td>
                            <td>
                                <%# Eval("Email") %>
                            </td>
                            <td>
                                <%# Eval("Address") %>
                            </td>
                            <td>
                                <%# Eval("Phone") %>
                            </td>                            
                            <td>
                                <%# Eval("RegionName") %>
                            </td>
                            <td>
                                <%# Eval("ProvinceName") %>
                            </td>                            
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <div>
                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
            </div>
        </asp:View>
        <asp:View ID="vwEdit" runat="server">
            <table cellpadding="2" cellspacing="2" class="t_admintable">
                <tr class='row1' style="display: none">
                    <td>
                        STT *
                    </td>
                    <td>
                        <asp:TextBox ID="txtDisplayOrder" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row1'>
                    <td>
                        Chọn Tỉnh Thành *
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlProvinceID" AutoPostBack="true" OnSelectedIndexChanged="ddlProvinceID_OnSelectedIndexChanged" CssClass="province" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class='row0'>
                    <td>
                        Chọn Khu Vực
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlRegionID" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class='row1'>
                    <td>
                        Tên Cửa Hàng *
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row1'>
                    <td>
                        Email
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row0'>
                    <td>
                        Địa chỉ *
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddress" CssClass="address" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row1'>
                    <td>
                        Longitude *
                    </td>
                    <td>
                        <asp:TextBox ID="txtLongitude" CssClass="longitude" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row0'>
                    <td>
                        Latitude *
                    </td>
                    <td>
                        <asp:TextBox ID="txtLatitude" CssClass="latitude" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="background-color: #FEF5AF">
                        Click vào <a href="http://itouchmap.com/latlong.html" target="_blank" class="button">đây</a> hoặc 
                        <a href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm" target="_blank" class="button">đây</a>
                        để tìm tọa độ thay thế nếu toạ độ trên không chính xác, sau đó lấy tọa độ mới nhập vào ô bên dưới:
                    </td>
                </tr>
                <tr class='row1'>
                    <td>
                        Force Longitude
                    </td>
                    <td>
                        <asp:TextBox ID="txtForceLongitude" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row0'>
                    <td>
                        Force Latitude
                    </td>
                    <td>
                        <asp:TextBox ID="txtForceLatitude" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row1'>
                    <td>
                        ĐT
                    </td>
                    <td>
                        <asp:TextBox ID="txtPhone" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Lưu Lại" CssClass="button" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Hủy" CssClass="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>
<br />
&nbsp; <asp:LinkButton ID="btnBack" Text="<< Back To Page" runat="server" CssClass="button" OnClick="btnBack_Click"></asp:LinkButton>
<script type="text/javascript"
    src="http://maps.googleapis.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
    var geocoder;

    $(function () {
        //GEOCODER
        geocoder = new google.maps.Geocoder();

        var geoLatLngCode = function (address) {
            if (address == undefined || address == "")
                return;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var lat = results[0].geometry.location.lat();
                    var lng = results[0].geometry.location.lng();
                    $('.longitude').val(lng);
                    $('.latitude').val(lat);
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            })
        }

        var $address = $('.address');
        geoLatLngCode($address.val());

        $address.blur(function () {
            var address = $(this).val();
            geoLatLngCode(address);
        });
    });
</script>
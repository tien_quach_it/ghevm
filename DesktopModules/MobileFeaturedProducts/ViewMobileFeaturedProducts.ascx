﻿<%@ Control Language="C#" Inherits="Philip.Modules.MobileFeaturedProducts.ViewMobileFeaturedProducts" CodeFile="ViewMobileFeaturedProducts.ascx.cs" AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<link href="<%= ResolveUrl("~/DesktopModules/MobileFeaturedProducts/css/ImagesCarousel.css") %>" rel="stylesheet" />
<div class="container hidden-lg hidden-md hidden-sm">
<div class="title-bar-mobile"> 
    <p> Sản Phẩm Mới</p> 
</div>
<div class="product_carousel1 bottomSliderContainer">
    <div style="width: 510px; overflow: hidden; height: 175px;" class="ui-carousel carousel" id="carousel1">
        <asp:Repeater id="rptNewProducts" runat="server">
            <ItemTemplate>
                <div class="product"><%--<%# Container.ItemIndex % 3 == 1 ? "product-margin" : "" %>--%>
                    <div class="product_image_inner">
                        <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><img src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture")) %>" border="0" alt='<%# DNNHelper.GetSeoAltText(Eval("ProductCode")) %>'></a>
                    </div>
                    <div class="product_info">
                        <div class="product_code">
                            <p> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><%# Eval("ProductCode") %></a> </p>
                        </div>
                        <div class="detail">
                            <p> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>">Chi Tiết</a> </p>
                        </div>
                    </div> 
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <a href="#" class="ui-carousel-next" id="ui-carousel-next1">
        <img src="/desktopmodules/MobileFeaturedProducts/images/carousel-arrow-right.png" alt=""></a>
    <a href="#" class="ui-carousel-prev" id="ui-carousel-prev1">
        <img src="/desktopmodules/MobileFeaturedProducts/images/carousel-arrow-left.png" alt=""></a>
</div>
<asp:Repeater ID="rptFeaturedCategories" runat="server" OnItemDataBound="rptFeaturedCategories_ItemDataBound">
    <ItemTemplate>
        <div class="title-bar-mobile"> 
            <p> <%# Eval("CategoryName") %> </p> 
            <asp:Literal ID="ltrID" runat="server" Text='<%# Eval("ID") %>' Visible="False"></asp:Literal>
        </div>
        <div class="product_carousel<%# Container.ItemIndex + 2 %> bottomSliderContainer">
            <div style="width: 510px; overflow: hidden; height: 175px;" class="ui-carousel carousel" id="carousel<%# Container.ItemIndex + 2 %>">
                <asp:Repeater ID="rptProducts" runat="server">
                    <ItemTemplate>                
                        <div class="product">
                            <div class="product_image_inner">
                                <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><img src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture")) %>" border="0" alt='<%# DNNHelper.GetSeoAltText(Eval("ProductCode")) %>'></a>
                            </div> 
                            <div class="product_info">
                                <div class="product_code">
                                    <p> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><%# Eval("ProductCode") %></a> </p>
                                </div>
                                <div class="detail">
                                    <p> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>">Chi Tiết</a> </p>
                                </div>
                            </div> 
                        </div>                    
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <a href="#" class="ui-carousel-next" id="ui-carousel-next<%# Container.ItemIndex + 2 %>">
                <img src="/desktopmodules/MobileFeaturedProducts/images/carousel-arrow-right.png" alt=""></a>
            <a href="#" class="ui-carousel-prev" id="ui-carousel-prev<%# Container.ItemIndex + 2 %>">
                <img src="/desktopmodules/MobileFeaturedProducts/images/carousel-arrow-left.png" alt=""></a>
        </div> 
    </ItemTemplate>
</asp:Repeater>
</div>
<% if (!DNNHelper.IsAdmin(DNNHelper.CurrentUser))
   { %>
<script src="<%= ControlPath %>js/jquery.ui.core.js" type="text/javascript"></script>
<script src="<%= ControlPath %>js/jquery.ui.widget.js" type="text/javascript"></script>
<% } %>

<script src="<%= ControlPath %>js/jquery.ui.rcarousel.js" type="text/javascript"></script>
<script src="<%= ControlPath %>js/script.js" type="text/javascript"></script>

﻿$(document).ready(function () {
    // Carousel
    var widthCheck = 970; // max with of carousel
    var itemWidth = 175;
    var itemHeight = 175;
    var resizeTimer = null;

    var windowWidth = $(window).width();
    var verticalFlg = windowWidth > widthCheck;
    var mobileFlg = windowWidth > 767;
    
    var visibleStep = 3;
    if (!mobileFlg) {
        visibleStep = parseInt(windowWidth / (itemWidth + 40));
    }
    else if (!verticalFlg) {
        visibleStep = parseInt(windowWidth / (itemWidth + 100));
    }

    visibleStep = visibleStep == 0 ? 1 : visibleStep;    
    visibleStep = visibleStep > 3 ? 3 : visibleStep;

    // Carousel options
    var enabled = false;
    var interval = 2000;

    var objs = [];
    for (var i = 0; i < 8; i++) {
        var index = i + 1;
        var obj = jQuery('#carousel' + index).clone();
        objs.push(obj);

        var ui_carousel_next = "#ui-carousel-next" + index;
        var ui_carousel_prev = "#ui-carousel-prev" + index;
        $("#carousel" + index).rcarousel({
            auto: {
                enabled: enabled,
                interval: interval,
                direction: "next"
            },
            width: itemWidth,
            height: itemHeight,
            step: visibleStep,
            visible: visibleStep,
            margin: 20,
            orientation: "horizontal",
            navigation: {
                next: ui_carousel_next,
                prev: ui_carousel_prev
            }
        });
    }

    $(window).resize(function () {             
        if ($(window).width() - windowWidth == 0)
            return;

        windowWidth = $(window).width();

        resizeTimer && clearTimeout(resizeTimer); // Cleraring old timer to avoid unwanted resize calls.
        resizeTimer = setTimeout(function () {
            var flg = ($(window).width() > widthCheck);
            if (true) {

                verticalFlg = flg;

                mobileFlg = $(window).width() > 767;

                visibleStep = 3;
                if (!mobileFlg)
                    visibleStep = parseInt($(window).width() / (itemWidth + 40));
                else if (!verticalFlg) {
                    visibleStep = parseInt($(window).width() / (itemWidth + 100));
                }

                visibleStep = visibleStep == 0 ? 1 : visibleStep;
                visibleStep = visibleStep > 3 ? 3 : visibleStep;

                for (var i = 0; i < objs.length; i++) {
                    var index = i + 1;

                    var $bottomSliderContainer = $('#carousel' + index).closest(".product_carousel" + index);
                    $bottomSliderContainer.html($(objs[i]).clone());
                    $bottomSliderContainer.append('<a href="#" class="ui-carousel-next" id="ui-carousel-next' + index + '"><img src="/desktopmodules/MobileFeaturedProducts/images/carousel-arrow-right.png" alt=""></a> <a href="#" class="ui-carousel-prev" id="ui-carousel-prev' + index + '"><img src="/desktopmodules/MobileFeaturedProducts/images/carousel-arrow-left.png" alt=""></a>');

                    var ui_carousel_next = "#ui-carousel-next" + index;
                    var ui_carousel_prev = "#ui-carousel-prev" + index;
                    $("#carousel" + index).rcarousel({
                        auto: {
                            enabled: enabled,
                            interval: interval,
                            direction: "next"
                        },
                        width: itemWidth,
                        height: itemHeight,
                        step: visibleStep,
                        visible: visibleStep,
                        margin: 20,
                        orientation: "horizontal",
                        navigation: {
                            next: ui_carousel_next,
                            prev: ui_carousel_prev
                        }
                    });
                }
            }
        }, 200);
    });

    //$(".bottomSliderContainer .item").live("mouseover", function (e) {
    //    e.preventDefault();
    //    $(this).find('.headline').hide();
    //    $(this).find('.bodyContent').show();
    //});
    //$(".bottomSliderContainer .item").live("mouseout", function (e) {
    //    e.preventDefault();
    //    $(this).find('.headline').show();
    //    $(this).find('.bodyContent').hide();
    //});
});
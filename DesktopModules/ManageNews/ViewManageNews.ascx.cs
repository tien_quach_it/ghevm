﻿// 
// DotNetNuke® - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;

using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;

namespace Philip.Modules.ManageNews
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewManageNews class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewManageNews : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                lblErr.Text = "";
                if (!IsPostBack)
                {
                    IsAddNew = false;
                    vws.ActiveViewIndex = 0;
                    Initialize();
                    ResetAllSort();
                    BindGrid();                    
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region Generated

        void Initialize()
        {

        }

        public List<NewsViewEntity> LoadData()
        {
            try
            {
                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_NewsWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString);

                List<NewsViewEntity> lst = results.GetResult<NewsViewEntity>().ToList();
                int NumRows = results.GetResult<int>().Single();

                // Paging

                int totalpage = NumRows / PageSize;
                if (NumRows % PageSize > 0)
                    totalpage = totalpage + 1;
                iexPaging.TotalPages = totalpage;
                iexPaging.Visible = totalpage > 1;

                return lst;
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
                return null;
            }
        }

        bool hasNoRow;
        public void BindGrid()
        {
            try
            {
                var lst = LoadData();
                rpt.DataSource = lst;
                rpt.DataBind();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        private bool IsAddNew
        {
            get
            {
                object o = Session["IsAddNew"];
                if (o != null)
                {
                    return Convert.ToBoolean(o);
                }
                return false;
            }
            set { Session["IsAddNew"] = value; }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                int editindex = Convert.ToInt32(lnk.CommandArgument);
                EditID = editindex;
                vws.ActiveViewIndex = 1;

                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_NewsWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString);

                NewsViewEntity obj = results.GetResult<NewsViewEntity>().Where(o => o.ID == EditID).First();
                int count = results.GetResult<int>().Single();

                txtTitle.Text = obj.Title;
                if (String.IsNullOrEmpty(obj.Picture))
                    imgPicture.Visible = false;
                else
                {
                    imgPicture.ImageUrl = ImageHelper.GetNewsImageFullPath(obj.Picture);
                    imgPicture.Visible = true;
                }
                txtSummary.Text = obj.Summary;
                txtDescription.Text = obj.Description;
                txtDisplayOrder.Text = obj.DisplayOrder.ToString();

            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = sender as LinkButton;
                int id = Convert.ToInt32(lnk.CommandArgument);
                //Delete
                NewsManager dal = new NewsManager();
                //dal.Delete(id);

                NewsEntity obj = dal.GetNewsById(id);
                obj.Deleted = true;
                dal.Update(obj);

                //EndDelete
                BindGrid();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            EditID = 0;
            ClearForm();
            SetNextDisplayOrder();
            vws.ActiveViewIndex = 1;
        }

        private void SetNextDisplayOrder()
        {
            txtDisplayOrder.Text = DBHelper.ExecuteScalar("select IsNull(max(DisplayOrder), 0) + 1 from [News] where Deleted = 0");
        }

        bool ValidateForm()
        {
            // DisplayOrder
            if (String.IsNullOrEmpty(txtDisplayOrder.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập STT!";
                return false;
            }
            Int32 tmpDisplayOrder;
            if (!Int32.TryParse(txtDisplayOrder.Text.Trim(), out tmpDisplayOrder))
            {
                lblErr.Text = "STT không đúng!";
                return false;
            }

            // Title
            if (String.IsNullOrEmpty(txtTitle.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập tiêu đề!";
                return false;
            }

            // Summary
            if (String.IsNullOrEmpty(txtSummary.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập sơ lược!";
                return false;
            }

            // Description
            if (String.IsNullOrEmpty(txtDescription.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập chi tiết!";
                return false;
            }

            return true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            NewsManager dal = new NewsManager();
            NewsEntity obj;

            if (EditID == 0)
                obj = new NewsEntity();
            else
            {
                obj = dal.GetNewsById(EditID);
            }

            obj.Title = txtTitle.Text;
            if (fupPicture.HasFile)
            {
                // Delete old file
                if (!String.IsNullOrEmpty(obj.Picture))
                {
                    string oldFile = String.Format("{0}{1}", Server.MapPath(ImageHelper.GetNewsImagePath()), obj.Picture);
                    if (File.Exists(oldFile))
                        File.Delete(oldFile);
                }
                // Upload new file
                obj.Picture = DNNHelper.UploadFile(fupPicture.PostedFile, Server.MapPath(ImageHelper.GetNewsImagePath()));
            }
            else
            {
                if (EditID == 0)
                {
                    obj.Picture = "";
                }
            }

            txtDescription.HtmlEncode = false;
            obj.Summary = txtSummary.Text;
            obj.Description = txtDescription.Text;
            obj.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text);
            obj.DateCreated = DateTime.Now;


            if (EditID == 0)
                dal.Insert(obj);
            else
                dal.Update(obj);

            vws.ActiveViewIndex = 0;

            BindGrid();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            vws.ActiveViewIndex = 0;
        }

        void ClearForm()
        {
            txtTitle.Text = "";
            imgPicture.Visible = false;
            txtSummary.Text = "";
            txtDescription.Text = "";
            txtDisplayOrder.Text = "";

        }

        protected int EditID
        {
            get
            {
                if (ViewState["EditID"] == null)
                    return 0;
                return Convert.ToInt32(ViewState["EditID"]);
            }
            set
            {
                ViewState["EditID"] = value;
            }
        }

        LinkButton btnSort;
        protected void btnSort_Click(object sender, EventArgs e)
        {
            ResetAllSort();

            btnSort = (sender as LinkButton);
            SortString = String.Format("{0} {1}", btnSort.CommandArgument, btnSort.CommandName);
            if (String.IsNullOrEmpty(btnSort.CommandName))
            {
                btnSort.CssClass = "sortAsc";
                btnSort.CommandName = "Desc";
            }
            else
            {
                btnSort.CssClass = "sortDesc";
                btnSort.CommandName = "";
            }

            BindGrid();
        }

        void ResetAllSort()
        {
            btnSortTitle.CssClass = "sortNone";
            btnSortPicture.CssClass = "sortNone";
            btnSortSummary.CssClass = "sortNone";
            btnSortDisplayOrder.CssClass = "sortNone";

            //btnSortID.CssClass = "sortNone";            
        }

        protected string SortString
        {
            get
            {
                if (ViewState["SortString"] == null)
                    return "DisplayOrder";
                return ViewState["SortString"].ToString();
            }
            set
            {
                ViewState["SortString"] = value;
            }
        }

        protected void iexPaging_PageChanged(object sender, EventArgs e)
        {
            CurrentPage = iexPaging.CurrentPageIndex - 1;

            BindGrid();
        }

        protected int PageSize
        {
            get
            {
                return 20;
            }
        }

        protected int CurrentPage
        {
            get
            {
                if (ViewState["CurPage"] == null)
                    ViewState["CurPage"] = 0;
                return Convert.ToInt32(ViewState["CurPage"]);
            }
            set
            {
                ViewState["CurPage"] = value;
            }
        }

        #endregion
    }
}


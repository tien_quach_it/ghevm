﻿<%@ Control language="C#" Inherits="Philip.Modules.BestSellProducts.ViewBestSellProducts" CodeFile="ViewBestSellProducts.ascx.cs" AutoEventWireup="true"%>
<div class="hot_product_box">
    <div class="title-bar">
        <p> Sản Phẩm Bán Chạy </p>
    </div>
    <div class="hotproduct_container">
        <marquee width="208" height="350" onmouseout="this.start()" onmouseover="this.stop()" direction="up" scrollamount="8" style="overflow: hidden">
            <asp:Repeater ID="rptBestSell" runat="server">
                <ItemTemplate>
                    <div class="hot_product">
                        <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>">
                            <img src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 220) %>" alt='<%# DNNHelper.GetSeoAltText(Eval("ProductCode")) %>' width="208" border="0"  />
                        </a>
                    </div>
                </ItemTemplate>
                <SeparatorTemplate>
                    <div class="gray-line"></div>
                </SeparatorTemplate>
            </asp:Repeater>
        </marquee>
        <%--<div class="hot_product">
            <a href="#" target="_blank">
                <img src="<%= SkinPath%>images/product/Hot-VM_97.png" alt="VM-97"  />
            </a>
            <img src="<%= SkinPath%>images/line.png" alt="line" />
        </div>
        <div class="hot_product">
            <a href="#" target="_blank">
                <img src="<%= SkinPath%>images/product/Hot-VM_10.png" alt="VM-10"  />
            </a>
            <img src="<%= SkinPath%>images/line.png" alt="line" />
        </div>
        <div class="hot_product">
            <a href="#" target="_blank">
                <img src="<%= SkinPath%>images/product/Hot-VM_46.png" alt="VM-46"  />
            </a>
            <img src="<%= SkinPath%>images/line.png" alt="line" />
        </div>
        <div class="hot_product">
            <a href="#" target="_blank">
                <img src="<%= SkinPath%>images/product/Hot-VM_74.png" alt="VM-74"  />
            </a>
            <img src="<%= SkinPath%>images/line.png" alt="line" />
        </div>--%>
                    
    </div>
</div> <!--- end_hot_product --->
﻿<%@ Control language="C#" Inherits="Philip.Modules.MyCart.ViewMyCart" CodeFile="ViewMyCart.ascx.cs" AutoEventWireup="true"%>

	<table cellspacing="0" cellpadding="0" border="0" class="table_product_list">
        <tbody><tr>
            <td class="prodID_title">
                <p> <strong>Mã Sản Phẩm</strong> </p>
            </td>
            <td class="prodName_title">
                <p> <strong>Sản Phẩm</strong> </p>
            </td>
            <td class="prodAmount_title">
                <p> <strong>Số Lượng</strong> </p>
            </td>
            <td class="prodPrice_title">
                <p> <strong>Giá</strong> </p>
            </td>
        </tr>
        <asp:Repeater ID="rptCart" runat="server">
            <ItemTemplate>
        <tr class="cart-item">
            <td class="prodID">
                <p> <%# Eval("ProductCode") %> </p>
                <asp:Label ID="lblProductID" runat="server" Text='<%# Eval("ProductID") %>'  Visible="false"></asp:Label>
                <asp:Label ID="lblColorID" runat="server" Text='<%# Eval("ColorID") %>'  Visible="false"></asp:Label> 
            </td>
            <td class="prod_purchased">
                <table width="351" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td class="prodImg">
                            <img width="71" alt="<%# Eval("ProductCode") %>" src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 220) %>">
                        </td>
                        <td class="prodName">
                            <%# Eval("ProductName") %><br />
                            <%# Eval("MaterialName") %><br />
                            <%# Eval("ColorName") %> 
                        </td>
                    </tr>
                </tbody></table>
            </td>
            <td class="prod_amount">
                <asp:TextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' Width="50" CssClass="quantity"></asp:TextBox>
            </td>
            <td class="prod_price">
                <p>
                    <asp:Label ID="lblPriceLabel" runat="server" Text='<%# GetPrice(Eval("Price"))  %>'></asp:Label>
                    <span style="display: none"><asp:Label ID="lblPrice" runat="server" Text='<%# Eval("Price") %>' CssClass="price"></asp:Label></span> 
                </p><%--<span> VND </span>--%>
            </td>
        </tr>
            </ItemTemplate>
        </asp:Repeater>
                    
        <tr>
            <td class="total" colspan="3">
                <p>
                    Tổng Cộng : </p>
            </td>

            <td class="total_price" align="center">
                <%--<span class="float">--%>
                    <strong><p class="total"> <asp:Literal runat="server" ID="lblTotal"></asp:Literal> </p><%--<span>VND</span>--%></strong>
                <%--</span>--%>
            </td>
        </tr>
    </tbody></table>       
    
    <div class="checkout-container">
        <asp:LinkButton ID="btnUpdateCart" runat="server" OnClick="btnUpdateCart_Click" CssClass="red-button" Text="Cập Nhật" />
        <asp:LinkButton ID="btnCheckOut" runat="server" OnClick="btnCheckOut_Click" CssClass="red-button" Text="Đặt Hàng" />
    </div>	


<script type="text/javascript">
    $(function () {
        numberOnlyForQuantity();

        $(".quantity").change(function () {
            calculateTotal(); 
        });
    });

    function numberOnlyForQuantity() {
        $(".quantity").keydown(function (event) {
            // Allow: backspace, delete, tab, escape, enter 
            if ($.inArray(event.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            else {
                // Exclude - and . characters
                if (event.keyCode == 189 || event.keyCode == 190)
                    event.preventDefault();
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
        });
    }

    function calculateTotal() {
        var total = 0;

        $(".cart-item").each(function () {
            var $item = $(this);
            var price = parseFloat($item.find(".price").text());
            var quantity = parseInt($item.find(".quantity").val());
            total += price * quantity;
        });
        
        if (total == 0)
            return;

        $("p.total").html(formatMoney(total));
    }
</script>
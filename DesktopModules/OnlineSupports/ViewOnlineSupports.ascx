﻿<%@ Control language="C#" Inherits="Philip.Modules.OnlineSupports.ViewOnlineSupports" CodeFile="ViewOnlineSupports.ascx.cs" AutoEventWireup="true"%>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<div class="support">
    <div class="title-bar">
        <p> Hổ Trợ trực tuyến </p>
    </div>
    <div class="support_container">
        <div class="support_icon">
            <img src="/portals/0/Skins/GheVM/images/hotline_ico.png" alt="hotline icon" />
        </div>
        <div class="hotline">
            <p> LIÊN HỆ MỞ ĐẠI LÝ : </p>
            <p class="number"> 072.3759.677 </p>
			<p class="number"> 072.3759.678 </p>
		</div>
        <asp:Repeater ID="rptSupportOnline" runat="server">
            <ItemTemplate>
                <div class="Yhonoff_icon">
                    <a href="ymsgr:sendIM?<%# Eval("YahooID") %>">
                        <img src="http://opi.yahoo.com/online?u=<%# Eval("YahooID") %>&amp;m=g&amp;t=2">
                    </a>
                </div>    
            </ItemTemplate>
        </asp:Repeater>
        <div class="support_icon">
            <img src="/portals/0/Skins/GheVM/images/hotline_ico.png" alt="hotline icon" />
        </div>
        <div class="hotline">
            <p> SHOWROOM : </p>
            <p class="number"> (08) 39.561.340  </p>
        </div>
        <asp:Repeater ID="rptSupportOnline2" runat="server">
            <ItemTemplate>
                <div class="Yhonoff_icon">
                    <a href="ymsgr:sendIM?<%# Eval("YahooID") %>">
                        <img src="http://opi.yahoo.com/online?u=<%# Eval("YahooID") %>&amp;m=g&amp;t=2">
                    </a>
                </div>    
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div><!--- end_support --->

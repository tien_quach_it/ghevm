﻿// 
// DotNetNuke® - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;

using System.Data;
using Core.DAL;
using System.Linq;
using System.Data.Linq;
using System.IO;

namespace Modules.ManageOrders
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewManageOrders class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewManageOrders : PortalModuleBase, IActionable
    {

        #region Private Members

        private string strTemplate;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                lblErr.Text = "";
                if (!IsPostBack)
                {
                    LoadUserSettings();

                    LoadSystemSettings();

                    IsAddNew = false;
                    vws.ActiveViewIndex = 0;
                    Initialize();
                    ResetAllSort();
                    BindGrid();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        private void LoadUserSettings()
        {
            IsAdmin = DNNHelper.CurrentUser.IsSuperUser || DNNHelper.CurrentUser.IsInRole("Administrators") || DNNHelper.CurrentUser.IsInRole("GheVM");
            pnlTopAdmin.Visible = IsAdmin;

            btnSave.Visible = IsAdmin;
        }

        protected bool IsAdmin
        {
            get
            {
                return Convert.ToBoolean(ViewState["IsAdmin"]);
            }
            set
            {
                ViewState["IsAdmin"] = value;
            }
        }


        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region Generated

        void Initialize()
        {
            txtTotal.Attributes.Add("readonly", "readonly");
            txtOnDate.Attributes.Add("readonly", "readonly");

            // StatusID
            OrderStatusManager dalStatusID = new OrderStatusManager();
            var lstStatusID = dalStatusID.GetOrderStatuss().ToList();

            ddlStatusID.DataSource = lstStatusID;
            ddlStatusID.DataTextField = "StatusName";
            ddlStatusID.DataValueField = "ID";
            ddlStatusID.DataBind();
            ddlStatusID.Items.Insert(0, new ListItem("", "0"));


        }

        public List<OrderViewEntity> LoadData()
        {
            try
            {
                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_OrderWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString);

                List<OrderViewEntity> lst = results.GetResult<OrderViewEntity>().ToList();
                int NumRows = results.GetResult<int>().Single();

                // Paging

                int totalpage = NumRows / PageSize;
                if (NumRows % PageSize > 0)
                    totalpage = totalpage + 1;
                iexPaging.TotalPages = totalpage;
                iexPaging.Visible = totalpage > 1;

                return lst;
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
                return null;
            }
        }

        bool hasNoRow;
        public void BindGrid()
        {
            try
            {
                var lst = LoadData();
                rpt.DataSource = lst;
                rpt.DataBind();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        private bool IsAddNew
        {
            get
            {
                object o = Session["IsAddNew"];
                if (o != null)
                {
                    return Convert.ToBoolean(o);
                }
                return false;
            }
            set { Session["IsAddNew"] = value; }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                int editindex = Convert.ToInt32(lnk.CommandArgument);
                EditID = editindex;
                vws.ActiveViewIndex = 1;

                MappingStoredProcedure dal = new MappingStoredProcedure();
                IMultipleResults results = dal.SP_OrderWithCount(CurrentPage.ToString(), PageSize.ToString(), SortString);

                OrderEntity obj = results.GetResult<OrderEntity>().Where(o => o.ID == EditID).First();
                int count = results.GetResult<int>().Single();

                txtFullName.Text = obj.FullName;
                txtAddress.Text = obj.Address;
                txtPhone.Text = obj.Phone;
                //txtFax.Text = obj.Fax;
                txtEmail.Text = obj.Email;
                txtComment.Text = obj.Comment;
                if (ddlStatusID.Items.FindByValue(obj.StatusID.ToString()) != null)
                    ddlStatusID.SelectedValue = obj.StatusID.ToString();
                txtTotal.Text = (obj.Total.ToString());
                txtOnDate.Text = DNNHelper.FormatDate(obj.OnDate);
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = sender as LinkButton;
                int id = Convert.ToInt32(lnk.CommandArgument);
                //Delete
                OrderManager dal = new OrderManager();
                //dal.Delete(id);

                OrderEntity obj = dal.GetOrderById(id);
                obj.Deleted = true;
                dal.Update(obj);

                //EndDelete
                BindGrid();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            EditID = 0;
            ClearForm();
            vws.ActiveViewIndex = 1;
        }

        bool ValidateForm()
        {
            // FullName
            if (String.IsNullOrEmpty(txtFullName.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Họ Tên!";
                return false;
            }

            // Address
            if (String.IsNullOrEmpty(txtAddress.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Địa Chỉ!";
                return false;
            }

            // Phone
            if (String.IsNullOrEmpty(txtPhone.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Điện Thoại!";
                return false;
            }

            // Fax
            //if (String.IsNullOrEmpty(txtFax.Text.Trim()))
            //{
            //    lblErr.Text = "Please enter Fax!";
            //    return false;
            //}

            // Email
            if (String.IsNullOrEmpty(txtEmail.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Email!";
                return false;
            }

            // Comment
            //if (String.IsNullOrEmpty(txtComment.Text.Trim()))
            //{
            //    lblErr.Text = "Please enter Comment!";
            //    return false;
            //}

            //if (ddlStatusID.SelectedIndex == 0)
            //{
            //    lblErr.Text = "Please select Status ID!";
            //    return false;
            //}
            // Total
            //if (String.IsNullOrEmpty(txtTotal.Text.Trim()))
            //{
            //    lblErr.Text = "Please enter Total!";
            //    return false;
            //}
            //Double tmpTotal;
            //if (!Double.TryParse(txtTotal.Text.Trim(), out tmpTotal))
            //{
            //    lblErr.Text = "Invalid Total!";
            //    return false;
            //}

            // OnDate
            if (String.IsNullOrEmpty(txtOnDate.Text.Trim()))
            {
                lblErr.Text = "Vui lòng nhập Ngày!";
                return false;
            }
            //DateTime tmpOnDate;
            //if (!DateTime.TryParse(txtOnDate.Text.Trim(), out tmpOnDate))
            //{
            //    lblErr.Text = "Invalid On Date!";
            //    return false;
            //}


            return true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            OrderManager dal = new OrderManager();
            OrderEntity obj;

            if (EditID == 0)
                obj = new OrderEntity();
            else
            {
                obj = dal.GetOrderById(EditID);
            }

            obj.FullName = txtFullName.Text;
            obj.Address = txtAddress.Text;
            obj.Phone = txtPhone.Text;
            obj.Fax = ""; //txtFax.Text;
            obj.Email = txtEmail.Text;
            obj.Comment = txtComment.Text;
            obj.StatusID = Convert.ToInt32(ddlStatusID.SelectedItem.Value);
            obj.Total = Convert.ToDouble(txtTotal.Text);
            obj.OnDate = Convert.ToDateTime(txtOnDate.Text);


            if (EditID == 0)
                dal.Insert(obj);
            else
                dal.Update(obj);

            vws.ActiveViewIndex = 0;

            BindGrid();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            vws.ActiveViewIndex = 0;
        }

        void ClearForm()
        {
            txtFullName.Text = "";
            txtAddress.Text = "";
            txtPhone.Text = "";
            //txtFax.Text = "";
            txtEmail.Text = "";
            txtComment.Text = "";
            ddlStatusID.SelectedIndex = 0;
            txtTotal.Text = "";
            txtOnDate.Text = "";

        }

        protected int EditID
        {
            get
            {
                if (ViewState["EditID"] == null)
                    return 0;
                return Convert.ToInt32(ViewState["EditID"]);
            }
            set
            {
                ViewState["EditID"] = value;
            }
        }

        LinkButton btnSort;
        protected void btnSort_Click(object sender, EventArgs e)
        {
            ResetAllSort();

            btnSort = (sender as LinkButton);
            SortString = String.Format("{0} {1}", btnSort.CommandArgument, btnSort.CommandName);
            if (String.IsNullOrEmpty(btnSort.CommandName))
            {
                btnSort.CssClass = "sortAsc";
                btnSort.CommandName = "Desc";
            }
            else
            {
                btnSort.CssClass = "sortDesc";
                btnSort.CommandName = "";
            }

            BindGrid();
        }

        void ResetAllSort()
        {
            btnSortFullName.CssClass = "sortNone";
            //btnSortAddress.CssClass = "sortNone";
            btnSortPhone.CssClass = "sortNone";
            //btnSortFax.CssClass = "sortNone";
            //btnSortEmail.CssClass = "sortNone";
            //btnSortComment.CssClass = "sortNone";
            btnSortTotal.CssClass = "sortNone";
            btnSortOnDate.CssClass = "sortNone";

            //btnSortID.CssClass = "sortNone";            
        }

        protected string SortString
        {
            get
            {
                if (ViewState["SortString"] == null)
                    return "ID desc";
                return ViewState["SortString"].ToString();
            }
            set
            {
                ViewState["SortString"] = value;
            }
        }

        protected void iexPaging_PageChanged(object sender, EventArgs e)
        {
            CurrentPage = iexPaging.CurrentPageIndex - 1;

            BindGrid();
        }

        protected int PageSize
        {
            get
            {
                return 20;
            }
        }

        protected int CurrentPage
        {
            get
            {
                if (ViewState["CurPage"] == null)
                    ViewState["CurPage"] = 0;
                return Convert.ToInt32(ViewState["CurPage"]);
            }
            set
            {
                ViewState["CurPage"] = value;
            }
        }

        #endregion

        private void LoadSystemSettings()
        {
            SystemSettingsManager manager = new SystemSettingsManager();
            SystemSettingsEntity settings = manager.GetSystemSettingsById(1);
            chkEnableShoppingCart.Checked = settings.EnableShoppingCart;
            chkNoPrice.Checked = settings.NoPrice;
        }

        protected void chkEnableShoppingCart_CheckedChanged(object sender, EventArgs e)
        {
            DBHelper.ExecuteScalarFormat("Update SystemSettings Set EnableShoppingCart = {0}", Convert.ToInt32(chkEnableShoppingCart.Checked));
        }

        protected void chkNoPrice_CheckedChanged(object sender, EventArgs e)
        {
            DBHelper.ExecuteScalarFormat("Update SystemSettings Set NoPrice = {0}", Convert.ToInt32(chkNoPrice.Checked));
        }
    }
}


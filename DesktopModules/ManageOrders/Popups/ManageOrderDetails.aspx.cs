﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DesktopModules_ManageOrders_Popups_ManageOrderDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblErr.Text = "";
        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(OrderID))
                return;

            IsAdmin = DNNHelper.CurrentUser.IsSuperUser || DNNHelper.CurrentUser.IsInRole("Administrators") || DNNHelper.CurrentUser.IsInRole("GheVM");
            lnkAddNew.Visible = IsAdmin;

            InitializeOrder();

            IsAddNew = false;
            vws.ActiveViewIndex = 0;
            Initialize();
            ResetAllSort();
            BindGrid();
        }
    }

    private void InitializeOrder()
    {
        OrderManager manager = new OrderManager();
        OrderEntity order = manager.GetOrderById(Convert.ToInt32(OrderID));
        ltrOrderID.Text = order.ID.ToString();
        lblTotal.Text = DNNHelper.FormatPrice(order.Total);
    }

    private string OrderID
    {
        get
        {
            return Request.QueryString["OrderID"];
        }
    }

    #region Generated

    void Initialize()
    {
        // ProductID
        ProductManager dalProductID = new ProductManager();
        var lstProductID = dalProductID.GetProducts().ToList();

        ddlProductID.DataSource = lstProductID;
        ddlProductID.DataTextField = "ProductCode";
        ddlProductID.DataValueField = "ID";
        ddlProductID.DataBind();
        //ddlProductID.Items.Insert(0, new ListItem("", "0"));

        LoadColorMaterials();
        LoadColorsBySelectedMaterial();
    }

    public List<OrderDetailViewEntity> LoadData()
    {
        try
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_OrderDetailWithCount(OrderID, CurrentPage.ToString(), PageSize.ToString(), SortString);

            List<OrderDetailViewEntity> lst = results.GetResult<OrderDetailViewEntity>().ToList();
            int NumRows = results.GetResult<int>().Single();

            // Paging

            int totalpage = NumRows / PageSize;
            if (NumRows % PageSize > 0)
                totalpage = totalpage + 1;
            iexPaging.TotalPages = totalpage;
            iexPaging.Visible = totalpage > 1;

            return lst;
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
            return null;
        }
    }

    bool hasNoRow;
    public void BindGrid()
    {
        try
        {
            var lst = LoadData();
            rpt.DataSource = lst;
            rpt.DataBind();
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    private bool IsAddNew
    {
        get
        {
            object o = Session["IsAddNew"];
            if (o != null)
            {
                return Convert.ToBoolean(o);
            }
            return false;
        }
        set { Session["IsAddNew"] = value; }
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            int editindex = Convert.ToInt32(lnk.CommandArgument);
            EditID = editindex;
            vws.ActiveViewIndex = 1;

            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_OrderDetailWithCount(OrderID, CurrentPage.ToString(), PageSize.ToString(), SortString);

            OrderDetailViewEntity obj = results.GetResult<OrderDetailViewEntity>().Where(o => o.ID == EditID).First();
            int count = results.GetResult<int>().Single();

            CurrentOrderDetail = obj;

            if (ddlProductID.Items.FindByValue(obj.ProductID.ToString()) != null)
                ddlProductID.SelectedValue = obj.ProductID.ToString();
            if (ddlColorID.Items.FindByValue(obj.ColorID.ToString()) != null)
                ddlColorID.SelectedValue = obj.ColorID.ToString();
            txtPrice.Text = DNNHelper.FormatDouble(obj.Price);
            txtQuantity.Text = obj.Quantity.ToString();

            LoadColorMaterials();
            LoadColorsBySelectedMaterial();

        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    private OrderDetailViewEntity CurrentOrderDetail
    {
        get
        {
            return (OrderDetailViewEntity) ViewState["CurrentOrderDetail"];
        }
        set
        {
            ViewState["CurrentOrderDetail"] = value;
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = sender as LinkButton;
            int id = Convert.ToInt32(lnk.CommandArgument);
            //Delete
            OrderDetailManager dal = new OrderDetailManager();
            dal.Delete(id);

            //OrderDetailEntity obj = dal.GetOrderDetailById(id);
            //obj.Deleted = true;
            //dal.Update(obj);

            //EndDelete
            BindGrid();

            // Update Order Total
            DBHelper.ExecuteSQLFormat("update [Order] set Total = (select Sum(Price * Quantity) from OrderDetail where OrderID = {0}) where ID = {0}", OrderID);
            
            InitializeOrder();
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.Message;
        }
    }

    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        EditID = 0;
        CurrentOrderDetail = null;
        ClearForm();
        vws.ActiveViewIndex = 1;
    }

    bool ValidateForm()
    {
        //if (ddlProductID.SelectedIndex == 0)
        //{
        //    lblErr.Text = "Please select Product ID!";
        //    return false;
        //}
        //if (ddlColorID.SelectedIndex == 0)
        //{
        //    lblErr.Text = "Please select Color ID!";
        //    return false;
        //}
        // Price
        if (String.IsNullOrEmpty(txtPrice.Text.Trim()))
        {
            lblErr.Text = "Please enter Price!";
            return false;
        }
        Double tmpPrice;
        if (!Double.TryParse(txtPrice.Text.Trim(), out tmpPrice))
        {
            lblErr.Text = "Invalid Price!";
            return false;
        }

        // Quantity
        if (String.IsNullOrEmpty(txtQuantity.Text.Trim()))
        {
            lblErr.Text = "Please enter Quantity!";
            return false;
        }
        Int32 tmpQuantity;
        if (!Int32.TryParse(txtQuantity.Text.Trim(), out tmpQuantity))
        {
            lblErr.Text = "Invalid Quantity!";
            return false;
        }


        return true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!ValidateForm())
        {
            return;
        }

        OrderDetailManager dal = new OrderDetailManager();
        OrderDetailEntity obj;

        if (EditID == 0)
            obj = new OrderDetailEntity();
        else
        {
            obj = dal.GetOrderDetailById(EditID);
        }

        obj.OrderID = Convert.ToInt32(OrderID);
        obj.ProductID = Convert.ToInt32(ddlProductID.SelectedItem.Value);
        obj.ColorID = Convert.ToInt32(ddlColorID.SelectedItem.Value);
        obj.Price = Convert.ToDouble(txtPrice.Text);
        obj.Quantity = Convert.ToInt32(txtQuantity.Text);


        if (EditID == 0)
            dal.Insert(obj);
        else
            dal.Update(obj);


        // Update Order Total
        DBHelper.ExecuteSQLFormat("update [Order] set Total = (select Sum(Price * Quantity) from OrderDetail where OrderID = {0}) where ID = {0}", OrderID);
        
        vws.ActiveViewIndex = 0;

        BindGrid();
        InitializeOrder();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        vws.ActiveViewIndex = 0;
    }

    void ClearForm()
    {
        ddlProductID.SelectedIndex = 0;
        LoadColorMaterials();
        LoadColorsBySelectedMaterial();
        //ddlColorID.SelectedIndex = 0;
        txtPrice.Text = "";
        txtQuantity.Text = "";

    }

    protected int EditID
    {
        get
        {
            if (ViewState["EditID"] == null)
                return 0;
            return Convert.ToInt32(ViewState["EditID"]);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }

    LinkButton btnSort;
    protected void btnSort_Click(object sender, EventArgs e)
    {
        ResetAllSort();

        btnSort = (sender as LinkButton);
        SortString = String.Format("{0} {1}", btnSort.CommandArgument, btnSort.CommandName);
        if (String.IsNullOrEmpty(btnSort.CommandName))
        {
            btnSort.CssClass = "sortAsc";
            btnSort.CommandName = "Desc";
        }
        else
        {
            btnSort.CssClass = "sortDesc";
            btnSort.CommandName = "";
        }

        BindGrid();
    }

    void ResetAllSort()
    {
        btnSortPrice.CssClass = "sortNone";
        btnSortQuantity.CssClass = "sortNone";
        btnSortPicture.CssClass = "sortNone";
        btnSortProductCode.CssClass = "sortNone";
        //btnSortProductName.CssClass = "sortNone";
        btnSortColorName.CssClass = "sortNone";
        btnSortMaterialName.CssClass = "sortNone";

        //btnSortID.CssClass = "sortNone";            
    }

    protected string SortString
    {
        get
        {
            if (ViewState["SortString"] == null)
                return "ID";
            return ViewState["SortString"].ToString();
        }
        set
        {
            ViewState["SortString"] = value;
        }
    }

    protected void iexPaging_PageChanged(object sender, EventArgs e)
    {
        CurrentPage = iexPaging.CurrentPageIndex - 1;

        BindGrid();
    }

    protected int PageSize
    {
        get
        {
            return 20;
        }
    }

    protected int CurrentPage
    {
        get
        {
            if (ViewState["CurPage"] == null)
                ViewState["CurPage"] = 0;
            return Convert.ToInt32(ViewState["CurPage"]);
        }
        set
        {
            ViewState["CurPage"] = value;
        }
    }

    #endregion

    private List<ProductColorViewEntity> lstColors;
    private void LoadProductColors()
    {
        MappingStoredProcedure dal = new MappingStoredProcedure();
        IMultipleResults results = dal.SP_ProductColorWithCount("0", "1000", "ID", ddlProductID.SelectedValue);
        lstColors = results.GetResult<ProductColorViewEntity>().ToList();
    }

    private void LoadColorMaterials()
    {
        LoadProductColors();
        //if (lstColors.Count == 0)
        //{
        //    ltrColorMaterialsTitle.Visible = false;
        //    ddlColorMaterials.Visible = false;
        //    ltrColorsTitle.Visible = false;
        //    ddlColors.Visible = false;
        //    ltrColorsTitle2.Visible = false;
        //    rptColors.Visible = false;
        //}

        var lstMaterials = lstColors.GroupBy(color => new { MaterialID = color.MaterialID, MaterialName = color.MaterialName }).Select(m => new MaterialEntity
        {
            ID = m.Key.MaterialID,
            MaterialName = m.Key.MaterialName
        });

        ddlColorMaterials.DataSource = lstMaterials;
        ddlColorMaterials.DataBind();

        if (ddlColorMaterials.Items.Count > 0)
            ddlColorMaterials.SelectedIndex = 0;

        if (CurrentOrderDetail != null)
        {
            if (ddlColorMaterials.Items.FindByValue(CurrentOrderDetail.MaterialID.ToString()) != null)
                ddlColorMaterials.SelectedValue = CurrentOrderDetail.MaterialID.ToString();
        }
    }

    protected void ddlColorMaterials_SelectedIndexChanged(object sender, EventArgs e)
    {
        CurrentOrderDetail = null;

        LoadColorsBySelectedMaterial();
    }

    protected void ddlProductID_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadPrice();

        CurrentOrderDetail = null;

        LoadColorMaterials();
        LoadColorsBySelectedMaterial();
    }
    
    private void LoadPrice()
    {
        ProductManager manager = new ProductManager();
        var product = manager.GetProductById(Convert.ToInt32(ddlProductID.SelectedValue));
        txtPrice.Text = product.Price.ToString();
    }

    private void LoadColorsBySelectedMaterial()
    {
        if (lstColors == null)
            LoadProductColors();

        if (lstColors.Count == 0)
            return;

        int selectedMaterialID = Convert.ToInt32(ddlColorMaterials.SelectedItem.Value);
        var lst = lstColors.Where(color => color.MaterialID == selectedMaterialID).ToList();

        ddlColorID.DataSource = lst;
        ddlColorID.DataBind();

        ddlColorPicture.DataSource = lst.Select(color => new ListItem(UrlHelper.ResolveUrl(ImageHelper.GetProductImageFullPath(color.Picture)), color.ID.ToString()));
        ddlColorPicture.DataBind();

        if (CurrentOrderDetail != null)
        {
            if (ddlColorID.Items.FindByValue(CurrentOrderDetail.ColorID.ToString()) != null)
                ddlColorID.SelectedValue = CurrentOrderDetail.ColorID.ToString();
            if (ddlColorPicture.Items.FindByValue(CurrentOrderDetail.ColorID.ToString()) != null)
                ddlColorPicture.SelectedValue = CurrentOrderDetail.ColorID.ToString();

            // Set Main Image with the first Color image
            imgPicture.ImageUrl = ImageHelper.GetProductImageFullPath(CurrentOrderDetail.Picture);
        }
        else
        {
            // Set Main Image with the first Color image
            imgPicture.ImageUrl = ImageHelper.GetProductImageFullPath(lst[0].Picture);
        }
    }

    protected bool IsAdmin
    {
        get
        {
            return Convert.ToBoolean(ViewState["IsAdmin"]);
        }
        set
        {
            ViewState["IsAdmin"] = value;
        }
    }
}
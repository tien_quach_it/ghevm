﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManageOrderDetails.aspx.cs" Inherits="DesktopModules_ManageOrders_Popups_ManageOrderDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
    <title>Quản Lý Màu Sắc Sản Phẩm</title>
    <link type="text/css" rel="stylesheet" href="<%= ResolveUrl("~/Portals/0/Skins/GheVM/css/admin.css") %>" />
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 class="t_title">Đơn Hàng Số
                <asp:Literal runat="server" ID="ltrOrderID"></asp:Literal></h1>
            <div>
                <asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red" CssClass="error-msg"></asp:Label>
                <%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
                <div>
                    <asp:Label ID="Label1" runat="server" Text="" Font-Bold="true" ForeColor="red"></asp:Label>
                    <asp:MultiView ID="vws" runat="server">
                        <asp:View ID="vwView" runat="server">
                            <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAddNew" Text="Add New" runat="server" CssClass="button" OnClick="lnkAddNew_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                                <tr class="t_admin_table_header">
                                    <th></th>
                                    <th>
                                        <asp:LinkButton ID="btnSortPicture" runat="server" Text="Hình Ảnh" CommandArgument="Picture" OnClick="btnSort_Click"></asp:LinkButton></th>
                                    <th>
                                        <asp:LinkButton ID="btnSortProductCode" runat="server" Text="Mã Sản Phẩm" CommandArgument="ProductCode" OnClick="btnSort_Click"></asp:LinkButton></th>
                                    <th>
                                        <asp:LinkButton ID="btnSortMaterialName" runat="server" Text="Chất Liệu" CommandArgument="MaterialName" OnClick="btnSort_Click"></asp:LinkButton></th>
                                    <th>
                                        <asp:LinkButton ID="btnSortColorName" runat="server" Text="Màu Sắc" CommandArgument="ColorName" OnClick="btnSort_Click"></asp:LinkButton></th>
                                    <th>
                                        <asp:LinkButton ID="btnSortQuantity" runat="server" Text="Số Lượng" CommandArgument="Quantity" OnClick="btnSort_Click"></asp:LinkButton></th>
                                    <th>
                                        <asp:LinkButton ID="btnSortPrice" runat="server" Text="Giá" CommandArgument="Price" OnClick="btnSort_Click"></asp:LinkButton></th>
                                    

                                </tr>
                                <asp:Repeater ID="rpt" runat="server">
                                    <ItemTemplate>
                                        <tr class="row<%# Container.ItemIndex % 2 %>">
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" OnClick="lnkEdit_Click" CommandArgument='<%# Eval("ID") %>' Visible='<%# IsAdmin %>'></asp:LinkButton></th>
                                                <br /><br />
                                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>' Visible='<%# IsAdmin %>'
                                                    Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                                    OnClick="lnkDelete_Click"></asp:LinkButton>
                                            </td>
                                            <td><img width="80px" alt="" src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 220) %>"></td>
                                            <td><%# Eval("ProductCode") %> (<%# Eval("ProductName") %>)</td>
                                            <td><%# Eval("MaterialName") %></td>
                                            <td><%# Eval("ColorName") %></td>
                                            <td><%# Eval("Quantity") %></td>
                                            <td><%# DNNHelper.FormatPrice(Convert.ToDouble(Eval("Price"))) %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr class="row0">
                                    <td colspan="5"></td>
                                    <td><strong>Tổng Cộng:</strong></td>
                                    <td><strong><asp:Label runat="server" ID="lblTotal"></asp:Label></strong></td>
                                </tr>  
                            </table>
                            <div>
                                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
                            </div>
                        </asp:View>
                        <asp:View ID="vwEdit" runat="server">
                            <table cellpadding="2" cellspacing="2" class="t_admintable">
                                <tr class='row0'>
                                    <td>Sản Phẩm *</td>
                                    <td>
                                        <asp:DropDownList ID="ddlProductID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductID_SelectedIndexChanged"></asp:DropDownList></td>
                                </tr>
                                <tr class='row1'>
                                    <td>Chất Liệu *</td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlColorMaterials" DataTextField="MaterialName" DataValueField="ID" AutoPostBack="true" OnSelectedIndexChanged="ddlColorMaterials_SelectedIndexChanged"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class='row0'>
                                    <td>Màu Sắc *</td>
                                    <td>
                                        <asp:DropDownList ID="ddlColorID" runat="server" DataTextField="ColorName" DataValueField="ID"></asp:DropDownList>
                                        <div style="display: none">
                                            <asp:DropDownList ID="ddlColorPicture" runat="server" DataTextField="Text" DataValueField="Value"></asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Image ID="imgPicture" runat="server" Width="200px"/>
                                    </td>
                                </tr>
                                <tr class='row1'>
                                    <td>Giá *</td>
                                    <td>
                                        <asp:TextBox ID="txtPrice" runat="server" Width="400"></asp:TextBox></td>
                                </tr>
                                <tr class='row0'>
                                    <td>Số Lượng *</td>
                                    <td>
                                        <asp:TextBox ID="txtQuantity" runat="server" Width="400"></asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                            <script type="text/javascript">
                                $(function () {
                                    var ddlColorID = "#<%= ddlColorID.ClientID %>";
                                    $(ddlColorID).change(function () {
                                        var $ddl = $(this);
                                        $('#<%= imgPicture.ClientID %>').attr("src", $("#<%= ddlColorPicture.ClientID %> option[value=" + $ddl.val() + "]").text());
                                    });
                                });
                        </script>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

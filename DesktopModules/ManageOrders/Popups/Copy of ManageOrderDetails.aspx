﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Copy of ManageOrderDetails.aspx.cs" Inherits="DesktopModules_ManageOrders_Popups_ManageOrderDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <title>Quản Lý Màu Sắc Sản Phẩm</title>
    <link type="text/css" rel="stylesheet" href="<%= ResolveUrl("~/Portals/0/Skins/GheVM/css/admin.css") %>" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1 class="t_title">Đơn Hàng Số <asp:Literal runat="server" ID="ltrOrderID"></asp:Literal></h1>
        <div>
            <asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red" CssClass="error-msg"></asp:Label>
            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                <tr class="t_admin_table_header">
                    <th>
                        Hình Ảnh</th>
                    <th>Mã Sản Phẩm</th>
                    <th>Màu Sắc</th>
                    <th>
                        Sản Phẩm</th>
                    <th>
                        Số Lượng</th>
                    <th>
                        Giá</th>
                </tr>
                <asp:Repeater ID="rpt" runat="server">
                    <ItemTemplate>
                        <tr class="row<%# Container.ItemIndex % 2 %>">
                            <td><img width="80px" alt="" src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 220) %>"></td>
                            <td><%# Eval("ProductCode") %></td>
                            <td><%# Eval("ColorName") %></td>
                            <td><%# Eval("ProductName") %></td>
                            <td><%# Eval("Quantity") %></td>
                            <td><%# DNNHelper.FormatPrice(Convert.ToDouble(Eval("Price"))) %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>  
                <tr class="row0">
                    <td><strong>Tổng Cộng:</strong></td>
                    <td colspan="4"></td>
                    <td><strong><asp:Label runat="server" ID="lblTotal"></asp:Label></strong></td>
                </tr>  
            </table>
        </div>
    </div>
    </form>
</body>
</html>

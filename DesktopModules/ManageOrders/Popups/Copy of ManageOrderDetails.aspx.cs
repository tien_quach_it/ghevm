﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DesktopModules_ManageOrders_Popups_ManageOrderDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(OrderID))
                return;

            Initialize();
            LoadOrderDetails();
            
        }
    }

    private void Initialize()
    {
        OrderManager manager = new OrderManager();
        OrderEntity order = manager.GetOrderById(Convert.ToInt32(OrderID));
        ltrOrderID.Text = order.ID.ToString();
        lblTotal.Text = DNNHelper.FormatPrice(order.Total);
    }

    private void LoadOrderDetails()
    {
        string sql = String.Format("select * from vw_OrderDetail where OrderID = {0}", OrderID);
        rpt.DataSource = DBHelper.GetTable(sql);
        rpt.DataBind();
        
    }

    private string OrderID
    {
        get
        {
            return Request.QueryString["OrderID"];
        }
    }
}
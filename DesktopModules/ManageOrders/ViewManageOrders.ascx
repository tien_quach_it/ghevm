﻿<%@ Control language="C#" Inherits="Modules.ManageOrders.ViewManageOrders" CodeFile="ViewManageOrders.ascx.cs" AutoEventWireup="true"%>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<div>
	<asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red"></asp:Label>
    <asp:MultiView ID="vws" runat="server">
        <asp:View ID="vwView" runat="server">
            <asp:Panel ID="pnlTopAdmin" runat="server">
                <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                    <tr>
                        <td>
                            <strong>Cho phép Đặt Hàng: </strong> &nbsp; <asp:CheckBox runat="server" ID="chkEnableShoppingCart" AutoPostBack="true" OnCheckedChanged="chkEnableShoppingCart_CheckedChanged" />
                        </td>    
                        <td style="display: none;">
                            <asp:LinkButton ID="lnkAddNew" Text="Thêm Mới" runat="server" CssClass="button" OnClick="lnkAddNew_Click"></asp:LinkButton>
                        </td>      
                        <td>
                            <strong>Hiển thị Giá Liên Hệ: </strong> &nbsp; <asp:CheckBox runat="server" ID="chkNoPrice" AutoPostBack="true" OnCheckedChanged="chkNoPrice_CheckedChanged" />
                        </td>         
                    </tr>
                </table>
                <br />
            </asp:Panel>
            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                <tr class="t_admin_table_header">
                    <th>
                    </th>    
                    <th>
                    </th>    
                    <th>
                    </th>    
                    <th>Mã Số</th>             
                    <th><asp:LinkButton ID="btnSortFullName" runat="server" Text="Họ Tên" CommandArgument="FullName" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <%--<th><asp:LinkButton ID="btnSortAddress" runat="server" Text="Địa Chỉ" CommandArgument="Address" OnClick="btnSort_Click"></asp:LinkButton></th>--%>
                    <th><asp:LinkButton ID="btnSortPhone" runat="server" Text="Điện Thoại" CommandArgument="Phone" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <%--<th><asp:LinkButton ID="btnSortFax" runat="server" Text="Fax" CommandArgument="Fax" OnClick="btnSort_Click"></asp:LinkButton></th>--%>
                    <%--<th><asp:LinkButton ID="btnSortEmail" runat="server" Text="Email" CommandArgument="Email" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th><asp:LinkButton ID="btnSortComment" runat="server" Text="Comment" CommandArgument="Comment" OnClick="btnSort_Click"></asp:LinkButton></th>--%>
                    <th><asp:LinkButton ID="btnSortTotal" runat="server" Text="Tổng Cộng" CommandArgument="Total" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th><asp:LinkButton ID="btnSortOnDate" runat="server" Text="Ngày" CommandArgument="OnDate" OnClick="btnSort_Click"></asp:LinkButton></th>
                    
                </tr>
                <asp:Repeater ID="rpt" runat="server">
                    <ItemTemplate>                        
                        <tr class="row<%# Container.ItemIndex % 2 %>">
                            <td>
                                <asp:LinkButton ID="lnkView" runat="server" Text="Xem" OnClick="lnkEdit_Click" CommandArgument='<%# Eval("ID") %>' Visible='<%# !IsAdmin %>'></asp:LinkButton>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Chỉnh Sửa" OnClick="lnkEdit_Click" CommandArgument='<%# Eval("ID") %>' Visible='<%# IsAdmin %>'></asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>' Visible='<%# IsAdmin %>'
                                    Text="Xóa" OnClientClick="return confirm('Bạn có chắc là muốn xóa dữ liệu này không?')"
                                    OnClick="lnkDelete_Click"></asp:LinkButton>
                            </td>
                            <td>
                                <a href="javascript:void(0)" onclick='dnnModal.show("<%= ResolveUrl("~/DesktopModules/ManageOrders/Popups/ManageOrderDetails.aspx?OrderID=") %><%# Eval("ID") %>&popUp=true",/*showReturn*/true,550,950,false,"")'>Chi Tiết</a>
                            </td>
                            <td><%# Eval("ID") %></td>
                            <td><%# Eval("FullName") %></td>
                            <%--<td><%# Eval("Address") %></td>--%>
                            <td><%# Eval("Phone") %></td>
                            <%--<td><%# Eval("Fax") %></td>--%>
                            <%--<td><%# Eval("Email") %></td>
                            <td><%# Eval("Comment") %></td>--%>
                            <td><%# DNNHelper.FormatPrice(Convert.ToDouble(Eval("Total")))  %></td>
                            <td><%# DNNHelper.FormatDate(Eval("OnDate")) %></td>
                            
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>      
            <div>
                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
            </div>      
        </asp:View>
        <asp:View ID="vwEdit" runat="server">
            <table cellpadding="2" cellspacing="2" class="t_admintable">
                <tr class='row0'><td>Họ Tên *</td><td><asp:TextBox ID="txtFullName" runat="server" Width="400"></asp:TextBox></td></tr>
                <tr class='row1'><td>Địa Chỉ *</td><td><asp:TextBox ID="txtAddress" runat="server" Width="400"></asp:TextBox></td></tr>
                <tr class='row0'><td>Điện Thoại *</td><td><asp:TextBox ID="txtPhone" runat="server" Width="400"></asp:TextBox></td></tr>
                <%--<tr class='row1'><td>Fax *</td><td><asp:TextBox ID="txtFax" runat="server" Width="400"></asp:TextBox></td></tr>--%>
                <tr class='row1'><td>Email *</td><td><asp:TextBox ID="txtEmail" runat="server" Width="400"></asp:TextBox></td></tr>
                <tr class='row0'><td>Ghi Chú *</td><td><asp:TextBox ID="txtComment" runat="server" Width="400" TextMode="MultiLine" Rows="10"></asp:TextBox></td></tr>
                <tr class='row1'><td>Trạng Thái *</td><td><asp:DropDownList ID="ddlStatusID" runat="server"></asp:DropDownList></td></tr>
                <tr class='row0'><td>Tổng Cộng *</td><td><asp:TextBox ID="txtTotal" runat="server" Width="400"></asp:TextBox></td></tr>
                <tr class='row1'><td>Ngày *</td><td><asp:TextBox ID="txtOnDate" runat="server" Width="80"></asp:TextBox></td></tr>
                
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Lưu Lại" CssClass="button" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Hủy" CssClass="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>
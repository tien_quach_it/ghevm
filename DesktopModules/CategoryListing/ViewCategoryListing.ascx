﻿<%@ Control language="C#" Inherits="Philip.Modules.CategoryListing.ViewCategoryListing" CodeFile="ViewCategoryListing.ascx.cs" AutoEventWireup="true"%>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging2.ascx" %>

<div class="sub_prodContent">
    <div class="prod_container">
        <div class="breadcrumbs">
            <asp:Repeater id="rptBreadcrumbs" runat="server">
                <ItemTemplate>
                    <a href='<%# Eval("Value") %>'><%# Eval("Text") %></a>
                </ItemTemplate>
                <SeparatorTemplate> &gt; </SeparatorTemplate>
            </asp:Repeater>
        </div>
        <asp:Label ID="lblErr" runat="server"></asp:Label>

        <asp:Repeater ID="rptFeaturedCategories" runat="server" OnItemDataBound="rptFeaturedCategories_ItemDataBound">
            <ItemTemplate>
                <div class="text_title clr">
                    <b> <%# Eval("CategoryName") %> </b>
                    <asp:Literal ID="ltrID" runat="server" Text='<%# Eval("ID") %>' Visible="False"></asp:Literal>
                </div>
                <ul>
                    <asp:Repeater ID="rptProducts" runat="server">
                        <ItemTemplate>
                             <li> <%--<%# Container.ItemIndex % 3 == 1 ? "product-margin" : "" %>"--%>
                                <div class="prod-title-bar"> <p> <%# Eval("ProductName") %> </p> </div>
                                <div class="prod_img">
                                    <div class="prod_img_inner">
                                        <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><img  alt='<%# DNNHelper.GetSeoAltText(Eval("ProductCode")) %>' src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 220) %>" border="0" /></a>
                                    </div>
                                    <div class="prod_id"> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><%# Eval("ProductCode") %></a> </div>
                                    <div class="btn_detail">
                                	    <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>">Chi Tiết</a>
                                    </div>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>      
            </ItemTemplate>
        </asp:Repeater> 

        <ul>    
            <asp:Repeater id="rpt" runat="server">
                <ItemTemplate>
                    <li>
                        <div class="prod-title-bar"> <p> <%# Eval("ProductName") %> </p> </div>
                        <div class="prod_img">
                            <div class="prod_img_inner">
                                <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><img alt='<%# DNNHelper.GetSeoAltText(Eval("ProductCode")) %>' src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture"), 220) %>" border="0" /></a></div>
                            <div class="prod_id"> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><%# Eval("ProductCode") %></a></div>
                            <div class="btn_detail">
                                <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"> Chi Tiết </a>
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
            
           <%-- <li>
                <div class="prod-title-bar"> <p> Ghế Lưới Cao Cấp </p> </div>
                <div class="prod_img">
                    <img alt="prod" src="images/product/vm_438.jpg">
                    <div class="prod_id"> VM 437</div>
                    <div class="btn_detail">
                        <a href="#"> Chi Tiết </a>
                    </div>
                </div>
            </li>--%>

        </ul>
    </div><!------ End sub_prodContent ------>
    
    <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />

    <div style="clear:both;"></div>
    
    <script type="text/javascript">
        $(function () {
            var ImageHeight = 219;
            
            // For cache images like on IE
            if (isIE()) {
                $(".prod_img_inner img").each(function() {
                    var $img = $(this);
                    if ($img.width() > $img.height()) {
                        $img.closest("div").css("position", "relative");
                        $img.parent().css("position", "absolute");
                        $img.parent().css("left", "0px");
                        $img.parent().css("top", ((ImageHeight - $img.height()) / 2) + "px");
                    }
                });
            }

            // For new loaded images
            $(".prod_img_inner a img").load(function () {
                var $img = $(this);
                if ($img.width() > $img.height()) {
                    $img.closest("div").css("position", "relative");
                    $img.parent().css("position", "absolute");
                    $img.parent().css("left", "0px");
                    $img.parent().css("top", ((ImageHeight - $img.height()) / 2) + "px");
                }
            });
        });
    </script>
</div>

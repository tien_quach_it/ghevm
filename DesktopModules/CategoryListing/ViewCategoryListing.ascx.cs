﻿// 
// DotNetNuke® - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Linq;

namespace Philip.Modules.CategoryListing
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewCategoryListing class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewCategoryListing : PortalModuleBase, IActionable
    {

        #region Private Members

        private List<ProductViewEntity> lstNewAndFeatured;


        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (String.IsNullOrEmpty(CategoryID))
                    {
                        LoadFeaturedCategories();
                        return;
                    }

                    Initialize();
                    BindGrid();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        #region Tien.Quach Added

        private void Initialize()
        {
            CategoryManager manager = new CategoryManager();
            var category = manager.GetCategoryById(Convert.ToInt32(CategoryID));
            //lblCategoryName.Text = category.CategoryName;

            LoadBreadcrumbs(category);

            // SEO
            var page = ((DotNetNuke.Framework.CDefault)this.Page);
            if (!String.IsNullOrEmpty(category.SeoTitle))
                page.Title = category.SeoTitle;
            else
                page.Title = GetSeoTitle(category);
            if (!String.IsNullOrEmpty(category.SeoKeywords))
                page.KeyWords = category.SeoKeywords;
            if (!String.IsNullOrEmpty(category.SeoDescription))
                page.Description = category.SeoDescription;
        }

        private void LoadBreadcrumbs(CategoryEntity category)
        {
            TabController controller = new TabController();

            // Home Tab
            var homeTab = controller.GetTab(PortalSettings.HomeTabId, PortalId, false);
            ArrayList arr = new ArrayList();
            //arr.Add(new ListItem(homeTab.Title, homeTab.FullUrl));
            arr.Add(new ListItem(homeTab.Title, DNNHelper.GetHomePageUrl()));

            if (category.ParentID == 0)
                arr.Add(new ListItem(category.CategoryName, category.SeoUrl));
            else
            {
                // Parent Category
                var manager = new CategoryManager();
                var parentCategory = manager.GetCategoryById(category.ParentID);
                arr.Add(new ListItem(parentCategory.CategoryName, parentCategory.SeoUrl));

                // Current Category
                arr.Add(new ListItem(category.CategoryName, category.SeoUrl));
            }

            rptBreadcrumbs.DataSource = arr;
            rptBreadcrumbs.DataBind();
        }

        private string GetSeoTitle(CategoryEntity category)
        {
            var manager = new CategoryManager();
            if (category.ParentID == 0)
            {
                return String.Format("{0} | Ghế Văn Phòng Văn Minh", category.CategoryName);
            }
            var parentCategory = manager.GetCategoryById(category.ParentID);
            return String.Format("{0} | {1} | Ghế Văn Phòng Văn Minh", category.CategoryName, parentCategory.CategoryName);
        }

        public void BindGrid()
        {
            try
            {
                var lst = LoadData();
                rpt.DataSource = lst;
                rpt.DataBind();
            }
            catch (Exception ex)
            {
                lblErr.Text = ex.Message;
            }
        }

        private List<ProductViewEntity> LoadData()
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            IMultipleResults results = dal.SP_ProductWithCount(CurrentPage.ToString(), PageSize.ToString(), "DisplayOrder", CategoryID);

            List<ProductViewEntity> lst = results.GetResult<ProductViewEntity>().ToList();
            int NumRows = results.GetResult<int>().Single();

            // Paging

            int totalpage = NumRows / PageSize;
            if (NumRows % PageSize > 0)
                totalpage = totalpage + 1;
            iexPaging.TotalPages = totalpage;
            iexPaging.Visible = totalpage > 1;

            return lst;
        }

        private int PageSize
        {
            get
            {
                return 18;
            }
        }

        private string CategoryID
        {
            get
            {
                return Request.QueryString["ID"];
            }
        }

        protected void iexPaging_PageChanged(object sender, EventArgs e)
        {
            CurrentPage = iexPaging.CurrentPageIndex - 1;

            BindGrid();
        }

        protected int CurrentPage
        {
            get
            {
                if (ViewState["CurPage"] == null)
                    ViewState["CurPage"] = 0;
                return Convert.ToInt32(ViewState["CurPage"]);
            }
            set
            {
                ViewState["CurPage"] = value;
            }
        }

        #endregion

        #region Featured Products

        private void LoadFeaturedCategories()
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            lstNewAndFeatured = dal.SP_FeaturedProducts().ToList(); 

            var lstFeaturedCategory = dal.SP_FeaturedCategories().OrderBy(c => c.DisplayOrder);
            rptFeaturedCategories.DataSource = lstFeaturedCategory.ToList();
            rptFeaturedCategories.DataBind();
        }

        private Repeater rptProducts;
        protected void rptFeaturedCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                rptProducts = e.Item.FindControl("rptProducts") as Repeater;
                int categoryID = Convert.ToInt32((e.Item.FindControl("ltrID") as Literal).Text);
                rptProducts.DataSource = lstNewAndFeatured.Where(p => p.IsFeatured && (p.CategoryID == categoryID || p.ParentCategoryID == categoryID)).OrderBy(p => p.DisplayOrder);
                rptProducts.DataBind();
            }
        }

        #endregion
    }
}


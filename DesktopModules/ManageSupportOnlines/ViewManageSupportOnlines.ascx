﻿<%@ Control Language="C#" Inherits="Philip.Modules.ManageSupportOnlines.ViewManageSupportOnlines"
    CodeFile="ViewManageSupportOnlines.ascx.cs" AutoEventWireup="true" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<div>
    <asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red"></asp:Label>
    <asp:MultiView ID="vws" runat="server">
        <asp:View ID="vwView" runat="server">
            <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkAddNew" Text="Thêm Mới" runat="server" CssClass="button"
                            OnClick="lnkAddNew_Click"></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                <tr class="t_admin_table_header">
                    <th>
                    </th>
                    <th>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSortDisplayOrder" runat="server" Text="STT" CommandArgument="DisplayOrder"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSortYahooID" runat="server" Text="Yahoo ID" CommandArgument="YahooID"
                            OnClick="btnSort_Click"></asp:LinkButton>
                    </th>                    
                    <th>
                        Loại
                    </th>
                </tr>
                <asp:Repeater ID="rpt" runat="server">
                    <ItemTemplate>
                        <tr class="row<%# Container.ItemIndex % 2 %>">
                            <td>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Chỉnh Sửa" OnClick="lnkEdit_Click"
                                    CommandArgument='<%# Eval("ID") %>'></asp:LinkButton></th>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>'
                                    Text="Xóa" OnClientClick="return confirm('Bạn có muốn xóa dữ liệu này không?')"
                                    OnClick="lnkDelete_Click"></asp:LinkButton>
                            </td>
                            <td>
                                <%# Eval("DisplayOrder") %>
                            </td>
                            <td>
                                <%# Eval("YahooID") %>
                            </td>   
                            <td>
                                <%# OnlineSupportManager.GetCategories().Single(i => i.Value.Equals(Eval("CategoryID").ToString())).Text %>
                            </td>                         
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <div>
                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
            </div>
        </asp:View>
        <asp:View ID="vwEdit" runat="server">
            <table cellpadding="2" cellspacing="2" class="t_admintable">
                <tr class='row1'>
                    <td>
                        STT *
                    </td>
                    <td>
                        <asp:TextBox ID="txtDisplayOrder" runat="server" Width="400"></asp:TextBox>
                    </td>
                </tr>
                <tr class='row0'>
                    <td>
                        Loại *
                    </td>
                    <td>
                        <asp:DropDownList id="ddlCategory" runat="server"></asp:DropDownList>
                </tr>
                <tr class='row1'>
                    <td>
                        Yahoo ID *
                    </td>
                    <td>
                        <asp:TextBox ID="txtYahooID" runat="server" Width="400"></asp:TextBox><br />
                        <span style="font-size: 11px">Yahoo ID không cần phải nhập phần đuôi yahoo.com, yahoo.com.vn...</span>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Lưu Lại" CssClass="button" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Hủy" CssClass="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>

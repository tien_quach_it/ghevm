<%@ Control language="C#" Inherits="Philip.Modules.ManagePageSeoInfo.ViewManagePageSeoInfo" CodeFile="ViewManagePageSeoInfo.ascx.cs" AutoEventWireup="true"%>
<table>
    <tr>
        <td><b>Page</b></td>
        <td><b>Page SEO Title</b></td>
    </tr>
    <asp:Repeater ID="rptTabs" runat="server">
        <ItemTemplate>
            <tr>
                <td>
                    <%# Eval("Title") %>
                    <asp:Literal ID="ltrTabID" runat="server" Text='<%# Eval("TabID") %>' Visible="False"></asp:Literal>
                </td>
                <td><asp:TextBox runat="server" ID="txtTitle" Width="500px" TextMode="MultiLine" Rows="2" Text='<%# GetTabTitle(Eval("TabID")) %>'></asp:TextBox></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    <tr>
        <td></td>
        <td><asp:LinkButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"></asp:LinkButton></td>
    </tr>
</table>
﻿<%@ Control language="C#" Inherits="Modules.ManageCategories.ViewManageCategories" CodeFile="ViewManageCategories.ascx.cs" AutoEventWireup="true"%>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<div>
    <asp:Label ID="lblErr" runat="server" Text="" Font-Bold="true" ForeColor="red"></asp:Label>
    <asp:MultiView ID="vws" runat="server">
        <asp:View ID="vwView" runat="server">
            <table width="100%" cellpadding="2" cellspacing="2" class="t_content">
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkAddNew" Text="Thêm Mới" runat="server" CssClass="button" OnClick="lnkAddNew_Click"></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" cellpadding="0" cellspacing="0" class="t_admintable">
                <tr class="t_admin_table_header">
                    <th></th>
                    <th></th>
                    <th>
                        <asp:LinkButton ID="btnSortDisplayOrder" runat="server" Text="STT" CommandArgument="DisplayOrder" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th>
                        <asp:LinkButton ID="btnSortCategoryName" runat="server" Text="Danh Mục" CommandArgument="CategoryName" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th>
                        <asp:LinkButton ID="btnSortParentID" runat="server" Text="Danh Mục Cha" CommandArgument="ParentID" OnClick="btnSort_Click"></asp:LinkButton></th>
                    <th>
                        <asp:LinkButton ID="btnSortIsFeatured" runat="server" Text="Hiển Thị Ở Trang Chủ" CommandArgument="IsFeatured" OnClick="btnSort_Click"></asp:LinkButton></th>

                </tr>
                <asp:Repeater ID="rpt" runat="server">
                    <ItemTemplate>
                        <tr class="row<%# Container.ItemIndex % 2 %>">
                            <td>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Chỉnh Sửa" OnClick="lnkEdit_Click" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton></th>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ID") %>'
                                    Text="Xóa" OnClientClick="return confirm('Bạn có chắc là muốn xóa dữ liệu này không?')"
                                    OnClick="lnkDelete_Click"></asp:LinkButton>
                            </td>
                            <td><%# Eval("DisplayOrder") %></td>
                            <td><%# Eval("CategoryName") %></td>
                            <td><%# Eval("ParentCategoryName") %></td>
                            <td><%# Convert.ToBoolean(Eval("IsFeatured")) ? "Có" : "Không" %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <div>
                <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
            </div>
        </asp:View>
        <asp:View ID="vwEdit" runat="server">
            <table cellpadding="2" cellspacing="2" class="t_admintable">
                <tr class='row0'>
                    <td>Danh Mục *</td>
                    <td>
                        <asp:TextBox ID="txtCategoryName" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr class='row1'>
                    <td>Danh Mục Cha</td>
                    <td>
                        <asp:DropDownList ID="ddlParentID" runat="server"></asp:DropDownList></td>
                </tr>
                <tr class='row0'>
                    <td>STT *</td>
                    <td>
                        <asp:TextBox ID="txtDisplayOrder" runat="server" Width="80"></asp:TextBox></td>
                </tr>
                <tr class='row1'>
                    <td>Hiển Thị Ở Trang Chủ</td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkIsFeatured" /></td>
                </tr>
                <tr class='row0'>
                    <td>SEO Url</td>
                    <td>
                        <asp:TextBox ID="txtSeoUrl" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr class='row1'>
                    <td>SEO Title</td>
                    <td>
                        <asp:TextBox ID="txtSeoTitle" runat="server" Width="400"></asp:TextBox></td>
                </tr>
                <tr class='row0'>
                    <td>SEO Keywords</td>
                    <td>
                        <asp:TextBox ID="txtSeoKeywords" runat="server" Width="400" TextMode="MultiLine" Rows="3"></asp:TextBox></td>
                </tr>
                <tr class='row1'>
                    <td>SEO Description</td>
                    <td>
                        <asp:TextBox ID="txtSeoDescription" runat="server" Width="400" TextMode="MultiLine" Rows="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Lưu Lại" CssClass="button" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Hủy" CssClass="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
            <script type="text/javascript">
                var txtCategoryNameID = '#<%= txtCategoryName.ClientID %>';
                
                $(function() {
                    $(txtCategoryNameID).keyup(function (e) {
                        populateSeoUrl();
                    });
                    
                    $(txtCategoryNameID).blur(function (e) {
                        populateSeoUrl();
                    });
                    
                    function populateSeoUrl() {
                        var url = "";
                        
                        var parentCategory = $("#<%= ddlParentID.ClientID %> option:selected").text();
                        if (parentCategory != "")
                            url += parentCategory + "/";

                        url = "/" + convertToUnsign(url + $(txtCategoryNameID).val()) + '.aspx';

                        $('#<%= txtSeoUrl.ClientID %>').val(url);
                    }
                });
            </script>
        </asp:View>
    </asp:MultiView>
</div>

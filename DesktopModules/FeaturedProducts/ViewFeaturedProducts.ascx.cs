// 
// DotNetNukeŽ - http://www.dotnetnuke.com
// Copyright (c) 2002-2011
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data.Linq;

namespace Modules.FeaturedProducts
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewFeaturedProducts class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewFeaturedProducts : PortalModuleBase, IActionable
    {

        #region Private Members

        private List<ProductViewEntity> lstNewAndFeatured;

        #endregion

        #region Public Methods

        public bool DisplayAudit()
        {
            bool retValue = false;

            if ((string)Settings["auditinfo"] == "Y")
            {
                retValue = true;
            }

            return retValue;
        }

        #endregion

        #region Event Handlers

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void Page_Load(System.Object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    LoadData();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        #endregion

        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Registers the module actions required for interfacing with the portal framework
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <history>
        /// </history>
        /// -----------------------------------------------------------------------------
        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(this.GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile), ModuleActionType.AddContent, "", "", this.EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

        private void LoadData()
        {
            MappingStoredProcedure dal = new MappingStoredProcedure();
            lstNewAndFeatured = dal.SP_FeaturedProducts().ToList(); 

            // New Products
            rptNewProducts.DataSource = lstNewAndFeatured.Where(p => p.IsNew).OrderBy(p => p.DisplayOrder);
            rptNewProducts.DataBind();

            // Featured Products
            //var lstFeaturedCategory = lstNewAndFeatured.Select(p => new CategoryEntity
            //{
            //    ID = p.CategoryID,
            //    CategoryName = p.CategoryName
            //}).Distinct();
            var lstFeaturedCategory = dal.SP_FeaturedCategories().OrderBy(c => c.DisplayOrder);
            rptFeaturedCategories.DataSource = lstFeaturedCategory.ToList();
            rptFeaturedCategories.DataBind();
        }

        private Repeater rptProducts;
        protected void rptFeaturedCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                rptProducts = e.Item.FindControl("rptProducts") as Repeater;
                int categoryID = Convert.ToInt32((e.Item.FindControl("ltrID") as Literal).Text);
                rptProducts.DataSource = lstNewAndFeatured.Where(p => p.IsFeatured && (p.CategoryID == categoryID || p.ParentCategoryID == categoryID )).OrderBy(p => p.DisplayOrder);
                rptProducts.DataBind();
            }
        }
    }
}


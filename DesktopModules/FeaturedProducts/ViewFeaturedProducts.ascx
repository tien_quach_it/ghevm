﻿<%@ Control language="C#" Inherits="Modules.FeaturedProducts.ViewFeaturedProducts" CodeFile="ViewFeaturedProducts.ascx.cs" AutoEventWireup="true"%>
<div class="hidden-xs">
<div class="product_show">
    <div class="title-bar"> 
        <p> Sản Phẩm Mới</p> 
    </div>
    <div class="product_container">
        <asp:Repeater id="rptNewProducts" runat="server">
            <ItemTemplate>
                <div class="product <%# Container.ItemIndex % 3 == 1 ? "product-margin" : "" %>">
                    <div class="product_image_inner">
                        <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><img src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture")) %>" border="0" alt='<%# DNNHelper.GetSeoAltText(Eval("ProductCode")) %>'></a>
                    </div>
                    <div class="product_info">
                        <div class="product_code">
                            <p> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><%# Eval("ProductCode") %></a> </p>
                        </div>
                        <div class="detail">
                            <p> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>">Chi Tiết</a> </p>
                        </div>
                    </div> 
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div style="clear:left;"></div>
    </div>
</div>

<asp:Repeater ID="rptFeaturedCategories" runat="server" OnItemDataBound="rptFeaturedCategories_ItemDataBound">
    <ItemTemplate>
        <div class="product_show">
            <div class="title-bar"> 
                <p> <%# Eval("CategoryName") %> </p> 
                <asp:Literal ID="ltrID" runat="server" Text='<%# Eval("ID") %>' Visible="False"></asp:Literal>
            </div>
            <div class="product_container">
                <asp:Repeater ID="rptProducts" runat="server">
                    <ItemTemplate>
                        <div class="product <%# Container.ItemIndex % 3 == 1 ? "product-margin" : "" %>">
                            <div class="product_image_inner">
                                <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><img src="<%# UrlHelper.GetProductThumbUrl(Eval("Picture")) %>" border="0" alt='<%# DNNHelper.GetSeoAltText(Eval("ProductCode")) %>'></a>
                            </div> 
                            <div class="product_info">
                                <div class="product_code">
                                    <p> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>"><%# Eval("ProductCode") %></a> </p>
                                </div>
                                <div class="detail">
                                    <p> <a href="<%# UrlHelper.GetProductDetailsUrl(Eval("ID"), Eval("SeoUrl")) %>">Chi Tiết</a> </p>
                                </div>
                            </div> 
                        </div> 
                    </ItemTemplate>
                </asp:Repeater>
                <div style="clear:left;"></div>
            </div>
        </div>
        
    </ItemTemplate>
</asp:Repeater> 
</div>
<script type="text/javascript">
    $(function () {
        var ImageWidth = 158;
        var ImageHeight = 139;

        // For cache images like on IE
        if (isIE()) {
            $(".product_image_inner img").each(function() {
                var $img = $(this);
                if ($img.width() > $img.height()) {
                    $img.closest("div").css("position", "relative");
                    $img.parent().css("position", "absolute");
                    $img.parent().css("left", ((ImageWidth - $img.width()) / 2) + "px");
                    $img.parent().css("top", ((ImageHeight - $img.height()) / 2) + "px");
                }
            });
        }

        // For new loaded images
        $(".product_image_inner a img").load(function () {
            var $img = $(this);
            if ($img.width() > $img.height()) {
                $img.closest("div").css("position", "relative");
                $img.parent().css("position", "absolute");
                $img.parent().css("left", ((ImageWidth - $img.width()) / 2) + "px");
                $img.parent().css("top", ((ImageHeight - $img.height()) / 2) + "px");
            }
        });
    });
    </script>
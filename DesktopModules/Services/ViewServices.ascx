﻿<%@ Control Language="C#" Inherits="Philip.Modules.Services.ViewServices" CodeFile="ViewServices.ascx.cs"
    AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Audit" Src="~/controls/ModuleAuditControl.ascx" %>
<%@ Register TagPrefix="iex" TagName="Paging" Src="~/Portals/0/UserControls/Paging.ascx" %>
<div class="services">
    <asp:MultiView ID="vws" runat="server">
        <asp:View ID="vwView" runat="server">
            <asp:Repeater ID="rptNews" runat="server">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <div class="services_container">
                            <div class="cover">
                                <img alt="cover" src='<%# ResolveUrl(ImageHelper.GetServiceImageFullPath(Eval("Picture").ToString())) %>'
                                    alt="" class="services" />
                            </div>
                            <div class="summary">
                                <h1>
                                    <a href="/DichVu/ChiTietDichVu.aspx?id=<%# Eval("ID") %>">
                                        <%# Eval("Title") %></a>
                                </h1>
                                <p>
                                    <%# Eval("Summary") %>
                                </p>
                            </div>
                            <div class="clear">
                            </div>
                            <%--<center>
                                <a class="show service" href="/DichVu/ChiTietDichVu.aspx?id=<%# Eval("ID") %>">Chi Tiết
                                </a>
                            </center>--%>
                        </div>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
            <iex:Paging ID="iexPaging" runat="server" OnPageChanged="iexPaging_PageChanged" />
        </asp:View>
        <asp:View ID="vwEdit" runat="server">
            <ul>
                <li>
                    <div class="services_container">
                        <div class="cover">
                            <asp:Image ID="imgService" runat="server" />
                        </div>
                        <div class="summary">
                            <h1>
                                <asp:Label ID="lblTitle" runat="server" CssClass="servicesTitle"></asp:Label>
                            </h1>
                            <p>
                                <asp:Label ID="lblSummary" runat="server" ></asp:Label>
                            </p>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="show_more">
                            <asp:Label ID="lblDetail" runat="server"></asp:Label>
                        </div>
                        <br />
                        <a href="/DichVu.aspx" class="newsLink"><< Quay Lại</a>
                    </div>
                </li>
            </ul>
        </asp:View>
    </asp:MultiView>
</div>
<!---- end n-eContent ---->
<div class="clear">
</div>

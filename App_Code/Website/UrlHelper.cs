﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for UrlHelper
/// </summary>
public class UrlHelper
{
    private const string ProductUrl = "~/SanPham/ChiTiet.aspx";
    private const string CategoryUrl = "~/SanPham.aspx";

    public static string GetProductDetailsUrl(object productID, object seoUrl)
    {
        if (String.IsNullOrEmpty((string)seoUrl))
            return ResolveUrl(String.Format("{0}?ID={1}", ProductUrl, productID));

        return seoUrl.ToString();
    }

    //public static string GetCategoryListingsUrl(object categoryID)
    //{
    //    return GetCategoryListingsUrl(categoryID, null);
    //}

    public static string GetCategoryListingsUrl(object categoryID, object seoUrl)
    {
        if (String.IsNullOrEmpty((string)seoUrl))
            return ResolveUrl(String.Format("{0}?ID={1}", CategoryUrl, categoryID));

        return seoUrl.ToString();
    }

    public static string ResolveUrl(string url)
    {
        return (HttpContext.Current.Handler as Page).ResolveUrl(url);
    }

    public static string GetProductThumbUrl(object picture)
    {
        return GetProductThumbUrl(picture, 150);
    }

    public static string GetProductThumbUrl(object picture, int width)
    {
        if (picture == null)
            return "";
        return ResolveUrl(String.Format("~/GetImage.ashx?image={0}&width={1}", picture, width));
    }

    public static string EnsureHttp(object url)
    {
        var strUrl = (string) url;
        if (String.IsNullOrEmpty(strUrl))
            return "#";

        if (strUrl.StartsWith("http://") || strUrl.StartsWith("//"))
            return strUrl;

        return String.Format("http://{0}", strUrl);
    }
}
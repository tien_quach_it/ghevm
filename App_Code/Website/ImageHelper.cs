﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ImageHelper
/// </summary>
public class ImageHelper
{
    private const string PRODUCT_IMAGE_PATH = "~/Portals/{0}/Products/";
    private const string PRODUCT_IMAGE_FULL_PATH = "~/Portals/{0}/Products/{1}";
    private const string HOME_BANNER_PATH = "~/Portals/{0}/HomeBanners/";
    private const string HOME_BANNER_FULL_PATH = "~/Portals/{0}/HomeBanners/{1}";
    private const string NEWS_PATH = "~/Portals/{0}/News/";
    private const string NEWS_FULL_PATH = "~/Portals/{0}/News/{1}";
    private const string SERVICE_PATH = "~/Portals/{0}/Services/";
    private const string SERVICE_FULL_PATH = "~/Portals/{0}/Services/{1}";
    private const string PARTNER_IMAGE_PATH = "~/Portals/{0}/Partners/";
    private const string PARTNER_IMAGE_FULL_PATH = "~/Portals/{0}/Partners/{1}";
    private const string CATALOGUE_IMAGE_PATH = "~/Portals/{0}/Catalogue/";
    private const string CATALOGUE_IMAGE_FULL_PATH = "~/Portals/{0}/Catalogue/{1}";
    private const string CATALOGUE_DOWNLOAD_PATH = "~/Portals/{0}/Catalogue/Download/";
    private const string CATALOGUE_DOWNLOAD_FULL_PATH = "~/Portals/{0}/Catalogue/Download/{1}";

    public static string GetProductImagePath()
    {
        return String.Format(PRODUCT_IMAGE_PATH, DNNHelper.CurrentPortalID);
    }

    public static string GetProductImageFullPath(object imageName)
    {
        if (imageName == null)
            return "";

        return String.Format(PRODUCT_IMAGE_FULL_PATH, DNNHelper.CurrentPortalID, imageName);
    }

    public static string GetHomeBannerImagePath()
    {
        return String.Format(HOME_BANNER_PATH, DNNHelper.CurrentPortalID);
    }
    public static string GetHomeBannerImageFullPath(string imageName)
    {
        return String.Format(HOME_BANNER_FULL_PATH, DNNHelper.CurrentPortalID, imageName);
    }

    public static string GetNewsImagePath()
    {
        return String.Format(NEWS_PATH, DNNHelper.CurrentPortalID);
    }
    public static string GetNewsImageFullPath(string imageName)
    {
        return String.Format(NEWS_FULL_PATH, DNNHelper.CurrentPortalID, imageName);
    }

    public static string GetServiceImagePath()
    {
        return String.Format(SERVICE_PATH, DNNHelper.CurrentPortalID);
    }
    public static string GetServiceImageFullPath(string imageName)
    {
        return String.Format(SERVICE_FULL_PATH, DNNHelper.CurrentPortalID, imageName);
    }

    public static string GetPartnerImagePath()
    {
        return String.Format(PARTNER_IMAGE_PATH, DNNHelper.CurrentPortalID);
    }

    public static string GetPartnerImageFullPath(object imageName)
    {
        if (imageName == null)
            return "";

        return String.Format(PARTNER_IMAGE_FULL_PATH, DNNHelper.CurrentPortalID, imageName);
    }

    public static string GetCatalogueImagePath()
    {
        return String.Format(CATALOGUE_IMAGE_PATH, DNNHelper.CurrentPortalID);
    }

    public static string GetCatalogueImageFullPath(object imageName)
    {
        if (imageName == null)
            return "";

        return String.Format(CATALOGUE_IMAGE_FULL_PATH, DNNHelper.CurrentPortalID, imageName);
    }

    public static string GetCatalogueDownloadPath()
    {
        return String.Format(CATALOGUE_DOWNLOAD_PATH, DNNHelper.CurrentPortalID);
    }

    public static string GetCatalogueDownloadFullPath(object fileName)
    {
        if (fileName == null)
            return "";

        return String.Format(CATALOGUE_DOWNLOAD_FULL_PATH, DNNHelper.CurrentPortalID, fileName);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

public partial class ShopViewEntity
{
	public Int32 ID { get; set; }

	public Int32 ProvinceID { get; set; }

	public Int32 RegionID { get; set; }

	public String Name { get; set; }

	public String Address { get; set; }

	public String Longitude { get; set; }

	public String Latitude { get; set; }

    public String ForceLongitude { get; set; }

    public String ForceLatitude { get; set; }

	public String Phone { get; set; }

	public Int32 DisplayOrder { get; set; }

	public Boolean Deleted { get; set; }

    public String Email { get; set; }

    public String ProvinceName { get; set; }

	public String RegionName { get; set; }

	public Int64 RowNumber { get; set; }

}

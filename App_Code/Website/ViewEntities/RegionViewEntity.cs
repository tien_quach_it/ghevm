using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

public partial class RegionViewEntity
{
	public Int32 ID { get; set; }

	public Int32 ProvinceID { get; set; }

	public String RegionName { get; set; }

	public Int32 DisplayOrder { get; set; }

	public Boolean Deleted { get; set; }

	public String ProvinceName { get; set; }

	public Int64 RowNumber { get; set; }

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

public partial class ProductColorViewEntity
{
	#region Properties Mapping

	public int ID { get; set; }

	public int ProductID { get; set; }

    public int MaterialID { get; set; }

	public string ColorName { get; set; }

	public string Picture { get; set; }

	public bool Deleted { get; set; }

    public string MaterialName { get; set; }

	#endregion
}

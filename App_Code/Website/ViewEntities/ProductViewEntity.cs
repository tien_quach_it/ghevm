using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

public partial class ProductViewEntity
{
	#region Properties Mapping
	
	public int ID { get; set; }

	public string ProductName { get; set; }

	public string ProductCode { get; set; }

	public int CategoryID { get; set; }

    public string Picture { get; set; }

	public string SpecsPicture { get; set; }

	public string Description { get; set; }

	public string Materials { get; set; }

    public string Colors { get; set; }

	public string Warranty { get; set; }

	public int DisplayOrder { get; set; }

    public double Price { get; set; }

    public bool IsFeatured { get; set; }

    public bool IsBestSell { get; set; }

    public bool IsNew { get; set; }

	public bool Deleted { get; set; }

    public string CategoryName { get; set; }

    public int ParentCategoryID { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoUrl { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoTitle { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoKeywords { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoDescription { get; set; }

    public string CategorySeoUrl { get; set; }

    #endregion
}

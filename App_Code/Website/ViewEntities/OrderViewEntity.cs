using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

public partial class OrderViewEntity
{
	#region Properties Mapping

	public int ID { get; set; }

	public string FullName { get; set; }

	public string Address { get; set; }

	public string Phone { get; set; }

	public string Fax { get; set; }

	public string Email { get; set; }

	public string Comment { get; set; }

	public int StatusID { get; set; }

	public double Total { get; set; }

	public DateTime OnDate { get; set; }

	public bool Deleted { get; set; }

    public string StatusName { get; set; }

	#endregion
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

public partial class NewsViewEntity
{
	public Int32 ID { get; set; }

	public String Title { get; set; }

	public String Picture { get; set; }

	public String Summary { get; set; }

	public String Description { get; set; }

	public Int32 DisplayOrder { get; set; }

	public DateTime DateCreated { get; set; }

	public Boolean Deleted { get; set; }

	public Int64 RowNumber { get; set; }

}

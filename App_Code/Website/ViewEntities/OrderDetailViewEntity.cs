using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

[Serializable]
public partial class OrderDetailViewEntity
{
	public Int32 ID { get; set; }

	public Int32 OrderID { get; set; }

	public Int32 ProductID { get; set; }

	public Int32 ColorID { get; set; }

	public Double Price { get; set; }

	public Int32 Quantity { get; set; }

	public String Picture { get; set; }

	public String ProductCode { get; set; }

	public String ProductName { get; set; }

	public String ColorName { get; set; }

	public String MaterialName { get; set; }

	public Int64 RowNumber { get; set; }

    public Int32 MaterialID { get; set; }
}

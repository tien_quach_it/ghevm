using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

public partial class CategoryViewEntity
{
	public int ID { get; set; }

    public int DisplayOrder { get; set; }
	
	public string CategoryName { get; set; }

	public int ParentID { get; set; }

    public bool IsFeatured { get; set; }

	public bool Deleted { get; set; }

    public string ParentCategoryName { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoUrl { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoTitle { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoKeywords { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoDescription { get; set; }
}

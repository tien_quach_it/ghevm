﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IStatistics
/// </summary>
public interface IStatistics
{
    DataTable StatisticsData { get; set; }
}
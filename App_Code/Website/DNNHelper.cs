﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Modules.Html;

/// <summary>
/// Summary description for WebCommon
/// </summary>
public class DNNHelper : Website.Common
{
    public DNNHelper()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string GetSeoAltText(object productCode)
    {
        if (productCode == null)
            return "Ghế văn phòng";
        return String.Format("Ghế văn phòng {0}", productCode);
    }

    public static string FormatPrice(double price)
    {
        return price.ToString("#,##0 VND").Replace(",", ".").Replace(".00 VND", " VND");
    }

    public static string FormatPriceWithNote(double price)
    {
        return FormatPrice(price) + "<p><i>Giá chưa bao gồm VAT 10%</i></p>";
    }

    public static string GetModuleSetting(Hashtable settings, string settingKey, string defaultValue)
    {
        string tmp = (string)settings[settingKey];
        if (String.IsNullOrEmpty(tmp))
            tmp = defaultValue;
        return tmp;
    }

    public static bool GetModuleSettingAsBool(Hashtable settings, string settingKey, bool defaultValue)
    {
        string tmp = (string)settings[settingKey];
        if (String.IsNullOrEmpty(tmp))
            return defaultValue;
        return Convert.ToBoolean(tmp);
    }

    public static string GetHtmlModuleContent(int moduleID)
    {        
        HtmlTextController controller = new HtmlTextController();
        return controller.GetTopHtmlText(moduleID, true, DotNetNuke.Common.Utilities.Null.NullInteger).Content;
    }

    public static bool IsMobileBrowser()
    {
        //GETS THE CURRENT USER CONTEXT
        HttpContext context = HttpContext.Current;

        //FIRST TRY BUILT IN ASP.NT CHECK
        if (context.Request.Browser.IsMobileDevice)
        {
            return true;
        }
        //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
        if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
        {
            return true;
        }
        //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
        if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
            context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
        {
            return true;
        }
        //AND FINALLY CHECK THE HTTP_USER_AGENT 
        //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
        if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
        {
            //Create a list of all mobile types
            string[] mobiles =
                new[]
                {
                    "midp", "j2me", "avant", "docomo", 
                    "novarra", "palmos", "palmsource", 
                    "240x320", "opwv", "chtml",
                    "pda", "windows ce", "mmp/", 
                    "blackberry", "mib/", "symbian", 
                    "wireless", "nokia", "hand", "mobi",
                    "phone", "cdm", "up.b", "audio", 
                    "SIE-", "SEC-", "samsung", "HTC", 
                    "mot-", "mitsu", "sagem", "sony"
                    , "alcatel", "lg", "eric", "vx", 
                    "NEC", "philips", "mmm", "xx", 
                    "panasonic", "sharp", "wap", "sch",
                    "rover", "pocket", "benq", "java", 
                    "pt", "pg", "vox", "amoi", 
                    "bird", "compal", "kg", "voda",
                    "sany", "kdd", "dbt", "sendo", 
                    "sgh", "gradi", "jb", "dddi", 
                    "moto", "iphone"
                };

            //Loop through each item in the list created above 
            //and check if the header contains that text
            foreach (string s in mobiles)
            {
                if (context.Request.ServerVariables["HTTP_USER_AGENT"].
                                                    ToLower().Contains(s.ToLower()))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public static string MemberFullName(object firstname, object middlename, object lastname)
    {
        if (!String.IsNullOrEmpty(middlename.ToString()))
            return String.Format("{0} {1} {2}", firstname.ToString(), middlename.ToString(), lastname.ToString());
        return String.Format("{0} {1}", firstname.ToString(), lastname.ToString());
    }

    #region Validate Query Strings

    public static bool ValidateQueryStrings(params string[] keys)
    {
        string value;
        foreach (string key in keys)
        {
            value = HttpContext.Current.Request.QueryString[key];
            if (!String.IsNullOrEmpty(value))
            {
                value = value.ToLower();
                if (value.Contains("$") || value.Contains("<"))
                    return false;
            }
        }
        return true;
    }

    #endregion

    public static string SendMailWithGoogle(string from, string to, string cc, string bcc, string subject, string body)
    {
        //try
        //{
            int port = 587;
            string smtpServer = "smtp.gmail.com";
            string smtpUser = "ghevanminh@gmail.com";
            string smtpPassword = "pass4now";
            bool enableSSL = true;
            if (String.IsNullOrEmpty(from))
                from = smtpUser;

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(from, to);

            if (!String.IsNullOrEmpty(cc))
                mail.CC.Add(cc);

            if (!String.IsNullOrEmpty(bcc))
                mail.Bcc.Add(bcc);

            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            client.Port = 587;
            client.Host = smtpServer;
            client.EnableSsl = enableSSL;
            client.Timeout = 10000;
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(smtpUser, smtpPassword);
            mail.IsBodyHtml = true;
            mail.Subject = subject;
            mail.Body = body;
            client.Send(mail);

            return "";
        //}
        //catch (Exception ex)
        //{
        //    return "Error: " + ex.Message + "<br/>" + ex.InnerException.Message;
        //}
    }

    public static bool UseDomainForHomePage
    {
        get
        {
            return Convert.ToBoolean(ConfigurationManager.AppSettings["UseDomainForHomePage"]);
        }
    }

    public static string GetHomePageUrl()
    {
        var portalSettings = CurrentPortalSettings;

        if (UseDomainForHomePage)
            return String.Format("{0}://{1}", HttpContext.Current.Request.Url.Scheme, portalSettings.PortalAlias.HTTPAlias);

        var controller = new TabController();
        return controller.GetTab(portalSettings.HomeTabId, portalSettings.PortalId, false).FullUrl;
    }
}
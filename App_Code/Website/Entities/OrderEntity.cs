using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

[Table(Name = "Order")]
public partial class OrderEntity
{
	#region Properties Mapping

	[Column(IsDbGenerated = true, IsPrimaryKey = true)]
	public int ID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string FullName { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Address { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Phone { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Fax { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Email { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Comment { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int StatusID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public double Total { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public DateTime OnDate { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public bool Deleted { get; set; }

	#endregion
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

[Table(Name = "Product")]
public partial class ProductEntity
{
	#region Properties Mapping

	[Column(IsDbGenerated = true, IsPrimaryKey = true)]
	public int ID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string ProductName { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string ProductCode { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int CategoryID { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string Picture { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string SpecsPicture { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Description { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Materials { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string Colors { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Warranty { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int DisplayOrder { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public double Price { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public bool IsFeatured { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public bool IsBestSell { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public bool IsNew { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public bool Deleted { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoUrl { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoTitle { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoKeywords { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoDescription { get; set; }

	#endregion
}

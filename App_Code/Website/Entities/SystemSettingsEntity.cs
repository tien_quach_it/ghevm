using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

[Table(Name = "SystemSettings")]
public partial class SystemSettingsEntity
{
	#region Properties Mapping

	[Column(IsDbGenerated = true, IsPrimaryKey = true)]
	public int ID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public bool EnableShoppingCart { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public bool NoPrice { get; set; }

	#endregion
}

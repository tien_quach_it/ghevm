﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping; 
/// <summary>
/// Summary description for SearchProductAndNews
/// </summary>
public class SearchProductAndNews
{
    #region Properties Mapping

    public int ID { get; set; }

    public string Title { get; set; }

    public string Picture { get; set; }

    public string Summary { get; set; }

    public string Description { get; set; }

    public string Type { get; set; }

    public string SeoUrl { get; set; }

    #endregion
}
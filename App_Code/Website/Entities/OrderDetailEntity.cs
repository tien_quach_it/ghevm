using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

[Table(Name = "OrderDetail")]
public partial class OrderDetailEntity
{
	#region Properties Mapping

	[Column(IsDbGenerated = true, IsPrimaryKey = true)]
	public int ID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int OrderID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int ProductID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int ColorID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public double Price { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int Quantity { get; set; }

	#endregion
}

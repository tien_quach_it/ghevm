using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

[Table(Name = "Shop")]
public partial class ShopEntity
{
	#region Properties Mapping

	[Column(IsDbGenerated = true, IsPrimaryKey = true)]
	public int ID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int ProvinceID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int RegionID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Name { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Address { get; set; }

	[Column(CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
	public string Longitude { get; set; }

	[Column(CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
	public string Latitude { get; set; }

    [Column(CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
    public string ForceLongitude { get; set; }

    [Column(CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
    public string ForceLatitude { get; set; }

	[Column(CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
	public string Phone { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int DisplayOrder { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public bool Deleted { get; set; }

    [Column(CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
    public string Email { get; set; }

	#endregion
}

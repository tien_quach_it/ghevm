using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

[Table(Name = "OnlineSupport")]
public partial class OnlineSupportEntity
{
	#region Properties Mapping

	[Column(IsDbGenerated = true, IsPrimaryKey = true)]
	public int ID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string YahooID { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public int DisplayOrder { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public bool Deleted { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public int CategoryID { get; set; }

	#endregion
}

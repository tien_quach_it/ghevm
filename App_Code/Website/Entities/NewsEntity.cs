using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

[Table(Name = "News")]
public partial class NewsEntity
{
	#region Properties Mapping

	[Column(IsDbGenerated = true, IsPrimaryKey = true)]
	public int ID { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Title { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Picture { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Summary { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string Description { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int DisplayOrder { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public DateTime DateCreated { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public bool Deleted { get; set; }

	#endregion
}

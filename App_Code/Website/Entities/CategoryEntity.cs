using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq.Mapping;

[Table(Name = "Category")]
public partial class CategoryEntity
{
	#region Properties Mapping

	[Column(IsDbGenerated = true, IsPrimaryKey = true)]
	public int ID { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public int DisplayOrder { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public string CategoryName { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public int ParentID { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public bool IsFeatured { get; set; }

	[Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
	public bool Deleted { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoUrl { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoTitle { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoKeywords { get; set; }

    [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string SeoDescription { get; set; }

	#endregion
}

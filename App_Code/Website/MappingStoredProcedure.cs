using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using Core.DAL;

public partial class MappingStoredProcedure : ProjectDataContext
{
    [Function(Name = "SP_CataloguePhotoWithCount")]
    [ResultType(typeof(CataloguePhotoEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_CataloguePhotoWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }


    [Function(Name = "SP_CategoryWithCount")]
    [ResultType(typeof(CategoryViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_CategoryWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }


    [Function(Name = "SP_ProductWithCount")]
    [ResultType(typeof(ProductEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_ProductWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy,
                                            [Parameter(Name = "CategoryID", DbType = "NVarChar")] string categoryID)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy, categoryID);

        return (IMultipleResults)(results.ReturnValue);
    }


    [Function(Name = "SP_ProductImageWithCount")]
    [ResultType(typeof(ProductImageEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_ProductImageWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy,
                                            [Parameter(Name = "ProductID", DbType = "NVarChar")] string productID)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy, productID);

        return (IMultipleResults)(results.ReturnValue);
    }


    [Function(Name = "SP_ProductSizeImageWithCount")]
    [ResultType(typeof(ProductSizeImageEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_ProductSizeImageWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy,
                                            [Parameter(Name = "ProductID", DbType = "NVarChar")] string productID)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy, productID);

        return (IMultipleResults)(results.ReturnValue);
    }


    [Function(Name = "SP_ProductColorWithCount")]
    [ResultType(typeof(ProductColorViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_ProductColorWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy,
                                            [Parameter(Name = "ProductID", DbType = "NVarChar")] string productID)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy, productID);

        return (IMultipleResults)(results.ReturnValue);
    }


    [Function(Name = "SP_BestSellProducts")]
    public ISingleResult<ProductViewEntity> SP_BestSellProducts([Parameter(Name = "Top", DbType = "NVarChar")] string top)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), top);

        return (ISingleResult<ProductViewEntity>)(results.ReturnValue);
    }


    [Function(Name = "SP_FeaturedProducts")]
    public ISingleResult<ProductViewEntity> SP_FeaturedProducts()
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));

        return (ISingleResult<ProductViewEntity>) (results.ReturnValue);
    }

    [Function(Name = "SP_FeaturedCategories")]
    public ISingleResult<CategoryEntity> SP_FeaturedCategories()
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));

        return (ISingleResult<CategoryEntity>)(results.ReturnValue);
    }


    public string SP_GetProdctNextDisplayOrder(string categoryID)
    {
        return DBHelper.ExecuteScalarFormat("exec [SP_GetProdctNextDisplayOrder] '{0}'", categoryID);
    }

    public string SP_GetProductImageNextDisplayOrder(string productID)
    {
        return DBHelper.ExecuteScalarFormat("exec [SP_GetProductImageNextDisplayOrder] '{0}'", productID);
    }

    public string SP_GetProductSizeImageNextDisplayOrder(string productID)
    {
        return DBHelper.ExecuteScalarFormat("exec [SP_GetProductSizeImageNextDisplayOrder] '{0}'", productID);
    }

    [Function(Name = "SP_OrderWithCount")]
    [ResultType(typeof(OrderViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_OrderWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }

    #region Ha.Huynh Added

    [Function(Name = "SP_HomeBannerWithCount")]
    [ResultType(typeof(HomeBannerViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_HomeBannerWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }

    [Function(Name = "SP_OnlineSupportWithCount")]
    [ResultType(typeof(OnlineSupportViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_OnlineSupportWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }

    [Function(Name = "SP_ProvinceWithCount")]
    [ResultType(typeof(ProvinceViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_ProvinceWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }

    [Function(Name = "SP_RegionWithCount")]
    [ResultType(typeof(RegionViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_RegionWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy,
                                            [Parameter(Name = "ProvinceID", DbType = "NVarChar")] string provinceId)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy, provinceId);

        return (IMultipleResults)(results.ReturnValue);
    }

    [Function(Name = "SP_ShopWithCount")]
    [ResultType(typeof(ShopViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_ShopWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy,
                                            [Parameter(Name = "ProvinceID", DbType = "NVarChar")] string provinceID,
                                            [Parameter(Name = "RegionID", DbType = "NVarChar")] string regionID)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy, provinceID, regionID);

        return (IMultipleResults)(results.ReturnValue);
    }

    [Function(Name = "SP_MaterialWithCount")]
    [ResultType(typeof(MaterialEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_MaterialWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }

    [Function(Name = "SP_NewsWithCount")]
    [ResultType(typeof(NewsViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_NewsWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }

    [Function(Name = "SP_SearchProductAndNews")]
    [ResultType(typeof(SearchProductAndNews))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_SearchProductAndNews([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy,
                                            [Parameter(Name = "Title", DbType = "NVarChar")] string title)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy, title);

        return (IMultipleResults)(results.ReturnValue);
    }

    [Function(Name = "SP_ServiceWithCount")]
    [ResultType(typeof(ServiceViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_ServiceWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }

    [Function(Name = "SP_OrderDetailWithCount")]
    [ResultType(typeof(OrderDetailViewEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_OrderDetailWithCount([Parameter(Name = "OrderID", DbType = "NVarChar")] string orderID, 
                                            [Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), orderID, pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }

    #endregion

    [Function(Name = "SP_PartnerWithCount")]
    [ResultType(typeof(PartnerEntity))]
    [ResultType(typeof(int))]
    public IMultipleResults SP_PartnerWithCount([Parameter(Name = "PageIndex", DbType = "NVarChar")] string pageIndex,
                                            [Parameter(Name = "PageSize", DbType = "NVarChar")] string pageSize,
                                            [Parameter(Name = "OrderBy", DbType = "NVarChar")] string orderBy)
    {
        IExecuteResult results = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pageIndex, pageSize, orderBy);

        return (IMultipleResults)(results.ReturnValue);
    }
}
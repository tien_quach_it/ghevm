﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Text.RegularExpressions;
using Website;

/// <summary>
/// Summary description for SeoHelper
/// </summary>
public static class SeoHelper
{
    private const string XmlPath = "~/SiteUrls.config";

    public static void UpdateSiteUrlsConfig(CategoryEntity category)
    {
        string xmlConfig = GetXmlPath();
        XmlDocument doc = new XmlDocument();
        doc.Load(xmlConfig);

        XmlNode rules = doc.SelectSingleNode("//Rules");

        var rewriteRule = doc.SelectSingleNode("//RewriterRule[@ID='" + GetCategoryConfigID(category.ID) + "']");
        if (rewriteRule == null)
        {
            rewriteRule = doc.CreateElement("RewriterRule");
            XmlAttribute attrID = doc.CreateAttribute("ID");
            attrID.Value = GetCategoryConfigID(category.ID);
            rewriteRule.Attributes.Append(attrID);
            rules.AppendChild(rewriteRule);

            rewriteRule.InnerXml = "<LookFor>.*" + category.SeoUrl + "(.*)</LookFor><SendTo>~/Default.aspx?TabId=57&amp;ID=" + category.ID + "$1</SendTo>";
        }
        else
        {
            rewriteRule.InnerXml = "<LookFor>.*" + category.SeoUrl + "(.*)</LookFor><SendTo>~/Default.aspx?TabId=57&amp;ID=" + category.ID + "$1</SendTo>";
        }

        doc.Save(xmlConfig);

        // Update Products of category
        DataTable tbl = DB.GetTable("select ID, ProductCode, CategorySeoUrl from vw_Product where Deleted = 0 and CategoryID = " + category.ID);
        foreach (DataRow row in tbl.Rows)
        {
            string url = SeoHelper.ConvertToUnsign(row["CategorySeoUrl"].ToString().Replace(".aspx", "/") +
                                            row["ProductCode"] + ".aspx");
            DB.ExecuteSQLFormat("Update Product set SeoUrl = '{0}' where ID = {1}", url, row["ID"]);

            SeoHelper.UpdateSiteUrlsConfig(new ProductEntity { ID = Convert.ToInt32(row["ID"]), SeoUrl = url });
        }
    }

    public static void RemoveCategoryFromSiteUrlsConfig(CategoryEntity category)
    {
        string xmlConfig = GetXmlPath();

        XmlDocument doc = new XmlDocument();
        doc.Load(xmlConfig);

        XmlNode rules = doc.SelectSingleNode("//Rules");

        var rewriteRule = doc.SelectSingleNode("//RewriterRule[@ID='" + GetCategoryConfigID(category.ID) + "']");
        if (rewriteRule != null)
        {
            rules.RemoveChild(rewriteRule);
        }

        doc.Save(xmlConfig);
    }

    private static string GetCategoryConfigID(int id)
    {
        return String.Format("cat-{0}", id);
    }

    public static void UpdateSiteUrlsConfig(ProductEntity product)
    {
        string xmlConfig = GetXmlPath();

        XmlDocument doc = new XmlDocument();
        doc.Load(xmlConfig);

        XmlNode rules = doc.SelectSingleNode("//Rules");

        var rewriteRule = doc.SelectSingleNode("//RewriterRule[@ID='" + GetProductConfigID(product.ID) + "']");
        if (rewriteRule == null)
        {
            rewriteRule = doc.CreateElement("RewriterRule");
            XmlAttribute attrID = doc.CreateAttribute("ID");
            attrID.Value = GetProductConfigID(product.ID);
            rewriteRule.Attributes.Append(attrID);
            rules.AppendChild(rewriteRule);

            rewriteRule.InnerXml = "<LookFor>.*" + product.SeoUrl + "(.*)</LookFor><SendTo>~/Default.aspx?TabId=97&amp;ID=" + product.ID + "$1</SendTo>";
        }
        else
        {
            rewriteRule.InnerXml = "<LookFor>.*" + product.SeoUrl + "(.*)</LookFor><SendTo>~/Default.aspx?TabId=97&amp;ID=" + product.ID + "$1</SendTo>";
        }

        doc.Save(xmlConfig);
    }

    public static void RemoveProductFromSiteUrlsConfig(ProductEntity product)
    {
        string xmlConfig = GetXmlPath();

        XmlDocument doc = new XmlDocument();
        doc.Load(xmlConfig);

        XmlNode rules = doc.SelectSingleNode("//Rules");

        var rewriteRule = doc.SelectSingleNode("//RewriterRule[@ID='" + GetProductConfigID(product.ID) + "']");
        if (rewriteRule != null)
        {
            rules.RemoveChild(rewriteRule);
        }

        doc.Save(xmlConfig);
    }

    private static string GetProductConfigID(int id)
    {
        return String.Format("product-{0}", id);
    }

    private static string GetXmlPath()
    {
        return HttpContext.Current.Server.MapPath(XmlPath);
    }

    public static string ConvertToUnsign(string s)
    {
        Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
        string temp = s.Normalize(NormalizationForm.FormD);
        return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').Replace(" ", "-").Replace("---", "-").Trim(' ', '-').ToLower();
    }  
}
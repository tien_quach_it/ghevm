using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class ProductManager : DataBaseProcessBase<ProductEntity, ProjectDataContext>
{
	public void Insert(ProductEntity product)
	{
		base.Add(product, "ID");
	}

	public void Update(ProductEntity product)
	{
		base.Update(product, o => o.ID == product.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<ProductEntity> GetProducts()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<ProductEntity> GetProducts(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public ProductEntity GetProductById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(ProductEntity product)
	{
		if (base.Count(null, o => o.ID == product.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

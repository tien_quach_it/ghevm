using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class SystemSettingsManager : DataBaseProcessBase<SystemSettingsEntity, ProjectDataContext>
{
	public void Insert(SystemSettingsEntity systemSettings)
	{
		base.Add(systemSettings, "ID");
	}

	public void Update(SystemSettingsEntity systemSettings)
	{
		base.Update(systemSettings, o => o.ID == systemSettings.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<SystemSettingsEntity> GetSystemSettingss()
	{
		return base.Get(null, 0, int.MaxValue);
	}

	public IList<SystemSettingsEntity> GetSystemSettingss(int startIndex, int endIndex)
	{
		return base.Get(null, startIndex, endIndex);
	}

	public SystemSettingsEntity GetSystemSettingsById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(SystemSettingsEntity systemSettings)
	{
		if (base.Count(null, o => o.ID == systemSettings.ID, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

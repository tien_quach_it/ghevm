using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class ProductSizeImageManager : DataBaseProcessBase<ProductSizeImageEntity, ProjectDataContext>
{
	public void Insert(ProductSizeImageEntity productSizeImage)
	{
		base.Add(productSizeImage, "ID");
	}

	public void Update(ProductSizeImageEntity productSizeImage)
	{
		base.Update(productSizeImage, o => o.ID == productSizeImage.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<ProductSizeImageEntity> GetProductSizeImages()
	{
		return base.Get(null, 0, int.MaxValue);
	}

	public IList<ProductSizeImageEntity> GetProductSizeImages(int startIndex, int endIndex)
	{
		return base.Get(null, startIndex, endIndex);
	}

	public ProductSizeImageEntity GetProductSizeImageById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(ProductSizeImageEntity productSizeImage)
	{
		if (base.Count(null, o => o.ID == productSizeImage.ID, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

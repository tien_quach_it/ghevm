using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class ProductImageManager : DataBaseProcessBase<ProductImageEntity, ProjectDataContext>
{
	public void Insert(ProductImageEntity productImage)
	{
		base.Add(productImage, "ID");
	}

	public void Update(ProductImageEntity productImage)
	{
		base.Update(productImage, o => o.ID == productImage.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<ProductImageEntity> GetProductImages()
	{
		return base.Get(null, 0, int.MaxValue);
	}

	public IList<ProductImageEntity> GetProductImages(int startIndex, int endIndex)
	{
		return base.Get(null, startIndex, endIndex);
	}

	public ProductImageEntity GetProductImageById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(ProductImageEntity productImage)
	{
		if (base.Count(null, o => o.ID == productImage.ID, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

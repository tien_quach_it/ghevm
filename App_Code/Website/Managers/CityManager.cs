using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class CityManager : DataBaseProcessBase<CityEntity, ProjectDataContext>
{
	public void Insert(CityEntity city)
	{
		base.Add(city, "ID");
	}

	public void Update(CityEntity city)
	{
		base.Update(city, o => o.ID == city.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<CityEntity> GetCitys()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<CityEntity> GetCitys(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public CityEntity GetCityById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(CityEntity city)
	{
		if (base.Count(null, o => o.ID == city.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class PartnerManager : DataBaseProcessBase<PartnerEntity, ProjectDataContext>
{
	public void Insert(PartnerEntity partner)
	{
		base.Add(partner, "ID");
	}

	public void Update(PartnerEntity partner)
	{
		base.Update(partner, o => o.ID == partner.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<PartnerEntity> GetPartners()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<PartnerEntity> GetPartners(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public PartnerEntity GetPartnerById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(PartnerEntity partner)
	{
		if (base.Count(null, o => o.ID == partner.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

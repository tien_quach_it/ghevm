using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class OrderDetailManager : DataBaseProcessBase<OrderDetailEntity, ProjectDataContext>
{
	public void Insert(OrderDetailEntity orderDetail)
	{
		base.Add(orderDetail, "ID");
	}

	public void Update(OrderDetailEntity orderDetail)
	{
		base.Update(orderDetail, o => o.ID == orderDetail.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<OrderDetailEntity> GetOrderDetails()
	{
		return base.Get(null, 0, int.MaxValue);
	}

	public IList<OrderDetailEntity> GetOrderDetails(int startIndex, int endIndex)
	{
		return base.Get(null, startIndex, endIndex);
	}

	public OrderDetailEntity GetOrderDetailById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(OrderDetailEntity orderDetail)
	{
		if (base.Count(null, o => o.ID == orderDetail.ID, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

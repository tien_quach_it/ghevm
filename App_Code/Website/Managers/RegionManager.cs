using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class RegionManager : DataBaseProcessBase<RegionEntity, ProjectDataContext>
{
	public void Insert(RegionEntity region)
	{
		base.Add(region, "ID");
	}

	public void Update(RegionEntity region)
	{
		base.Update(region, o => o.ID == region.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<RegionEntity> GetRegions()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<RegionEntity> GetRegions(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public RegionEntity GetRegionById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(RegionEntity region)
	{
		if (base.Count(null, o => o.ID == region.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

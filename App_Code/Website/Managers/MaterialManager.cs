using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class MaterialManager : DataBaseProcessBase<MaterialEntity, ProjectDataContext>
{
	public void Insert(MaterialEntity material)
	{
		base.Add(material, "ID");
	}

	public void Update(MaterialEntity material)
	{
		base.Update(material, o => o.ID == material.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<MaterialEntity> GetMaterials()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<MaterialEntity> GetMaterials(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public MaterialEntity GetMaterialById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(MaterialEntity material)
	{
		if (base.Count(null, o => o.ID == material.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class StoreManager : DataBaseProcessBase<StoreEntity, ProjectDataContext>
{
	public void Insert(StoreEntity store)
	{
		base.Add(store, "ID");
	}

	public void Update(StoreEntity store)
	{
		base.Update(store, o => o.ID == store.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<StoreEntity> GetStores()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<StoreEntity> GetStores(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public StoreEntity GetStoreById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(StoreEntity store)
	{
		if (base.Count(null, o => o.ID == store.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

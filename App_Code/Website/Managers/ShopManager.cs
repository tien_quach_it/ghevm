using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class ShopManager : DataBaseProcessBase<ShopEntity, ProjectDataContext>
{
	public void Insert(ShopEntity shop)
	{
		base.Add(shop, "ID");
	}

	public void Update(ShopEntity shop)
	{
		base.Update(shop, o => o.ID == shop.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<ShopEntity> GetShops()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<ShopEntity> GetShops(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public ShopEntity GetShopById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(ShopEntity shop)
	{
		if (base.Count(null, o => o.ID == shop.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class OrderManager : DataBaseProcessBase<OrderEntity, ProjectDataContext>
{
	public void Insert(OrderEntity order)
	{
		base.Add(order, "ID");
	}

	public void Update(OrderEntity order)
	{
		base.Update(order, o => o.ID == order.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<OrderEntity> GetOrders()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<OrderEntity> GetOrders(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public OrderEntity GetOrderById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(OrderEntity order)
	{
		if (base.Count(null, o => o.ID == order.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

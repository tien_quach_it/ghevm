using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class CataloguePhotoManager : DataBaseProcessBase<CataloguePhotoEntity, ProjectDataContext>
{
	public void Insert(CataloguePhotoEntity cataloguePhoto)
	{
		base.Add(cataloguePhoto, "ID");
	}

	public void Update(CataloguePhotoEntity cataloguePhoto)
	{
		base.Update(cataloguePhoto, o => o.ID == cataloguePhoto.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<CataloguePhotoEntity> GetCataloguePhotos()
	{
		return base.Get(null, 0, int.MaxValue);
	}

	public IList<CataloguePhotoEntity> GetCataloguePhotos(int startIndex, int endIndex)
	{
		return base.Get(null, startIndex, endIndex);
	}

	public CataloguePhotoEntity GetCataloguePhotoById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(CataloguePhotoEntity cataloguePhoto)
	{
		if (base.Count(null, o => o.ID == cataloguePhoto.ID, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Core.DAL;

public partial class OnlineSupportManager : DataBaseProcessBase<OnlineSupportEntity, ProjectDataContext>
{
	public void Insert(OnlineSupportEntity onlineSupport)
	{
		base.Add(onlineSupport, "ID");
	}

	public void Update(OnlineSupportEntity onlineSupport)
	{
		base.Update(onlineSupport, o => o.ID == onlineSupport.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<OnlineSupportEntity> GetOnlineSupports()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<OnlineSupportEntity> GetOnlineSupports(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public OnlineSupportEntity GetOnlineSupportById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(OnlineSupportEntity onlineSupport)
	{
		if (base.Count(null, o => o.ID == onlineSupport.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

    public static List<ListItem> GetCategories()
    {
        var lst = new List<ListItem>();
        lst.Add(new ListItem("Xưởng", "1"));
        lst.Add(new ListItem("Showroom", "2"));
        return lst;
    }

}


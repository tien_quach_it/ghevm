using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class ServiceManager : DataBaseProcessBase<ServiceEntity, ProjectDataContext>
{
	public void Insert(ServiceEntity service)
	{
		base.Add(service, "ID");
	}

	public void Update(ServiceEntity service)
	{
		base.Update(service, o => o.ID == service.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<ServiceEntity> GetServices()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<ServiceEntity> GetServices(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public ServiceEntity GetServiceById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(ServiceEntity service)
	{
		if (base.Count(null, o => o.ID == service.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

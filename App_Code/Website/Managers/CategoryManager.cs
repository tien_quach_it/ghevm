using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class CategoryManager : DataBaseProcessBase<CategoryEntity, ProjectDataContext>
{
	public void Insert(CategoryEntity category)
	{
		base.Add(category, "ID");
	}

	public void Update(CategoryEntity category)
	{
		base.Update(category, o => o.ID == category.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<CategoryEntity> GetCategorys()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<CategoryEntity> GetCategorys(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public CategoryEntity GetCategoryById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(CategoryEntity category)
	{
		if (base.Count(null, o => o.ID == category.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

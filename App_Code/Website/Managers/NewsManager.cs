using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class NewsManager : DataBaseProcessBase<NewsEntity, ProjectDataContext>
{
	public void Insert(NewsEntity news)
	{
		base.Add(news, "ID");
	}

	public void Update(NewsEntity news)
	{
		base.Update(news, o => o.ID == news.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<NewsEntity> GetNewss()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<NewsEntity> GetNewss(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public NewsEntity GetNewsById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(NewsEntity news)
	{
		if (base.Count(null, o => o.ID == news.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class HomeBannerSettingManager : DataBaseProcessBase<HomeBannerSettingEntity, ProjectDataContext>
{
    public void Insert(HomeBannerSettingEntity homeBanner)
    {
        base.Add(homeBanner, "ID");
    }

    public void Update(HomeBannerSettingEntity homeBanner)
    {
        base.Update(homeBanner, o => o.ID == homeBanner.ID);
    }

    public void Delete(int iD)
    {
        base.Delete(o => o.ID.Equals(iD));
    }

    public IList<HomeBannerSettingEntity> GetHomeBannerSettings()
    {
        return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
    }

    public IList<HomeBannerSettingEntity> GetHomeBannerSettings(int startIndex, int endIndex)
    {
        return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
    }

    public HomeBannerSettingEntity GetHomeBannerSettingById(int iD)
    {
        return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
    }

    public bool IsExist(HomeBannerSettingEntity homeBanner)
    {
        if (base.Count(null, o => o.ID == homeBanner.ID && o.Deleted == false, 0, int.MaxValue) > 0)
        {
            return true;
        }
        return false;
    }

}

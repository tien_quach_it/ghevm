using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class ProvinceManager : DataBaseProcessBase<ProvinceEntity, ProjectDataContext>
{
	public void Insert(ProvinceEntity province)
	{
		base.Add(province, "ID");
	}

	public void Update(ProvinceEntity province)
	{
		base.Update(province, o => o.ID == province.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<ProvinceEntity> GetProvinces()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<ProvinceEntity> GetProvinces(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public ProvinceEntity GetProvinceById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(ProvinceEntity province)
	{
		if (base.Count(null, o => o.ID == province.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

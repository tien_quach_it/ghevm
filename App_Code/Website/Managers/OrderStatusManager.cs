using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class OrderStatusManager : DataBaseProcessBase<OrderStatusEntity, ProjectDataContext>
{
	public void Insert(OrderStatusEntity orderStatus)
	{
		base.Add(orderStatus, "ID");
	}

	public void Update(OrderStatusEntity orderStatus)
	{
		base.Update(orderStatus, o => o.ID == orderStatus.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<OrderStatusEntity> GetOrderStatuss()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<OrderStatusEntity> GetOrderStatuss(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public OrderStatusEntity GetOrderStatusById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(OrderStatusEntity orderStatus)
	{
		if (base.Count(null, o => o.ID == orderStatus.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

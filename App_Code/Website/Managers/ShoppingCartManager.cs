using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class ShoppingCartManager : DataBaseProcessBase<ShoppingCartEntity, ProjectDataContext>
{
	public void Insert(ShoppingCartEntity shoppingCart)
	{
		base.Add(shoppingCart, "ID");
	}

	public void Update(ShoppingCartEntity shoppingCart)
	{
		base.Update(shoppingCart, o => o.ID == shoppingCart.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<ShoppingCartEntity> GetShoppingCarts()
	{
		return base.Get(null, 0, int.MaxValue);
	}

	public IList<ShoppingCartEntity> GetShoppingCarts(int startIndex, int endIndex)
	{
		return base.Get(null, startIndex, endIndex);
	}

	public ShoppingCartEntity GetShoppingCartById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(ShoppingCartEntity shoppingCart)
	{
		if (base.Count(null, o => o.ID == shoppingCart.ID, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;

public partial class ProductColorManager : DataBaseProcessBase<ProductColorEntity, ProjectDataContext>
{
	public void Insert(ProductColorEntity productColor)
	{
		base.Add(productColor, "ID");
	}

	public void Update(ProductColorEntity productColor)
	{
		base.Update(productColor, o => o.ID == productColor.ID);
	}

	public void Delete(int iD)
	{
		base.Delete(o => o.ID.Equals(iD));
	}

	public IList<ProductColorEntity> GetProductColors()
	{
		return base.Get(o => o.Deleted.Equals(false), 0, int.MaxValue);
	}

	public IList<ProductColorEntity> GetProductColors(int startIndex, int endIndex)
	{
		return base.Get(o => o.Deleted.Equals(false), startIndex, endIndex);
	}

	public ProductColorEntity GetProductColorById(int iD)
	{
		return base.Get(o => o.ID.Equals(iD), 0, 0).SingleOrDefault();
	}

	public bool IsExist(ProductColorEntity productColor)
	{
		if (base.Count(null, o => o.ID == productColor.ID && o.Deleted == false, 0, int.MaxValue) > 0)
		{
			return true;
		}
		return false;
	}

}

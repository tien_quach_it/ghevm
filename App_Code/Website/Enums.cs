﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public enum eOrderStatus
{
    Pending = 1,
    Completed = 2,
    Cancelled = 3
}
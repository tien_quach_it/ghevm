﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for getShopList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class getShopList : System.Web.Services.WebService {

    public getShopList () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetMapList(string province, string region)
    {
        ShopManager dal = new ShopManager();
        IList<ShopEntity> lst = dal.GetShops();

        if (province != "0")
            lst = lst.Where(o => o.ProvinceID == Convert.ToInt32(province)).ToList();

        if (region != "0")
            lst = lst.Where(o => o.RegionID == Convert.ToInt32(region)).ToList();

        ICollection<ShopEntity> forceListMaps = new List<ShopEntity>();
        foreach (var item in lst)
        {
            if (!String.IsNullOrEmpty(item.ForceLatitude))
                item.Latitude = item.ForceLatitude;
            if (!String.IsNullOrEmpty(item.ForceLongitude))
                item.Longitude = item.ForceLongitude;
            forceListMaps.Add(item);
        }

        //var json = new JavaScriptSerializer().Serialize(lst);
        var json = new JavaScriptSerializer().Serialize(forceListMaps);
        return json.ToString();        
    }

    [WebMethod]
    public string GetRegionByProvince(string province)
    {
        // RegionID
        RegionManager dalRegionID = new RegionManager();
        var lstRegionID = dalRegionID.GetRegions().Where(o=>o.ProvinceID == Convert.ToInt32(province)).OrderBy(o=>o.RegionName).ToList();

        var json = new JavaScriptSerializer().Serialize(lstRegionID);
        return json.ToString();
    }
}

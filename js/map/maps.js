var map;
var bounds;
var maxServiceHight;
var DEFAULT_LAT = 10.823099;
var DEFFAULT_LONG = 106.629664;
function getCentreMarkerOptions(latLng) {
	var m = { 
		position: latLng,
		animation: google.maps.Animation.DROP,
		clickable: true,
		icon: new google.maps.MarkerImage("portals/0/images/display_centre.png", google.maps.Size(35,35)),
		map: map		
	};
	return m;
}

function getHomeMarkerOptions(latLng) {
	var m = { 
		position: latLng,
		animation: google.maps.Animation.DROP,
		clickable: true,
		icon: new google.maps.MarkerImage("portals/0/images/display_home.png", google.maps.Size(35, 35)),
		map: map		
	};
	return m;
}

function getShopMarkerOptions(latLng) {
	var m = { 
		position: latLng,
		animation: google.maps.Animation.DROP,
		clickable: true,
		icon: new google.maps.MarkerImage("portals/0/images/display-supplier.png", google.maps.Size(35, 35)),
		map: shop_map
	};
	return m;
}

function getShopInfoWindow(marker, content) {
    infowindow = new google.maps.InfoWindow({ maxWidth: 420, maxHeight: 300 });
    // create info window and attach to marker click event
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(content);        
        infowindow.open(shop_map, marker);
    });
}

function getShopsMap(shops) {
    var centerLat = DEFAULT_LAT;
    var centerLong = DEFFAULT_LONG;

    if (shops.length > 0) {
        centerLat = shops[0].Latitude;
        centerLong = shops[0].Longitude;
    }

    var myOptions = {
        zoom: mZoom,
        center: new google.maps.LatLng(centerLat, centerLong, false),
        disableDefaultUI: true,
        scrollwheel: true,
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        mapTypeControl: true,
    	mapTypeControlOptions: {
        	style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        	position: google.maps.ControlPosition.TOP_RIGHT
    	},
    	panControl: false,
    	panControlOptions: {
        	position: google.maps.ControlPosition.TOP_RIGHT
    	},
    	streetViewControl: true,
	    streetViewControlOptions: {
	        position: google.maps.ControlPosition.TOP_RIGHT
	    },
        mapTypeId:google.maps.MapTypeId.ROADMAP
      }; 
	// create map
    shop_map = new google.maps.Map(document.getElementById("shop_map_canvas"), myOptions);

    var markers = [];
	for (var i = 0; i < shops.length; i++) {
	    var latLng = new google.maps.LatLng(shops[i].Latitude, shops[i].Longitude)
	    var markerOptions = getShopMarkerOptions(latLng);
	    var marker = new google.maps.Marker(markerOptions);

	    var info = [];

	    info.push(buildPopupTemplate(shops[i].Name, shops[i].Address, shops[i].Email, shops[i].Phone));

	    // attach info window to the marker
	    getShopInfoWindow(marker, info.join(""));
		
		markers.push(marker);		
	}

    var markerCluster = new MarkerClusterer(shop_map, markers);
	markerCluster.setMaxZoom(10);
}

function buildPopupTemplate(name, address, email, phone) {
    var html = '<div class="windowInfoPopup">';        
        html +=     '<div class="contentMap"><div class="basicInfoLeft">';
        html +=         '<h2>';
        html +=             name;
        html +=         '</h2>';
        html +=         '<div>';
        html +=             address;
        html +=         '</div>';
        if (email != null) {
            html += '<div>Email: ';
            html += email;
            html += '</div>';
        }
        if (phone != null) {
            html += '<div>Phone: ';
            html += phone;
            html += '</div>';
        }
        html +=     '</div>';
        html += '</div>';        
        return html;
}
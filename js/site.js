﻿$(function () {
    //    $('.service').click(function () {
    //        var $show_more = $(this).closest('.services_container').find('.show_more');
    //        if ($show_more.css('display') == "none") {
    //            $show_more.fadeIn();
    //            $(this).addClass('hide');
    //            $(this).removeClass('show');
    //            $(this).html('Thu Lại');
    //        }
    //        else {
    //            $show_more.fadeOut();
    //            $(this).addClass('show');
    //            $(this).removeClass('hide');
    //            $(this).html('Chi Tiết');
    //        }
    //        return false;
    //    });

    // Desktop Search
    var urlSearch = '/KetQuaTimKiem.aspx?term=';

    $('.btnSearch a').on('click', function () {
        var txt = $('.txtSearch').val();
        if (txt == "") {
            alert('Nhập tiêu đề tìm kiếm.');
        }
        else {
            var url = urlSearch + txt;
            window.open(url, "_self");
        }
        return false;
    });

    $("#txtSearchMobile").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $('.mobile-search-container a').click();
        }
    });

    // Mobile Search
    $('.mobile-search-container a').on('click', function () {
        var txt = $('#txtSearchMobile').val();
        if (txt == "") {
            alert('Nhập tiêu đề tìm kiếm.');
        }
        else {
            var url = urlSearch + txt;
            window.open(url, "_self");
        }
        return false;
    });

    $('input.txtSearch').on('keypress', function (e) {
        var txt = $('.txtSearch').val();
        key = e.keyCode ? e.keyCode : e.which;
        if (key == 13) {
            if (txt == "") {
                alert('Nhập tiêu đề tìm kiếm.');
                return false;
            }
            else {
                e.preventDefault();
                var url = urlSearch + txt;
                window.open(url, "_self");
            }
        }
        else
            return true;
    });

    $(".push").append("<div class='overlay'></div>");

    $('.open-left-button').click(function () {
        
        var toggle_el = $(this).data('toggle');
        $(toggle_el).addClass('fly-in-left');
        $('.push').addClass("open-left"); //push content left
        $(".stickyHeader").css({ left: "240px" }); // Due to the sticky menu
        $('nav#mobile').removeClass('fixed-on-top'); //make top menu not fixed
        $(".overlay").addClass('overlay-show');
        $(".mobileBreak").hide();
        $('html, body').css('overflowX', 'hidden');
        return false;
    });
    $(".overlay").click(function () {
        $(".push").removeClass('open-right');
        $(".push").removeClass('open-left');
        $(".stickyHeader").css({ left: "0" }); // Due to the sticky menu
        $(".fly-in-right").removeClass('fly-in-right');
        $(".fly-in-left").removeClass('fly-in-left');
        $('nav#mobile').addClass('fixed-on-top')
        $(".overlay").removeClass('overlay-show');
        $(".mobileBreak").show();
        $('html, body').css('overflowX', 'auto');
        return false;
    });

    //$('#myModal').modal('toggle');


    //mobile menu
    $(".childIndicator").click(function () {
        $(".topMobileNav").css("left", "-242px");
        $(this).next(".sub").addClass("open");
    });

    $(".mobileMenuBackBtn").click(function () {
        $(".topMobileNav").css("left", "0");
        $(".topMobileNav .sub").removeClass("open");
    });


    // Sticky Mobile Header
    var $stickyHeader = $('.stickyHeader');
    var $content = $(".content");

    // Check the initial Poistion of the Sticky Header
    var stickyMobileHeaderTop = $stickyHeader.offset().top;
    var stickyMobileHeaderHeight = $stickyHeader.outerHeight();
    //alert(stickyHeaderHeight)
    $(window).scroll(function () {
        if ($(window).scrollTop() > stickyMobileHeaderHeight) {
            $stickyHeader.css({ position: 'fixed', top: '0px', height: stickyMobileHeaderHeight + "px" });
            $content.css({ "margin-top": stickyMobileHeaderHeight + "px" });
        } else {
            $stickyHeader.css({ position: 'static', top: '0px' });
            $content.css({ "margin-top": 0 });
        }

        // Move the main nav down to scroll top position
        //alert($(window).scrollTop())
        $(".mobileMainNav").css({ top: $(window).scrollTop() + "px" });
    });
});

function formatMoney(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " VND";
}

function isIE() {
    return (window.navigator.userAgent.indexOf('MSIE ') > 0);
}

function convertToUnsign(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/ /g, '-');
    //str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");  
    /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
    str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-  
    str = str.replace(/^\-+|\-+$/g, "");
    //cắt bỏ ký tự - ở đầu và cuối chuỗi 
    return str.toLowerCase();
}

var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
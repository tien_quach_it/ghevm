(function($) {
    $(function() {
        //$('.jcarousel').jcarousel();
        var jcarousel = $('.jcarousel');

        jcarousel.on('jcarousel:reload jcarousel:create', function () {
                        jcarousel.jcarousel('items').width(jcarousel.innerWidth());
                    })
                    .jcarousel();

                    //.jcarousel({
                    //    wrap: 'circular',
                    //    transitions: Modernizr.csstransitions ? {
                    //        transforms: Modernizr.csstransforms,
                    //        transforms3d: Modernizr.csstransforms3d,
                    //        easing: 'ease'
                    //    } : false
                    //});

        $('.jcarousel-control-prev')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .jcarouselPagination();
    });
})(jQuery);

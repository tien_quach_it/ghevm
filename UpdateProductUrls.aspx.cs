﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website;

public partial class UpdateProductUrls : System.Web.UI.Page
{
    //protected void btnUpdateProductUrls_Click(object sender, EventArgs e)
    //{
    //    DataTable tbl = DB.GetTable("select ID, ProductCode, CategorySeoUrl from vw_Product where Deleted = 0");
    //    foreach (DataRow row in tbl.Rows)
    //    {
    //        string url = SeoHelper.ConvertToUnsign(row["CategorySeoUrl"].ToString().Replace(".aspx", "/") +
    //                                        row["ProductCode"] + ".aspx");
    //        DB.ExecuteSQLFormat("Update Product set SeoUrl = '{0}' where ID = {1}", url, row["ID"]);

    //        SeoHelper.UpdateSiteUrlsConfig(new ProductEntity { ID = Convert.ToInt32(row["ID"]), SeoUrl = url});
    //    }
    //}

    protected void btnUpdateCategoryUrls_Click(object sender, EventArgs e)
    {
        DataTable tbl = DB.GetTable("select ID, CategoryName, ParentCategoryName, ParentID from vw_Category where Deleted = 0");
        foreach (DataRow row in tbl.Rows)
        {
            string url = "";
            if (Convert.ToInt32(row["ParentID"]) == 0)
                url = SeoHelper.ConvertToUnsign(String.Format("/{0}.aspx", row["CategoryName"]));
            else
                url = SeoHelper.ConvertToUnsign(String.Format("/{0}/{1}.aspx", row["ParentCategoryName"], row["CategoryName"]));

            DB.ExecuteSQLFormat("Update Category set SeoUrl = '{0}' where ID = {1}", url, row["ID"]);

            SeoHelper.UpdateSiteUrlsConfig(new CategoryEntity { ID = Convert.ToInt32(row["ID"]), SeoUrl = url });
        }
    }
}